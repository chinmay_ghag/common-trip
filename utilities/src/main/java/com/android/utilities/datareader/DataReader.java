package com.android.utilities.datareader;

/**
 * The Class _DataReader.
 */
public abstract class DataReader {

    /**
     * The m data client.
     */
    protected DataClient mDataClient;

    /**
     * Instantiates a new _ data reader.
     *
     * @param client the client
     */
    protected DataReader(DataClient client) {
        mDataClient = client;
    }

    /**
     * Read.
     *
     *
     */
    public abstract void read(int readRquestCode);

    /**
     * Inits the.
     */
    public abstract void init();

    /**
     * Notify client.
     *
     * @param readRequestCode the read request code
     * @param result          the result
     */
    protected final void notifyClientData(int readRequestCode, Object result) {
        if (mDataClient.needResponse()) {
            mDataClient.onDataReceived(readRequestCode, result);
        }
    }

    /**
     * Notify client empty.
     *
     * @param readRequestCode the read request code
     */
    protected final void notifyClientEmpty(int readRequestCode) {
        if (mDataClient.needResponse()) {
            mDataClient.onDataEmpty(readRequestCode);
        }
    }

    /**
     * Notify client error.
     *
     * @param requestCode the request code
     */
    protected final void notifyClientError(int requestCode,Object error) {
        {
            if (mDataClient.needResponse()) {
                mDataClient.onError(requestCode,error);
            }
        }
    }

    /**
     * The Interface DataClient.
     */
    public interface DataClient {

        /**
         * On data received.
         *
         * @param readRequestCode the read request code
         * @param data            the data
         */
        public void onDataReceived(int readRequestCode, Object data);

        /**
         * On data empty.
         *
         * @param readRequestCode the read request code
         */
        public void onDataEmpty(int readRequestCode);

        /**
         * On error.
         *
         * @param readRequestCode the read request code
         */
        public void onError(int readRequestCode,Object error);

        public boolean needResponse();
    }

}