package com.android.utilities.async;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by yashesh on 4/2/15.
 */
public class AsyncExecutor {


    public static AsyncExecutor instance;


    private AsyncExecutor(){


    }

    public static synchronized  AsyncExecutor getInstance(){
        if(instance==null){
            instance=new AsyncExecutor();
        }
        return  instance;
    }


    public synchronized void execute(AsyncExecutable job){

        job.execute();

    }




}
