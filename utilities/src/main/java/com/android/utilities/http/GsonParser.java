package com.android.utilities.http;

import android.util.Log;

import com.google.gson.Gson;


// TODO: Auto-generated Javadoc

/**
 * The Class GsonParser.
 *
 * @param <T> the generic type
 */
public class GsonParser<T extends WebResponse<T>> {

    /**
     * The gson.
     */
    Gson gson;

    /**
     * Instantiates a new gson parser.
     */
    public GsonParser() {
        gson = new Gson();
    }

    /**
     * Parses the web service response.
     *
     * @param response the response
     * @param model    the model
     * @return the response model
     */
    public T parseWebServiceResponse(String response, Class<T> model) {
        try {
            T modelObj = gson.fromJson(response, model);
            Log.w("parsed model", modelObj + "");
            return modelObj;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;


    }

    /**
     * Parses the web service response as array.
     *
     * @param response the response
     * @param model    the model
     * @return the array of response models
     */
    public T[] parseWebServiceResponseAsArray(String response, Class<T[]> model) {

        T[] modelObj = gson.fromJson(response, model);
        return modelObj;
    }

}
