package com.android.utilities.http;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.android.utilities.datareader.DataReader;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Class HttpDataReader.
 */
public class HttpDataReader extends DataReader {

    /**
     * The Enum RequestType.
     */
    public static enum RequestType {

        /**
         * The get.
         */
        GET,
        /**
         * The post.
         */
        POST;
    }

    Map<String, String> headers = null;

    /**
     * The Enum DataType.
     */
    public static enum DataType {

        /**
         * The xml.
         */
        XML, /**
         * The json.
         */
        JSON;
    }

    /**
     * The m req type.
     */
    private RequestType mReqType;

    /**
     * The m data type.
     */
    private DataType mDataType;

    /**
     * Instantiates a new http data reader.
     *
     * @param client the client
     */
    protected HttpDataReader(DataClient client) {
        super(client);

    }

    /**
     * Gets the.
     *
     * @param url            the url
     * @param params         the params
     * @param readRquestCode the read rquest code
     * @throws IOException
     * @throws ClientProtocolException
     */
    public String get(String url, HttpParams params, final int readRquestCode)
            throws ClientProtocolException, IOException {

        return RestClient.get(url, params);

    }

    /**
     * Gets the.
     *
     * @param url            the url
     * @param params         the params
     * @param readRquestCode the read rquest code
     * @throws IOException
     * @throws ClientProtocolException
     */
    public String get(String url, HttpParams params, final int readRquestCode, Map<String, String> headers)
            throws ClientProtocolException, IOException {

        return RestClient.get(url, params, headers);

    }

    /**
     * Post.
     *
     * @param url            the url
     * @param params         the params
     * @param readRquestCode the read rquest code
     * @throws IOException
     * @throws ClientProtocolException
     */
    public String post(String url, HttpParams params,
                       AbstractHttpEntity entity, final int readRquestCode)
            throws ClientProtocolException, IOException {

        return RestClient.post(url, params, entity);

    }


    /**
     * Post.
     *
     * @param url            the url
     * @param params         the params
     * @param readRquestCode the read rquest code
     * @throws IOException
     * @throws ClientProtocolException
     */
    public String post(String url, HttpParams params,
                       AbstractHttpEntity entity, final int readRquestCode, Map<String, String> headers)
            throws ClientProtocolException, IOException {

        return RestClient.post(url, params, entity, headers);

    }

    /*
     * (non-Javadoc)
     *
     * @see com.android.utilities.DataReader.DataReader#read(int)
     */
    @Override
    public void read(int readRquestCode) {

    }

    /*
     * (non-Javadoc)
     *
     * @see com.android.utilities.DataReader.DataReader#init()
     */
    @Override
    public void init() {

    }

    public static final class Request<T extends WebResponse<T>> extends
            HttpDataReader {
        GsonParser<T> parser;
        RequestType type;
        String mUrl;
        AbstractHttpEntity entity;
        int mCode;

        public AbstractHttpEntity getEntity() {
            return entity;
        }

        public void setEntity(AbstractHttpEntity entity) {
            this.entity = entity;
        }

        HttpParams parameters;
        boolean isJsonArray = false;
        Class<T> objType;
        Class<T[]> arrayType;

        private Request(DataClient client) {
            super(client);
            parser = new GsonParser();

        }

        public void execute() {

            try {
                if (isJsonArray) {
                    notifyClientData(mCode, parseArray(doRequest()));
                } else {
                    notifyClientData(mCode, parseObject(doRequest()));

                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.w("UTILITIES WEB ERROR:", e.toString());
                notifyClientError(mCode,e.toString());
            }

        }

        public String doRequest() throws ClientProtocolException, IOException {

            String response = "";
            switch (type) {
                case GET:

                    response = get(mUrl, parameters, mCode, headers);
                    Log.e("get response", response + "");
                    break;
                case POST:
                    response = post(mUrl, parameters, entity, mCode, headers);
                    Log.e("strinf response", response + "");
                    break;
                default:
                    break;
            }
            //		Log.e("REQ class", "Response doReq():\n" + response);
            return response;

        }

        public void executeAsync() {
            final Handler handler = new Handler() {

                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);

                    if (msg.what != 0) {
                        String respo = msg.obj + "";

                        try {
                            if (isJsonArray) {

                                Object resObj = parseArray(respo);

                                notifyClientData(mCode, resObj);

                            } else {
                                notifyClientData(mCode, parseObject(respo));

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.w("UTILITIES WEB ERROR:", e.toString());
                            notifyClientError(mCode,e.toString());
                        }
                    } else {
                        notifyClientError(mCode,"Error");
                    }
                }

            };

            new Thread() {
                public void run() {
                    try {
                        String response = doRequest();
                        Message msg = new Message();
                        msg.what = 1;
                        msg.obj = response;
                        handler.sendMessage(msg);
                    } catch (ClientProtocolException e) {
                        handler.sendEmptyMessage(0);
                        e.printStackTrace();
                    } catch (IOException e) {
                        handler.sendEmptyMessage(0);
                        e.printStackTrace();
                    }
                }
            }.start();
        }

        public T[] parseArray(String response) {
            return parser.parseWebServiceResponseAsArray(response, arrayType);
        }

        public T parseObject(String response) {
            return parser.parseWebServiceResponse(response, objType);
        }

        public static class Builder<T extends WebResponse<T>> {
            Request<T> mRequest;

            public Builder(DataClient client) {
                mRequest = new Request<T>(client);
            }

            public Builder<T> ofType(RequestType type) {
                mRequest.type = type;
                return this;
            }

            public Builder<T> onUrl(String url) {
                mRequest.mUrl = url;
                return this;
            }

            public Builder<T> setRequestCode(int requestCode) {
                mRequest.mCode = requestCode;
                return this;
            }


            public Builder<T> addHeader(String key, String value) {
                if (mRequest.headers == null) {
                    mRequest.headers = new HashMap<String, String>();

                }
                mRequest.headers.put(key, value);
                return this;
            }

            public Builder<T> setPostEntity(AbstractHttpEntity entity) {
                mRequest.entity = entity;

                return this;
            }

            public Builder<T> addParameter(String name, Object value) {
                if (mRequest.parameters == null) {
                    mRequest.parameters = new BasicHttpParams();

                }

                mRequest.parameters.setParameter(name, value);

                return this;
            }

            public Builder<T> ofResponseType(Class<T> type) {
                mRequest.objType = type;
                mRequest.isJsonArray = false;
                return this;
            }

            public Builder<T> ofResponseArrayType(Class<T[]> type) {
                mRequest.arrayType = type;
                mRequest.isJsonArray = true;
                return this;
            }

            public Request<T> build() {
                return mRequest;
            }

        }

    }
}
