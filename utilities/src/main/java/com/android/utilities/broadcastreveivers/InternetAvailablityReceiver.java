package com.android.utilities.broadcastreveivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.android.utilities.Validation;


/**
 * The Class InternetAvailablityReceiver.
 * for receiving the broadcast from android system.
 */
public class InternetAvailablityReceiver extends BroadcastReceiver {

    /* (non-Javadoc)
     * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        // forward intent to AppInternetReceiver across the app.

        Intent localIntent = new Intent("internet");

        context.sendBroadcast(localIntent);
    }


    public static class InternetAvailablilityFilter extends IntentFilter {


        public InternetAvailablilityFilter() {
            super("internet");
        }

    }

    /**
     * The Class AppInternetReceiver.
     * for app level receiving of internet alrets.
     */
    public static class AppInternetReceiver extends BroadcastReceiver {
        InternetAlertReceiver mReveiver;

        public AppInternetReceiver(InternetAlertReceiver receiver) {
            mReveiver = receiver;
        }

        /* (non-Javadoc)
         * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Validation.Network.isConnected(context)) {
                mReveiver.onInternetAvailable();
            } else {
                mReveiver.onInternetUnavailable();
            }

        }

    }


    /**
     * The Interface InternetAlertReceiver.
     */
    public static interface InternetAlertReceiver {

        /**
         * On iternet avaialble.
         */
        public void onInternetAvailable();

        /**
         * On internet unavailable.
         */
        public void onInternetUnavailable();

    }
}
