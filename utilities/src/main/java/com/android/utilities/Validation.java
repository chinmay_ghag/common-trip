package com.android.utilities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.text.format.Formatter;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import org.apache.http.conn.util.InetAddressUtils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODO: Auto-generated Javadoc

/**
 * The Class Validation.
 */
public class Validation {

    /**
     * The Class Network.
     */
    public static class Network {

        /**
         * Checks if is connected.
         *
         * @param context the context
         * @return true, if is connected
         */
        public static boolean isConnected(Context context) {

            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager
                    .getActiveNetworkInfo();


            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        /**
         * Checks if is wifi connected.
         *
         * @param context the context
         * @return true, if is wifi connected
         */
        public static boolean isWifiConnected(Context context) {

            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager
                    .getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                return activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI ? true
                        : false;
            }
            return false;
        }

        /**
         * Open network setting.
         *
         * @param context the context
         */
        public void openNetworkSettings(Context context) {
            context.startActivity(new Intent(
                    android.provider.Settings.ACTION_WIRELESS_SETTINGS));
        }

        /**
         * Gets the local ip address.
         *
         * @param context the context
         * @return the local ip address
         */
        public static String getLocalIpAddress(Context context) {
            /** This method returns the Local IP address */
            WifiManager wifiMgr = (WifiManager) context
                    .getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
            int ip = wifiInfo.getIpAddress();
            String address = Formatter.formatIpAddress(ip);

            return address;
        }

        /**
         * Gets the network ip address.
         *
         * @param useIPv4 the use i pv4
         * @return the network ip address
         */
        public static String getNetworkIPAddress(boolean useIPv4) {
            /** This method returns the Network IP address */
            try {
                List<NetworkInterface> interfaces = Collections
                        .list(NetworkInterface.getNetworkInterfaces());
                for (NetworkInterface intf : interfaces) {
                    List<InetAddress> addrs = Collections.list(intf
                            .getInetAddresses());
                    for (InetAddress addr : addrs) {
                        if (!addr.isLoopbackAddress()) {
                            String sAddr = addr.getHostAddress().toUpperCase();
                            boolean isIPv4 = InetAddressUtils
                                    .isIPv4Address(sAddr);
                            if (useIPv4) {
                                if (isIPv4)
                                    return sAddr;
                            } else {
                                if (!isIPv4) {
                                    int delim = sAddr.indexOf('%'); // drop ip6
                                    // port
                                    // suffix
                                    return delim < 0 ? sAddr : sAddr.substring(
                                            0, delim);
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
            } // for now eat exceptions
            return "";
        }

        /**
         * Checks if is airplane mode on.
         *
         * @param context the context
         * @return true, if is airplane mode on
         */
        public static boolean isAirplaneModeOn(Context context) {
            /** This method returns true if Airplane mode is ON */
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        }

        /**
         * Gets the available network info.
         *
         * @param context the context
         * @return the available network info
         */
        public static String getAvailableNetworkInfo(Context context) {
            ConnectivityManager conMan = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            State mobile = conMan.getNetworkInfo(0).getState();
            State wifi = conMan.getNetworkInfo(1).getState();
            if (mobile == NetworkInfo.State.CONNECTED
                    || mobile == NetworkInfo.State.CONNECTING) {
                return "mobile";
            } else if (wifi == NetworkInfo.State.CONNECTED
                    || wifi == NetworkInfo.State.CONNECTING) {
                return "wifi";
            }

            return null;
        }
    }

    /**
     * The Class UI.
     */
    public static class UI {


        public static class FieldError {

            private String errorMessage;
            private int fieldResourceId;

            public FieldError(int resourceId, String errorMessage) {
                this.errorMessage = errorMessage;
                fieldResourceId = resourceId;
            }

            public String getErrorMessage() {
                return errorMessage;
            }

            public void setErrorMessage(String errorMessage) {
                this.errorMessage = errorMessage;
            }

            public int getFieldResourceId() {
                return fieldResourceId;
            }

            public void setFieldResourceId(int fieldResourceId) {
                this.fieldResourceId = fieldResourceId;
            }
        }


        /**
         * Show keyboard.
         *
         * @param context the context
         * @param view    the view
         */
        public static void showKeyboard(Context context, View view) {
            ((InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE))
                    .showSoftInput(view, 0);
        }

        /**
         * Hide keyboard.
         *
         * @param context the context
         * @param view    the view
         */
        public static void hideKeyboard(Context context, View view) {
            ((InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE))
                    .hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        /**
         * Toggle visibility.
         *
         * @param view the view
         */
        public static void toggleVisibility(View view) {
            if (view.getVisibility() == View.VISIBLE) {
                view.setVisibility(View.GONE);
            } else {
                view.setVisibility(View.VISIBLE);
            }
        }

        /**
         * Toggle in visibility.
         *
         * @param view the view
         */
        public static void toggleInVisibility(View view) {
            if (view.getVisibility() == View.VISIBLE) {
                view.setVisibility(View.INVISIBLE);
            } else {
                view.setVisibility(View.VISIBLE);
            }
        }

        /**
         * Checks if is visible.
         *
         * @param v the v
         * @return true, if is visible
         */
        public static final boolean isVisible(View v) {
            return v.getVisibility() == View.VISIBLE ? true : false;
        }
    }


    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password

    /**
     * @param Password
     * @return true if Password is valid Password
     * @author priyanka kale
     */
    public static boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 6) {
            return true;
        }
        return false;
    }
}
/** Email validation pattern. *//*
    private static final Pattern EMAIL_ADDRESS = Pattern
			.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
					+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
					+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");

					
	*//**
 * Validation for email. for Android2.2 onwards android.util.Pattern
 *
 * @param email the email
 * @return true if email is valid email
 * @author Snehal Penurkar
 *//*
    public static boolean isEmailValid(CharSequence email) {
		return EMAIL_ADDRESS.matcher(email).matches();
	}*/
