package com.drive.share.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.drive.share.data.webapi.TalkModel;
import com.opendroid.db.CursorUtils;
import com.opendroid.db.dao.BaseDAO;
import com.opendroid.db.dao.DAOException;

/**
 * Created by webwerks on 14/3/15.
 */
public class TalkDao extends BaseDAO<TalkModel> {
    /**
     * Instantiates a new base dao.
     *
     * @param context the context
     * @param db      the db
     */
    public TalkDao(Context context, SQLiteDatabase db) {
        super(context, db);
    }

    /**
     * Gets the table name for DAO.
     *
     * @return the table name
     */
    @Override
    public String getTableName() {
        return TalkModel.TABLE_NAME;
    }

    @Override
    public String getPrimaryColumnName() {
        return TalkModel.COLUMN_ID;
    }

    @Override
    public TalkModel fromCursor(Cursor c) {

        TalkModel model = new TalkModel();
        model.setTripId(CursorUtils.extractStringOrNull(c,TalkModel.COLUMN_TRIP_ID));
        model.setTripName(CursorUtils.extractStringOrNull(c, TalkModel.COLUMN_TRIP_NAME));
        model.setMessageOwnerId(CursorUtils.extractStringOrNull(c, TalkModel.COLUMN_MESSAGE_OWNER_ID));
        model.setMessageOwnerName(CursorUtils.extractStringOrNull(c, TalkModel.COLUMN_MESSAGE_OWNER_NAME));
        model.setMessage(CursorUtils.extractStringOrNull(c, TalkModel.COLUMN_MESSAGE));
        model.setReceptionType(CursorUtils.extractStringOrNull(c, TalkModel.COLUMN_RECEPTION_TYPE));
        model.setTimestamp(CursorUtils.extractStringOrNull(c, TalkModel.COLUMN_TIMESTAMP));
        return model;
    }


    @Override
    public void createOrUpdate(TalkModel model) throws DAOException {
        if (exists(model.getId())) {
            update(model);
        } else {
            create(model);
        }
    }


    public boolean exists(String id) throws DAOException {
        {
            Cursor c = null;

            try {
                c = db.rawQuery("select "+getPrimaryColumnName()+" from " + getTableName() + " where "
                        + getPrimaryColumnName() + " = ?", new String[]{id});
                return c.moveToFirst();
            } catch (Exception e) {
                throw new DAOException(e);
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        }
    }

    @Override
    public ContentValues values(TalkModel talkModel) {

        ContentValues values = new ContentValues();
        values.put(TalkModel.COLUMN_TRIP_ID,talkModel.getTripId());
        values.put(TalkModel.COLUMN_TRIP_NAME,talkModel.getTripName());
        values.put(TalkModel.COLUMN_MESSAGE_OWNER_ID,talkModel.getMessageOwnerId());
        values.put(TalkModel.COLUMN_MESSAGE_OWNER_NAME,talkModel.getMessageOwnerName());
        values.put(TalkModel.COLUMN_MESSAGE,talkModel.getMessage());
        values.put(TalkModel.COLUMN_RECEPTION_TYPE,talkModel.getReceptionType());
        values.put(TalkModel.COLUMN_TIMESTAMP,talkModel.getTimestamp());
        return values;
    }
}
