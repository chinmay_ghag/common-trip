package com.drive.share.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.drive.share.data.webapi.TripModel;
import com.drive.share.data.webapi.UserModel;
import com.opendroid.db.CursorUtils;
import com.opendroid.db.StringUtils;
import com.opendroid.db.dao.BaseDAO;

/**
 * Created by webwerks on 17/2/15.
 */
public class TripDao extends BaseDAO<TripModel> {
    /**
     * Instantiates a new base dao.
     *
     * @param context the context
     * @param db      the db
     */
    public TripDao(Context context, SQLiteDatabase db) {
        super(context, db);
    }

    /**
     * Gets the table name for DAO.
     *
     * @return the table name
     */
    @Override
    public String getTableName() {
        return TripModel.TABLE_NAME;
    }

    @Override
    public String getPrimaryColumnName() {
        return UserModel.COLUMN_MOB_NUMBER;
    }

    @Override
    public TripModel fromCursor(Cursor c) {
        TripModel model = new TripModel();
        model.setTrip_name(CursorUtils.extractStringOrNull(c,TripModel.COLUMN_TRIP_NAME));
        model.setCreate_departure_trip(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_CREATE_DEPARTURE_TRIP));
        model.setDestination_latitude(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_DESTINATION_LATITUDE));
        model.setDestination_longitude(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_DESTINATION_LONGITUDE));
        model.setDestination_name(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_DESTINATION_NAME));
        model.setEnd_date(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_END_DATE));
        model.setEnd_time(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_END_TIME));
        model.setIs_pick_up(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_IS_PICK_UP));
        model.setOrigin_latitude(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_ORIGIN_LATITUDE));
        model.setOrigin_longitude(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_DESTINATION_LONGITUDE));
        model.setOrigin_name(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_ORIGIN_NAME));
        model.setStart_date(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_START_DATE));
        model.setStart_time(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_START_TIME));
        model.setTrip_days(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_TRIP_DAYS));
        model.setTrip_frequency(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_TRIP_FREQUENCY));
        model.setTripDistance(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_TRIP_DISTANCE));
        model.setTripTotalPassenger(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_TRIP_TOTAL_PASSENGER));
        model.setTripUserRole(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_TRIP_USER_ROLE));
        model.setVehicle_id(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_VEHICLE_ID));
        model.setTrip_type(CursorUtils.extractStringOrNull(c, TripModel.COLUMN_TRIP_TYPE));
        return model;
    }

    @Override
    public ContentValues values(TripModel tripModel) {
        ContentValues values = new ContentValues();
        values.put(TripModel.COLUMN_USER_ID, tripModel.getUser_id());
        values.put(TripModel.COLUMN_TRIP_CARD_ID, tripModel.getTrip_card_id());
        values.put(TripModel.COLUMN_TRIP_NAME, tripModel.getTrip_name());
        values.put(TripModel.COLUMN_TRIP_FREQUENCY, tripModel.getTrip_frequency());
        values.put(TripModel.COLUMN_VEHICLE_ID, tripModel.getVehicle_id());
        values.put(TripModel.COLUMN_ORIGIN_LATITUDE, tripModel.getOrigin_latitude());
        values.put(TripModel.COLUMN_ORIGIN_LONGITUDE, tripModel.getOrigin_longitude());
        values.put(TripModel.COLUMN_ORIGIN_NAME, tripModel.getOrigin_name());
        values.put(TripModel.COLUMN_DESTINATION_LATITUDE, tripModel.getDestination_latitude());
        values.put(TripModel.COLUMN_DESTINATION_LONGITUDE, tripModel.getDestination_longitude());
        values.put(TripModel.COLUMN_DESTINATION_NAME, tripModel.getDestination_name());
        values.put(TripModel.COLUMN_CREATE_DEPARTURE_TRIP, tripModel.getCreate_departure_trip());
        values.put(TripModel.COLUMN_START_DATE, tripModel.getStart_date());
        values.put(TripModel.COLUMN_START_TIME, tripModel.getStart_time());
        values.put(TripModel.COLUMN_END_DATE, tripModel.getEnd_date());
        values.put(TripModel.COLUMN_TRIP_DAYS, tripModel.COLUMN_TRIP_DAYS);
        values.put(TripModel.COLUMN_TRIP_DISTANCE, tripModel.COLUMN_TRIP_DISTANCE);
        values.put(TripModel.COLUMN_TRIP_TOTAL_PASSENGER, tripModel.COLUMN_TRIP_TOTAL_PASSENGER);
        values.put(TripModel.COLUMN_TRIP_USER_ROLE, tripModel.getTripUserRole());
        values.put(TripModel.COLUMN_IS_PICK_UP, tripModel.getIs_pick_up());
        values.put(TripModel.COLUMN_TRIP_TYPE, tripModel.getTrip_type());
        values.put(TripModel.COLUMN_END_TIME, tripModel.getEnd_time());

        return values;
    }


}
