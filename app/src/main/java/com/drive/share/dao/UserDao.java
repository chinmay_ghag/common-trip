package com.drive.share.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.drive.share.data.webapi.UserModel;
import com.opendroid.db.CursorUtils;
import com.opendroid.db.dao.BaseDAO;
import com.opendroid.db.dao.DAOException;

/**
 * Created by webwerks on 17/2/15.
 */
public class UserDao extends BaseDAO<UserModel> {
    /**
     * Instantiates a new base dao.
     *
     * @param context the context
     * @param db      the db
     */
    public UserDao(Context context, SQLiteDatabase db) {
        super(context, db);
    }

    /**
     * Gets the table name for DAO.
     *
     * @return the table name
     */
    @Override
    public String getTableName() {
        return UserModel.TABLE_NAME;
    }

    @Override
    public String getPrimaryColumnName() {
        return UserModel.COLUMN_MOB_NUMBER;
    }

    @Override
    public UserModel fromCursor(Cursor c) {

        UserModel model = new UserModel();
        model.setMobNumber(CursorUtils.extractStringOrNull(c,UserModel.COLUMN_MOB_NUMBER));
        model.setFirstName(CursorUtils.extractStringOrNull(c, UserModel.COLUMN_FIRST_NAME));
        model.setLastName(CursorUtils.extractStringOrNull(c, UserModel.COLUMN_LAST_NAME));
        model.setEmail(CursorUtils.extractStringOrNull(c, UserModel.COLUMN_EMAIL_ID));
        model.setUserName(CursorUtils.extractStringOrNull(c, UserModel.COLUMN_USER_NAME));
        model.setDob(CursorUtils.extractStringOrNull(c, UserModel.COLUMN_DOB));
        model.setGender(CursorUtils.extractStringOrNull(c, UserModel.COLUMN_GENDER));
        model.setProfilePic(CursorUtils.extractStringOrNull(c, UserModel.COLUMN_PROFILE_PIC));
        model.setUserId(CursorUtils.extractStringOrNull(c, UserModel.COLUMN_USER_ID));
        model.setIsRegistered(CursorUtils.extractIntegerOrNull(c, UserModel.COLUMN_IS_REGISTERED));
        return model;
    }

    @Override
    public void createOrUpdate(UserModel model) throws DAOException {
        if (exists(model.getMobNumber())) {
            update(model);
        } else {
            create(model);
        }
    }


    public boolean exists(String id) throws DAOException {
        {
            Cursor c = null;

            try {
                c = db.rawQuery("select "+getPrimaryColumnName()+" from " + getTableName() + " where "
                        + getPrimaryColumnName() + " = ?", new String[]{id});
                return c.moveToFirst();
            } catch (Exception e) {
                throw new DAOException(e);
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        }
    }

    @Override
    public ContentValues values(UserModel userModel) {

        ContentValues values = new ContentValues();
        values.put(UserModel.COLUMN_MOB_NUMBER,userModel.getMobNumber());
        values.put(UserModel.COLUMN_FIRST_NAME,userModel.getFirstName());
        values.put(UserModel.COLUMN_LAST_NAME,userModel.getLastName());
        values.put(UserModel.COLUMN_EMAIL_ID,userModel.getEmail());
        values.put(UserModel.COLUMN_USER_NAME,userModel.getUserName());
        values.put(UserModel.COLUMN_DOB,userModel.getDob());
        values.put(UserModel.COLUMN_GENDER,userModel.getGender());
        values.put(UserModel.COLUMN_PROFILE_PIC,userModel.getProfilePic());
        values.put(UserModel.COLUMN_USER_ID,userModel.getUserId());
        values.put(UserModel.COLUMN_IS_REGISTERED,userModel.isRegistered());
        return values;
    }
}
