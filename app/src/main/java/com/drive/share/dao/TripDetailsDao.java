package com.drive.share.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.drive.share.data.webapi.TripCardDetails;
import com.drive.share.ui.activities.tripcard.TripCardActivity;
import com.opendroid.db.CursorUtils;
import com.opendroid.db.dao.BaseDAO;

/**
 * Created by Pervacio-Dev on 3/8/2015.
 */
public class TripDetailsDao extends BaseDAO<TripCardDetails> {
    /**
     * Instantiates a new base dao.
     *
     * @param context the context
     * @param db      the db
     */
    public TripDetailsDao(Context context, SQLiteDatabase db) {
        super(context, db);
    }

    @Override
    public String getPrimaryColumnName() {
        return TripCardDetails.COLUMN_TRIP_CARD_ID;
    }

    @Override
    public String getTableName() {
        return TripCardDetails.TABLE_NAME;
    }

    @Override
    public TripCardDetails fromCursor(Cursor c) {
        TripCardDetails model = new TripCardDetails();
        model.setDestinationName(CursorUtils.extractStringOrNull(c, TripCardDetails.COLUMN_DESTINATION_NAME));
        model.setDistance(CursorUtils.extractStringOrNull(c, TripCardDetails.COLUMN_TRIP_CARD_ID));
        model.setDistance(CursorUtils.extractStringOrNull(c, TripCardDetails.COLUMN_DISTANCE));
        model.setFirstname(CursorUtils.extractStringOrNull(c, TripCardDetails.COLUMN_TRIP_NAME));
        model.setLasttname(CursorUtils.extractStringOrNull(c, TripCardDetails.COLUMN_LAST_NAME));
        model.setOriginName(CursorUtils.extractStringOrNull(c, TripCardDetails.COLUMN_ORIGIN_NAME));
        model.setTripCardId(CursorUtils.extractStringOrNull(c, TripCardDetails.COLUMN_TRIP_CARD_ID));
        model.setTripName(CursorUtils.extractStringOrNull(c, TripCardDetails.COLUMN_TRIP_NAME));
        return model;
    }

    @Override
    public ContentValues values(TripCardDetails tripDetails) {
        ContentValues values = new ContentValues();
        values.put(TripCardDetails.COLUMN_TRIP_CARD_ID, tripDetails.getTripCardId());
        values.put(TripCardDetails.COLUMN_TRIP_NAME, tripDetails.getTripName());
        values.put(TripCardDetails.COLUMN_FIRST_NAME, tripDetails.getFirstname());
        values.put(TripCardDetails.COLUMN_LAST_NAME, tripDetails.getLasttname());
        values.put(TripCardDetails.COLUMN_ORIGIN_NAME, tripDetails.getOriginName());
        values.put(TripCardDetails.COLUMN_DESTINATION_NAME, tripDetails.getDestinationName());
        values.put(TripCardDetails.COLUMN_DISTANCE, tripDetails.getDistance());
        return values;
    }
}
/*
public static final String COLUMN_TRIP_CARD_ID = "trip_card_id";
public static final String COLUMN_TRIP_NAME = "trip_name";
public static final String COLUMN_FIRST_NAME = "first_name";
public static final String COLUMN_LAST_NAME = "last_name";
public static final String COLUMN_ORIGIN_NAME = "origin_name";
public static final String COLUMN_DESTINATION_NAME = "destination_name";
public static final String COLUMN_DISTANCE = "distance";*/