package com.drive.share.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.drive.share.data.webapi.Passenger;
import com.opendroid.db.CursorUtils;
import com.opendroid.db.dao.BaseDAO;

/**
 * Created by Pervacio-Dev on 3/8/2015. 9221946518
 */
public class PassengerDao extends BaseDAO<Passenger>{
    /**
     * Instantiates a new base dao.
     *
     * @param context the context
     * @param db      the db
     */
    public PassengerDao(Context context, SQLiteDatabase db) {
        super(context, db);
    }

    @Override
    public String getPrimaryColumnName() {
        return Passenger.COLUMN_PASSENGER_ID;
    }

    @Override
    public String getTableName() {
        return Passenger.TABLE_NAME;
    }

    @Override
    public Passenger fromCursor(Cursor c) {
        Passenger passenger = new Passenger();
        passenger.setPassengerId(CursorUtils.extractStringOrNull(c, Passenger.COLUMN_PASSENGER_ID));
        passenger.setPassengerName(CursorUtils.extractStringOrNull(c, Passenger.COLUMN_PASSENGER_NAME));
        passenger.setTrip_card_id(CursorUtils.extractStringOrNull(c, Passenger.COLUMN_TRIP_CARD_ID));
        return passenger;
    }

    @Override
    public ContentValues values(Passenger passenger) {
        ContentValues values = new ContentValues();
        values.put(Passenger.COLUMN_PASSENGER_ID, passenger.getPassengerId());
        values.put(Passenger.COLUMN_PASSENGER_NAME, passenger.getPassengerName());
        return values;
    }
}
