package com.drive.share.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.drive.share.data.webapi.VehicleModel;
import com.opendroid.db.CursorUtils;
import com.opendroid.db.dao.BaseDAO;

/**
 * Created by yashesh on 17/2/15.
 */
public class VehicleDao extends BaseDAO<VehicleModel> {
    /**
     * Instantiates a new base dao.
     *
     * @param context the context
     * @param db      the db
     */
    public VehicleDao(Context context, SQLiteDatabase db) {
        super(context, db);
    }

    /**
     * Gets the table name for DAO.
     *
     * @return the table name
     */
    @Override
    public String getTableName() {
        return VehicleModel.TABLE_NAME;
    }

    @Override
    public String getPrimaryColumnName() {
        return VehicleModel.COLUMN_VEHICLE_ID;
    }

    @Override
    public VehicleModel fromCursor(Cursor c) {

        VehicleModel model = new VehicleModel();
        model.setVehicleId(CursorUtils.extractStringOrNull(c, VehicleModel.COLUMN_VEHICLE_ID));
        model.setVehicle_type(CursorUtils.extractIntegerOrNull(c, VehicleModel.COLUMN_VEHICLE_TYPE));
        model.setImage(CursorUtils.extractStringOrNull(c, VehicleModel.COLUMN_IMAGE));
        model.setVehicle_registration_number(CursorUtils.extractStringOrNull(c,
                VehicleModel.COLUMN_VEHICLE_REGISTRATION_NUMBER));
        model.setVehicle_name(CursorUtils.extractStringOrNull(c, VehicleModel.COLUMN_VEHICLE_NAME));
        model.setDescription(CursorUtils.extractStringOrNull(c, VehicleModel.COLUMN_VEHICLE_NAME));
        model.setUser_id(CursorUtils.extractStringOrNull(c, VehicleModel.COLUMN_USER_ID));
        return model;
    }

    @Override
    public ContentValues values(VehicleModel userModel) {

        ContentValues values = new ContentValues();
        values.put(VehicleModel.COLUMN_VEHICLE_ID,userModel.getVehicleId());
        values.put(VehicleModel.COLUMN_VEHICLE_TYPE,userModel.getVehicle_type());
        values.put(VehicleModel.COLUMN_IMAGE,userModel.getImage());
        values.put(VehicleModel.COLUMN_VEHICLE_REGISTRATION_NUMBER,userModel.getVehicle_registration_number());
        values.put(VehicleModel.COLUMN_VEHICLE_NAME,userModel.getVehicle_name());
        values.put(VehicleModel.COLUMN_DESCRIPTION,userModel.getDescription());
        values.put(VehicleModel.COLUMN_USER_ID,userModel.getUser_id());
        return values;
    }
}
