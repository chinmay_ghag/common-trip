package com.drive.share.loaders;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.util.Log;

import com.android.utilities.datareader.DataReader;
import com.drive.share.dao.UserDao;
import com.drive.share.data.webapi.UserModel;
import com.drive.share.globals.App;
import com.drive.share.globals.Constants;
import com.opendroid.db.DbHelper;
import com.opendroid.db.dao.DAOException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashesh on 2/2/15.
 */
public class ContactsLoader extends DataReader implements Runnable {
    private static final int MINIMUM_TRACKING_PHONE_LENGTH = 8;
    /**
     * Instantiates a new _ data reader.
     *
     * @param client the client
     */


    private Handler clientHandler;

    private Context mContext;

    public ContactsLoader(Context cnt, DataClient client) {
        super(client);
        mContext = cnt;

        clientHandler = new Handler() {

            @Override
            public void handleMessage(Message msg) {

                switch (msg.what) {

                    case Constants.REQ_LOAD_CONTACTS:
                        //Log.e("CONTACTS SERVICE", msg.obj + "");
                        notifyClientData(Constants.REQ_LOAD_CONTACTS, msg.obj);
                        break;
                    case Constants.HANDLER_MSG_NOT_OK:

                        break;


                }

            }
        };


    }

    @Override
    public void read(int readRquestCode) {
        // start worker thread for loading contacts.
        new Thread(this).start();
    }


    public void loadLocal() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    Message msg = new Message();
                    msg.what = Constants.REQ_LOAD_CONTACTS;

                    List<UserModel> lstContacts = new UserDao(App.getInstance(), DbHelper.getInstance(App.getInstance()).getSQLiteDatabase()).findAllGroupedAndOrdered(UserModel.COLUMN_IS_REGISTERED, UserModel.COLUMN_FIRST_NAME, true);

                    Log.d("Alpha", "Contact Size in loadlocal" + lstContacts.size());


                    for (UserModel model : lstContacts) {

                        if (!model.isRegistered()) {
                            model.setFirstName(getContactName(App.getInstance(), model.getMobNumber()));
                            model.setProfilePic(getContactPhotoUri(model.getMobNumber()));
                        }

                    }

                    msg.obj = lstContacts;
                    clientHandler.sendMessage(msg);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public static boolean areRegisteredFriendAvailable() {
        try {
            return new UserDao(App.getInstance(), DbHelper.getInstance(App.getInstance()).getSQLiteDatabase()).isNotEmpty();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public void loadRegisteredFriends() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    Message msg = new Message();
                    msg.what = Constants.REQ_LOAD_CONTACTS;

                    List<UserModel> lstContacts = new UserDao(App.getInstance(), DbHelper.getInstance(App.getInstance()).getSQLiteDatabase()).findAllByField(UserModel.COLUMN_IS_REGISTERED, "1", "ORDER BY " + UserModel.COLUMN_FIRST_NAME + " ASC");

                    Log.d("Alpha", "Loading reg frnds Contact Size -- " + lstContacts.size());


                    for (UserModel model : lstContacts) {

                        if (!model.isRegistered()) {
                            model.setFirstName(getContactName(App.getInstance(), model.getMobNumber()));
                            model.setProfilePic(getContactPhotoUri(model.getMobNumber()));
                        }

                    }

                    msg.obj = lstContacts;
                    clientHandler.sendMessage(msg);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    @Override
    public void init() {

    }

    // load contacts off the UI thread here.
    @Override
    public void run() {
        Message msg = new Message();
        msg.what = Constants.REQ_LOAD_CONTACTS;
        msg.obj = getPhoneContacts();
        clientHandler.sendMessage(msg);
    }


    public static List<UserModel> getPhoneContacts() {

        List<UserModel> contacts = new ArrayList<UserModel>();

        // phone contacts uri
        Uri phoneContactsUri = ContactsContract.Contacts.CONTENT_URI;

        // Querying the table ContactsContract.Contacts to retrieve all the contacts
        Cursor contactsCursor = App.getInstance().getContentResolver().query(phoneContactsUri, null, null, null,
                ContactsContract.Contacts.DISPLAY_NAME + " ASC ");

        if (contactsCursor != null && contactsCursor.getCount() > 0) {
            contactsCursor.moveToFirst();
            while (!contactsCursor.isAfterLast()) {

                int phoneCount = contactsCursor.getInt(contactsCursor
                        .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                if (phoneCount > 0) {

                    contacts.add(parseCursor(contactsCursor));
                }
                contactsCursor.moveToNext();
            }

        }

        if (contactsCursor != null && !contactsCursor.isClosed()) {
            contactsCursor.close();
        }

        Log.e("Alpha", contacts.size() + "Phone contacts");

        return contacts;
    }

    public static String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return contactName;
    }

    public static String getContactId(String phoneNumber) {
        ContentResolver cr = App.getInstance().getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup._ID}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactId = null;
        if (cursor.moveToFirst()) {
            contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup._ID));
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return contactId;
    }

    public static String getContactPhotoUri(String phoneNumber) {
        ContentResolver contentResolver = App.getInstance().getContentResolver();


        String contactId = getContactId(phoneNumber);

        if (contactId == null) {
            return null;
        }

        Cursor cursor = null;

        try {
            cursor = contentResolver
                    .query(ContactsContract.Data.CONTENT_URI,
                            null,
                            ContactsContract.Data.CONTACT_ID
                                    + "="
                                    + getContactId(phoneNumber)
                                    + " AND "

                                    + ContactsContract.Data.MIMETYPE
                                    + "='"
                                    + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
                                    + "'", null, null);

            if (cursor != null) {
                if (!cursor.moveToFirst()) {
                    return null; // no photo
                }
            } else {
                return null; // error in cursor process
            }


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        Uri person = ContentUris.withAppendedId(
                ContactsContract.Contacts.CONTENT_URI, Long.parseLong(contactId));
        return Uri.withAppendedPath(person,
                ContactsContract.Contacts.Photo.CONTENT_DIRECTORY).toString();
    }


    public static UserModel parseCursor(Cursor contactCursor) {

        UserModel contact = new UserModel();


        long contactId = contactCursor.getLong(contactCursor.getColumnIndex("_ID"));


        // fetch detailed data about contact
        Uri dataUri = ContactsContract.Data.CONTENT_URI;

        Cursor dataCursor = App.getInstance().getContentResolver().query(dataUri, null,
                ContactsContract.Data.CONTACT_ID + "=" + contactId,
                null, null);


        if (dataCursor.moveToFirst()) {
            // Getting Display Name
            // Getting Display Name
            contact.setUserName(contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME)));
            do {

                // Getting NickName
                if (dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE))
                    contact.setUserName(dataCursor.getString(dataCursor.getColumnIndex("data1")));

                // Getting Phone numbers
                if (dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)) {
                    switch (dataCursor.getInt(dataCursor.getColumnIndex("data2"))) {
                        /*case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                            contact.setHomePhone(dataCursor.getString(dataCursor.getColumnIndex("data1")));
                            break;*/
                        case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                            //Ashwini
                            String phnNo = dataCursor.getString(dataCursor.getColumnIndex("data1"));
//                            Log.d("Alpha","original phn no"+phnNo);
//                            if(phnNo != null && phnNo.length() > MINIMUM_TRACKING_PHONE_LENGTH &&phnNo.startsWith("0") && phnNo.charAt(2)!='0') {
//                                phnNo = phnNo.substring(1);
//                                Log.d("Alpha","after phn no"+phnNo);
//                                contact.setMobNumber(phnNo);
//                            }
//                            else
                                contact.setMobNumber(phnNo);
                            //
                            break;
                        /*case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                            contact.setWorkPhone(dataCursor.getString(dataCursor.getColumnIndex("data1")));
                            break;*/
                    }
                }

                // Getting EMails
                if (dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)) {
                    switch (dataCursor.getInt(dataCursor.getColumnIndex("data2"))) {
                        case ContactsContract.CommonDataKinds.Email.TYPE_HOME:
                            contact.setEmail(dataCursor.getString(dataCursor.getColumnIndex("data1")));
                            break;
                        case ContactsContract.CommonDataKinds.Email.TYPE_WORK:
                            contact.setEmail(dataCursor.getString(dataCursor.getColumnIndex("data1")));
                            break;
                    }
                }

                // Getting Organization details
                /*if (dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)) {
                    contact.setCompanyName(dataCursor.getString(dataCursor.getColumnIndex("data1")));
                    contact.setTitle(dataCursor.getString(dataCursor.getColumnIndex("data4")));
                }*/


      /*  String details = "";

        // Concatenating various information to single string
        if(homePhone != null && !homePhone.equals("") )
            details = "HomePhone : " + homePhone + "\n";
        if(mobilePhone != null && !mobilePhone.equals("") )
            details += "MobilePhone : " + mobilePhone + "\n";
        if(workPhone != null && !workPhone.equals("") )
            details += "WorkPhone : " + workPhone + "\n";
        if(nickName != null && !nickName.equals("") )
            details += "NickName : " + nickName + "\n";
        if(homeEmail != null && !homeEmail.equals("") )
            details += "HomeEmail : " + homeEmail + "\n";
        if(workEmail != null && !workEmail.equals("") )
            details += "WorkEmail : " + workEmail + "\n";
        if(companyName != null && !companyName.equals("") )
            details += "CompanyName : " + companyName + "\n";
        if(title != null && !title.equals("") )
            details += "Title : " + title + "\n";*/

            } while (dataCursor.moveToNext());


        }

        if (dataCursor != null)
            dataCursor.close();

        return contact;
    }


}
