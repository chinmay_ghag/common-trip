package com.drive.share.loaders;

import android.content.Context;
import android.location.Geocoder;
import android.os.Handler;
import android.os.Message;

import com.android.utilities.datareader.DataReader;

import java.io.IOException;

/**
 * Created by webwerks on 31/1/15.
 */
public class GeoLoacationLoader extends DataReader {


    Context mContext;


    /**
     * Instantiates a new _ data reader.
     *
     * @param client the client
     */
    public GeoLoacationLoader(Context context, DataClient client) {
        super(client);
        mContext = context;
    }

    @Override
    public void read(int readRquestCode) {

    }


    public void read(final int requestCode, final String parameter) {

        final Handler readOperationHandler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        notifyClientError(requestCode, null);
                        break;
                    case 1:
                        notifyClientData(requestCode, msg.obj);
                        break;
                }
            }
        };


        new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(mContext);
                Message msg = new Message();
                try {

                    msg.obj = geocoder.getFromLocationName(parameter, 20);
                    msg.what = 1;

                    readOperationHandler.sendMessage(msg);


                } catch (IOException e) {

                    msg.what = 0;
                    readOperationHandler.sendMessage(msg);

                    e.printStackTrace();
                }
            }
        }.start();
    }


    @Override
    public void init() {

    }
}
