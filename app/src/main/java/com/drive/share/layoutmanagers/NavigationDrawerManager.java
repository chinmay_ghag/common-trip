package com.drive.share.layoutmanagers;

import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.drive.share.adapters.DrawerAdapter;
import com.drive.share.data.DrawerItem;
import com.drive.share.globals.App;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.HomeActivity;
import com.drive.share.ui.activities.profile.ProfileSetUpActivity;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by webwerks on 3/2/15.
 */
public class NavigationDrawerManager implements AdapterView.OnItemClickListener {

    private static NavigationDrawerManager manager;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ArrayList<DrawerItem> mDrawerItemsList = null;

    private String[] navMenu;
    private TypedArray navMenuIcon;

    private HomeActivity mActivity = null;
    private ActionBarDrawerToggle mDrawerToggle;
    private CircleImageView imgProfilePic;
    private TextView txtUserName, txtUserEmail;


    public static NavigationDrawerManager getInstance() {

        if (manager == null) {
            manager = new NavigationDrawerManager();
        }
        return manager;
    }

    private NavigationDrawerManager() {
    }


    public void initializeDrawer(HomeActivity activity, ActionBarDrawerToggle mDrawerToggle, Toolbar toolbar) {

        mDrawerItemsList = new ArrayList<DrawerItem>();

        navMenu = activity.getResources().getStringArray(R.array.nav_menu);
        navMenuIcon = activity.getResources().obtainTypedArray(R.array.nav_menu_icon);


        mDrawerItemsList.add(new DrawerItem(navMenu[0], navMenuIcon.getResourceId(0, 0), false));
        mDrawerItemsList.add(new DrawerItem(navMenu[1], navMenuIcon.getResourceId(1, 0), false));
        mDrawerItemsList.add(new DrawerItem(navMenu[2], navMenuIcon.getResourceId(2, 0), false));
        mDrawerItemsList.add(new DrawerItem(navMenu[3], navMenuIcon.getResourceId(3, 0), true));
        mDrawerItemsList.add(new DrawerItem(navMenu[4], navMenuIcon.getResourceId(4, 0), true));
        mDrawerItemsList.add(new DrawerItem(navMenu[5], navMenuIcon.getResourceId(5, 0), false));
        mDrawerItemsList.add(new DrawerItem(navMenu[6], navMenuIcon.getResourceId(6, 0), false));

       /* mDrawerItemsList.add("My Profile");
        mDrawerItemsList.add("Achievements");
        mDrawerItemsList.add("Trip History");
        mDrawerItemsList.add("Social");
        mDrawerItemsList.add("Message");
        mDrawerItemsList.add("Settings");
        mDrawerItemsList.add("Help and Feedback");*/

        mActivity = activity;
        this.mDrawerToggle = mDrawerToggle;

        mDrawerLayout = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) activity.findViewById(R.id.left_drawer);


        LinearLayout linear = (LinearLayout) LayoutInflater.from(activity).inflate(R.layout.layout_profile_header, null);

        imgProfilePic = (CircleImageView) linear.findViewById(R.id.imgProfilePic);
        txtUserName = (TextView) linear.findViewById(R.id.txtName);
        txtUserEmail = (TextView) linear.findViewById(R.id.txtEmail);


        mDrawerToggle = new ActionBarDrawerToggle(mActivity, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                mActivity.invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                mActivity.invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
        linear.setPadding(0, 0, 0, 10);
        mDrawerList.addHeaderView(linear);
        DrawerAdapter drawerAdapter = new DrawerAdapter(mActivity, mDrawerItemsList);
        mDrawerList.setAdapter(drawerAdapter);
        /*mDrawerList.setAdapter(new ArrayAdapter<String>(activity,
                android.R.layout.simple_list_item_1, mDrawerItemsList));*/
        mDrawerList.setOnItemClickListener(this);
        onItemClick(null, null, 2, 0);
    }

    public void loadUserData() {

        try {

            if (!TextUtils.isEmpty(App.getInstance().getLoggedInUser().getProfilePic())) {

                Glide.with(mActivity)
                        .load(App.getInstance().getLoggedInUser().getProfilePic())
                        .centerCrop()
                        .crossFade()
                        .into(imgProfilePic);
            }

            if (!TextUtils.isEmpty(App.getInstance().getLoggedInUser().getFirstName())) {
                txtUserName.setText(App.getInstance().getLoggedInUser().getFirstName());
            }

            if (!TextUtils.isEmpty(App.getInstance().getLoggedInUser().getEmail())) {
                txtUserEmail.setText(App.getInstance().getLoggedInUser().getEmail());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {

            case 1:
                mActivity.startActivity(new Intent(mActivity, ProfileSetUpActivity.class));
                break;

            case 2:
                /*mActivity.fragmentTransaction(HomeActivity.REPLACE_FRAGMENT,
                        new ContactsFragment(), R.id.frameContainer, false);*/
                break;
        }
        mDrawerLayout.closeDrawer(Gravity.START);
    }
}
