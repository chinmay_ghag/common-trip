package com.drive.share.receiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.drive.share.globals.utils.NotificationHelper;
import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Handling of GCM messages.
 */
public class GcmBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //Toast.makeText(context, "Data received!", Toast.LENGTH_LONG).show();


        try {

            if (intent.getExtras().getString("message") != null) {
                String msg = intent.getExtras().getString("message").toString();

                Log.d("Data Received !", "" + msg);

                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);

                String messageType = gcm.getMessageType(intent);

                Log.d("Message Type ", "" + messageType);

                if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                    //Error
                    NotificationHelper.sendNotification(context, "Send error: " + intent.getExtras().toString());

                } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                    //Delete
                    NotificationHelper.sendNotification(context, "Deleted messages on server: " +
                            intent.getExtras().toString());
                } else {
                    //Success
                    NotificationHelper.sendNotification(context, msg);
                }
                setResultCode(Activity.RESULT_OK);
            } else {
               // NotificationHelper.sendNotification(context, "message");
            }
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        } catch (Exception npe) {
            npe.printStackTrace();
        }
    }

}