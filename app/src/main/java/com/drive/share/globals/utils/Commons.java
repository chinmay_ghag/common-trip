package com.drive.share.globals.utils;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.drive.share.globals.App;
import com.drive.share.globals.Constants;
import com.drive.share.sharedrive.R;
import com.drive.share.storage.PreferenceUtils;

import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by yashesh on 20/2/15.
 */
public class Commons {


    public static final String getUnixTime() {

        return (System.currentTimeMillis() / 1000) + "";

    }

    public static void startServiceIfItsNotRuning(Class<?> class1, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (class1.getName().equals(service.service.getClassName())) {
                //    Log.d("servisstart SERVICE ALREADY START" + class1.getName());
                return;
            }
        }
        //   Log.d("servisstart SSERVICE NEW SERVIS " + class1.getName());
        context.startService(new Intent(context, class1));
    }


    /**
     * Gets the current date key.
     *
     * @return the current date key
     */
    public static String getCurrentDate() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss", Locale.UK);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date current = new Date(System.currentTimeMillis());
        return format.format(current);
    }

    public static String getFormattedDate(String date) {

        String year = date.substring(0, 3);
        String month = date.substring(4, 5);
        String day = date.substring(6, 7);

        Calendar cal = Calendar.getInstance();
        cal.set(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day));
        Date today = cal.getTime();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        // (3) create a new String using the date format we want
        String folderName = formatter.format(today);

        // (4) this prints "Folder Name = 2009-09-06-08.23.23"
        System.out.println("Folder Name = " + folderName);
        return folderName;


    }


    public static String getCurrentTime() {
        SimpleDateFormat format = new SimpleDateFormat("HHmmss", Locale.UK);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date current = new Date(System.currentTimeMillis());
        return format.format(current);
    }

    public static void getShaKey() {
        PackageInfo info;
        try {
            info = App.getInstance().getPackageManager().getPackageInfo(
                    App.getInstance().getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    /**
     * This method is used to provide standard contact display irrelevant of contact saved in mobile phones.
     * <p/>
     * Cases covered
     * <p/>
     * if number starts with 0, then remove that 0 and append country code
     * if number starts with 00, the leave the number untouched
     * Standard format to maintain for contacts is +91 123132133312
     * i.e. countryCode+space+number
     * *
     */
    public static String getValidatedPhoneNumber(String number) {


        /*String validNumber = "";

        if (TextUtils.isEmpty(validNumber)) {
            return validNumber;
        }


        if ((number.startsWith("00")) || (number.startsWith("+")) || (number.startsWith(PreferenceUtils.getString(Constants.KEY_COUNTRY_CODE)))) {
            validNumber = number;
        } else {

            number.replaceAll("/[^0-9]/", number);
            validNumber = getAppendedContactNumber(number);
        }*/

        if (number.contains("-")) {
            number = number.replaceAll("-", "");
        }
        if (number.contains(" ")) {
            number = number.replaceAll(" ", "");
        }
        if (number.startsWith("0") && !number.startsWith("00")) {
            number = number.substring(1);
        }
        if (!number.startsWith("00")) {
            if (number.startsWith(PreferenceUtils.getString(Constants.KEY_COUNTRY_CODE))) {
                number = PreferenceUtils.getString(Constants.KEY_COUNTRY_CODE) + " " + number.substring(PreferenceUtils.getString(Constants.KEY_COUNTRY_CODE).length(), number.length());
            } else if (number.startsWith(PreferenceUtils.getString(Constants.KEY_COUNTRY_CODE).replace("+", ""))) {
                number = PreferenceUtils.getString(Constants.KEY_COUNTRY_CODE) + " " + number.substring(PreferenceUtils.getString(Constants.KEY_COUNTRY_CODE).replace("+", "").length(), number.length());
            } else {
                number = PreferenceUtils.getString(Constants.KEY_COUNTRY_CODE) + " " + number;
            }
        }
        return number;

    }


    public static String getFormattedContactNumber(String unformattedNumber) {


        //remove space if any
        if (!TextUtils.isEmpty(unformattedNumber)) {
            unformattedNumber.trim();
            unformattedNumber.replace(" ", "");
        }

        /*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return PhoneNumberUtils.formatNumber(unformattedNumber);
        } else {
            TelephonyManager tm = (TelephonyManager) App.getInstance().getSystemService(Context.TELEPHONY_SERVICE);
            String countryCode = tm.getSimCountryIso();

            return PhoneNumberUtils.formatNumber(unformattedNumber, countryCode);
        }*/

        return unformattedNumber;
    }

    public static void hideKeyBoard(Context context, View view) {

        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showKeyBoard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, 0);
    }

    public static void appShareIntent(Context context) {
        Intent sendIntent = new Intent();


        sendIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "CommonTrip");
        sendIntent.putExtra(Intent.EXTRA_TEXT, "market://details?id=" + context.getPackageName());
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

    public static void makeActionOverflowMenuShown(Context context) {
        //devices with hardware menu button (e.g. Samsung Note) don't show action overflow menu
        try {
            ViewConfiguration config = ViewConfiguration.get(context);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            Log.d("OverFlow", e.getLocalizedMessage());
        }
    }

    public static final boolean VIEW_GONE = true;
    public static final boolean VIEW_VISIBLE = false;

    public static void setItemData(TextView tv, String text, String defaultText) {
        if (!TextUtils.isEmpty(text)) {
            tv.setText(text);
        } else {
            if (TextUtils.isEmpty(defaultText)) {
                defaultText = "";
            }
            tv.setText(defaultText);
        }
    }

    public static void setItemData(TextView tv, String text, boolean hide) {
        if (!TextUtils.isEmpty(text)) {
            tv.setText(text);
        } else if (hide) {
            tv.setVisibility(View.GONE);
        }
    }

    public static String appendKM(String text) {
        return text + "Km";
    }

    public static String appendZero(int val) {
        if (val < 10) {
            return "0" + val;
        } else {
            return "" + val;
        }
    }

    public static String appendZero(String val) {
        try {
            if (Integer.parseInt(val) < 10) {
                return "0" + val;
            } else {
                return "" + val;
            }
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }

        return "";
    }

    public static String appendPassenger(String text) {
        return text + " Passenger";
    }

    public static Object getFormattedAsyncContact(Object data) {


        /*for (UserModel contact : ((ContactSyncModel) data).getLstSyncData()) {
            try {
                contact.setMobNumber(contact.getMobNumber().replaceFirst(contact.getCountryCode(), ""));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/

        return data;
    }

    public static void errorNoInternet(Context context) {
        Toast.makeText(context, context.getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
    }

    public static void toastShort(Context context, String string) {
        Toast.makeText(context, string, Toast.LENGTH_SHORT).show();
    }

    public static void toastLong(Context context, String string) {
        Toast.makeText(context, string, Toast.LENGTH_LONG).show();
    }

    public static void setDialogFullScreen(Context context, Dialog dialog) {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public static int getCurrentHour() {
        return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
    }

    public static int getCurrentMinute() {
        return Calendar.getInstance().get(Calendar.MINUTE);
    }

    public static String getFormattedTime(int hour, int min) {
        String format = "";
        if (hour == 0) {
            hour += 12;
            format = "AM";
        } else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }

        return new StringBuilder().append(appendZero(hour)).append(" : ").append(appendZero(min))
                .append(" ").append(format).toString();
    }


}
