package com.drive.share.globals;

import android.app.Application;
import android.util.Log;

import com.android.utilities.Validation;
import com.android.utilities.datareader.DataReader;
import com.crittercism.app.Crittercism;
import com.drive.share.businesslayer.ContactLogic;
import com.drive.share.data.webapi.ContactSyncModel;
import com.drive.share.data.webapi.TripModel;
import com.drive.share.data.webapi.UserModel;
import com.drive.share.data.webapi.VehicleModel;
import com.drive.share.globals.utils.Commons;
import com.drive.share.services.ContactObserverService;
import com.drive.share.storage.PreferenceUtils;
import com.opendroid.db.DbConfiguration;
import com.opendroid.db.DbHelper;
import com.opendroid.db.DbModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashesh on 5/2/15.
 */
public class App extends Application implements DataReader.DataClient {
    public static App instance;
    private String mAppUserId = null;
    private UserModel model = null;


    public static synchronized App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // initialize singleton.
        instance = this;


        Crittercism.initialize(getApplicationContext(), "54fee616e0697fa4496374e2");


        Log.d("patrol", "onCreate()--> APP-->");
        // initialize DAO configuration
        List<DbModel> lstDbModels = new ArrayList<DbModel>();
        lstDbModels.add(new UserModel());
        lstDbModels.add(new VehicleModel());
        lstDbModels.add(new TripModel());
        DbConfiguration dbConfiguration = new DbConfiguration.Builder().
                setDatabaseName("commontrip.db").setModels(lstDbModels).build();
        DbHelper.init(this, dbConfiguration);


        // sync contacts.
        if (getAppUserId() != null && Validation.Network.isConnected(this)) {
            try {
                model = (UserModel) PreferenceUtils.
                        getObject(PreferenceUtils.KEY_USER_MODEL, UserModel.class);
                // default resync for getting server updates.

                Log.d("resync", "resyncing");
                ContactLogic.resyncContact(null, this);
            } catch (Exception e) {
                e.printStackTrace();
            }


            // startService(new Intent(this, ContactSyncService.class));
        }else{
            Log.d("resync", "error resyncing");
        }

        // start contact observer service if not running.
        Commons.startServiceIfItsNotRuning(ContactObserverService.class, this);

        //Shows Overflow icon
        Commons.makeActionOverflowMenuShown(this);


    }

    public void setLoggedInUser(UserModel model) {
        this.model = model;
    }

    public UserModel getLoggedInUser() {
        return model;
    }

    public String getAppUserId() {
        if (mAppUserId == null) {
            mAppUserId = PreferenceUtils.getString(PreferenceUtils.KEY_USER_ID_UNIQUE);
        }
        return mAppUserId;
    }

    /**
     * On data received.
     *
     * @param readRequestCode the read request code
     * @param data            the data
     */
    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        if (data != null) {

            switch (readRequestCode) {

                case ContactLogic.REQ_UPLOAD_CONTACTS:

                    Log.d("resync", "resync finished " + ((ContactSyncModel) data).getLstSyncData().size());

                    ContactLogic.processContactSyncData((ContactSyncModel) Commons.getFormattedAsyncContact(data));


                    break;
                case Constants.DEVICE_DETAILS:
                    Log.d("Gcm", "App registered " + data.toString());
                    break;

            }


        }
    }

    /**
     * On data empty.
     *
     * @param readRequestCode the read request code
     */
    @Override
    public void onDataEmpty(int readRequestCode) {

    }

    /**
     * On error.
     *
     * @param readRequestCode the read request code
     * @param error
     */
    @Override
    public void onError(int readRequestCode, Object error) {
        switch (readRequestCode) {

            case ContactLogic.REQ_UPLOAD_CONTACTS:

                Log.d("Error", error.toString());
                break;
            case Constants.DEVICE_DETAILS:
                Log.d("Error", error.toString());
                break;
        }
    }

    @Override
    public boolean needResponse() {
        return true;
    }
}
