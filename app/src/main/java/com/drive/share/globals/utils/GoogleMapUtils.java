package com.drive.share.globals.utils;

import static java.lang.Math.asin;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

import java.util.ArrayList;

import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Property;


import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;


// TODO: Auto-generated Javadoc
/**
 * The Class GoogleMapUtils.
 */
public class GoogleMapUtils {

	/**
	 * Gets the location bounds.
	 * 
	 * @param map
	 *            the map
	 * @return the location bounds
	 */
	public static double[] getLocationBounds(GoogleMap map) {

		double[] rangeArray = new double[6];

		VisibleRegion region = map.getProjection().getVisibleRegion();

		LatLngBounds bounds = region.latLngBounds;
		LatLng ltlng = bounds.northeast;
		rangeArray[0] = ltlng.latitude;
		rangeArray[1] = ltlng.longitude;

		ltlng = bounds.southwest;
		rangeArray[2] = ltlng.latitude;
		rangeArray[3] = ltlng.longitude;

		ltlng = bounds.getCenter();
		rangeArray[4] = ltlng.latitude;
		rangeArray[5] = ltlng.longitude;

		
		return rangeArray;
	}

	public static final boolean regonContainsPoint(GoogleMap map,LatLng point){
		VisibleRegion region = map.getProjection().getVisibleRegion();

		return region.latLngBounds.contains(point);
	}
	
	/**
	 * Sets the geo fence.
	 *
	 * @param centerLatLng the center lat lng
	 * @param map the map
	 */
	public static void setGeoFence(Context context,LatLng centerLatLng,GoogleMap map) {
		  
		  CircleOptions circleOpt = new CircleOptions()
		  .center(centerLatLng)
		  .radius(2000)
		  .fillColor(Color.TRANSPARENT)
		  .strokeColor(Color.BLUE)
		  .strokeWidth(7);
		  map.addCircle(circleOpt);
		 
		 }
		
	
	
	
	/**
	 * Move camera.
	 *
	 * @param map the map
	 * @param latitude the latitude
	 * @param longitude the longitude
	 * @param zoomLevel the zoom level
	 */
	public static void moveCamera(GoogleMap map, double latitude,
			double longitude,float zoomLevel) {

		CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(
				latitude, longitude), zoomLevel);
		map.animateCamera(update);

	}
	/**
	 * Move camera.
	 *
	 * @param map the map
	 * @param latitude the latitude
	 * @param longitude the longitude
	 *
	 */
	public static void moveCameraWithotZoom(GoogleMap map, double latitude,
			double longitude) {

		CameraUpdate update = CameraUpdateFactory.newLatLng(new LatLng(
				latitude, longitude));
		map.animateCamera(update);

	}
	
	public static void zoomCamera(GoogleMap map, 
			float zoomLeval) {

		CameraUpdate update = CameraUpdateFactory.zoomTo(zoomLeval);
		map.moveCamera(update);

	}
	
	/**
	 * Move camera static.
	 *
	 * @param map the map
	 * @param latitude the latitude
	 * @param longitude the longitude
	 * @param zoomLevel the zoom level
	 */
	public static void moveCameraStatic(GoogleMap map, double latitude,
			double longitude,float zoomLevel) {

		CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(
				latitude, longitude), zoomLevel);
		map.moveCamera(update);

	}
	
	/**
	 * Draw marker.
	 *
	 * @param map the map
	 * @param id the id
	 * @param title the title
	 * @param snippet the snippet
	 * @param markerResource the marker resource
	 * @param latitude the latitude
	 * @param longitude the longitude
	 * @return the marker
	 */
	public static Marker drawMarker(GoogleMap map,String id, String title,
			String snippet, int markerResource, double latitude,
			double longitude) {

		MarkerOptions options = new MarkerOptions();
	
		options.title(title);
		options.snippet(snippet);
		options.icon(BitmapDescriptorFactory.fromResource(markerResource));
		options.position(new LatLng(latitude, longitude));
		return map.addMarker(options);
	}
	/**
	 * Draw marker.
	 *
	 * @param map the map
	 * @param id the id
	 * @param title the title
	 * @param snippet the snippet
	 *
	 * @param latitude the latitude
	 * @param longitude the longitude
	 * @return the marker
	 */
	public static Marker drawMarker(GoogleMap map,String id, String title,
			String snippet,  double latitude,
			double longitude) {

		MarkerOptions options = new MarkerOptions();
	
		options.title(title);
		options.snippet(snippet);
		options.icon(BitmapDescriptorFactory.defaultMarker());
		options.position(new LatLng(latitude, longitude));
		return map.addMarker(options);
	}
	/**
	 * Draw marker.
	 *
	 * @param map the map
	 * @param id the id
	 * @param title the title
	 * @param snippet the snippet
	 * @param markerResource the marker resource
	 * @param latitude the latitude
	 * @param longitude the longitude
	 * @return the marker
	 */
	public static Marker drawMarker(GoogleMap map,String id, String title,
			String snippet, Bitmap markerResource, double latitude,
			double longitude) {

		MarkerOptions options = new MarkerOptions();
	
		options.title(title);
		options.snippet(snippet);
		//options.anchor(0.5f, 0.5f);
		options.icon(BitmapDescriptorFactory.fromBitmap(markerResource));
		options.position(new LatLng(latitude, longitude));
		return map.addMarker(options);
	}
	/**
	 * The Class MarkerAnimation.
	 * 
	 * @author Chris Broadfoot (Android Googlemap team)
	 * 
	 * 
	 *         animates the map with its latlong.using propertyanimation api.
	 */
	public static class MarkerAnimation {
		
		  /*public static void animateMarkerToGB(final Marker marker, final
		  LatLng finalPosition, final LatLngInterpolator latLngInterpolator) {
			  
		  final LatLng startPosition = marker.getPosition(); final Handler
		  handler = new Handler(); 
		  final long start =SystemClock.uptimeMillis(); final Interpolator interpolator = new
		  AccelerateDecelerateInterpolator(); final float durationInMs = 3000;
		 
		  handler.post(new Runnable() 
		  { long elapsed; float t; float v;
		  
		 @Override public void run() { 
		// Calculate progress using
		  interpolator elapsed = SystemClock.uptimeMillis() - start; 
		  t =elapsed / durationInMs; 
		  v = interpolator.getInterpolation(t);
		  
		  marker.setPosition(latLngInterpolator.interpolate(v, startPosition,
		  finalPosition));
		  
		  // Repeat till progress is complete. 
		// Post again 16ms
		  if (t < 1) { 
		 later. handler.postDelayed(this, 16); } } }); 
		  }*/
	/*
		 * @TargetApi(Build.VERSION_CODES.HONEYCOMB) static void
		 * animateMarkerToHC(final Marker marker, final LatLng finalPosition,
		 * final LatLngInterpolator latLngInterpolator) { final LatLng
		 * startPosition = marker.getPosition();
		 * 
		 * ValueAnimator valueAnimator = new ValueAnimator();
		 * valueAnimator.addUpdateListener(new
		 * ValueAnimator.AnimatorUpdateListener() {
		 * 
		 * @Override public void onAnimationUpdate(ValueAnimator animation) {
		 * float v = animation.getAnimatedFraction(); LatLng newPosition =
		 * latLngInterpolator.interpolate(v, startPosition, finalPosition);
		 * marker.setPosition(newPosition); } });
		 * valueAnimator.setFloatValues(0, 1); // Ignored.
		 * valueAnimator.setDuration(3000); valueAnimator.start(); }
		 */

		/**
		 * Animate marker to ics.
		 * 
		 * @param marker
		 *            the marker
		 * @param finalPosition
		 *            the final position
		 * @param latLngInterpolator
		 *            the lat lng interpolator
		 */
		// @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		public static void animateMarkerToICS(final Marker marker,
				final LatLng finalPosition,
				final LatLngInterpolator latLngInterpolator) {
			
					TypeEvaluator<LatLng> typeEvaluator = new TypeEvaluator<LatLng>() {
						@Override
						public LatLng evaluate(float fraction,
								LatLng startValue, LatLng endValue) {
							return latLngInterpolator.interpolate(fraction,
									startValue, endValue);
						}
					};
					Property<Marker, LatLng> property = Property.of(
							Marker.class, LatLng.class, "position");
					ObjectAnimator animator = ObjectAnimator.ofObject(marker,
							property, typeEvaluator, finalPosition);
					// animator.setInterpolator(new AccelerateInterpolator());
					animator.setDuration(450);
					animator.start();

			

		}

	}

	/**
	 * The Interface LatLngInterpolator.
	 * 
	 * @author Chris Broadfoot (Android Googlemap team)
	 */
	public interface LatLngInterpolator {

		/**
		 * Interpolate.
		 * 
		 * @param fraction
		 *            the fraction
		 * @param a
		 *            the a
		 * @param b
		 *            the b
		 * @return the lat lng
		 */
		public LatLng interpolate(float fraction, LatLng a, LatLng b);

		/**
		 * The Class Linear.
		 */
		public static class Linear implements LatLngInterpolator {

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * com.msgps.utils.GoogleMapUtils.LatLngInterpolator#interpolate
			 * (float, com.google.android.gms.maps.model.LatLng,
			 * com.google.android.gms.maps.model.LatLng)
			 */
			@Override
			public LatLng interpolate(float fraction, LatLng a, LatLng b) {
				double lat = (b.latitude - a.latitude) * fraction + a.latitude;
				double lng = (b.longitude - a.longitude) * fraction
						+ a.longitude;
				return new LatLng(lat, lng);
			}
		}

		/**
		 * The Class LinearFixed.
		 */
		public static class LinearFixed implements LatLngInterpolator {

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * com.msgps.utils.GoogleMapUtils.LatLngInterpolator#interpolate
			 * (float, com.google.android.gms.maps.model.LatLng,
			 * com.google.android.gms.maps.model.LatLng)
			 */
			@Override
			public LatLng interpolate(float fraction, LatLng a, LatLng b) {
				double lat = (b.latitude - a.latitude) * fraction + a.latitude;
				double lngDelta = b.longitude - a.longitude;

				// Take the shortest path across the 180th meridian.
				if (Math.abs(lngDelta) > 180) {
					lngDelta -= Math.signum(lngDelta) * 360;
				}
				double lng = lngDelta * fraction + a.longitude;
				return new LatLng(lat, lng);
			}
		}

		/**
		 * The Class Spherical.
		 */
		public static class Spherical implements LatLngInterpolator {

			/* From github.com/googlemaps/android-maps-utils */
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * com.msgps.utils.GoogleMapUtils.LatLngInterpolator#interpolate
			 * (float, com.google.android.gms.maps.model.LatLng,
			 * com.google.android.gms.maps.model.LatLng)
			 */
			@Override
			public LatLng interpolate(float fraction, LatLng from, LatLng to) {
				// http://en.wikipedia.org/wiki/Slerp
				double fromLat = toRadians(from.latitude);
				double fromLng = toRadians(from.longitude);
				double toLat = toRadians(to.latitude);
				double toLng = toRadians(to.longitude);
				double cosFromLat = cos(fromLat);
				double cosToLat = cos(toLat);

				// Computes Spherical interpolation coefficients.
				double angle = computeAngleBetween(fromLat, fromLng, toLat,
						toLng);
				double sinAngle = sin(angle);
				if (sinAngle < 1E-6) {
					return from;
				}
				double a = sin((1 - fraction) * angle) / sinAngle;
				double b = sin(fraction * angle) / sinAngle;

				// Converts from polar to vector and interpolate.
				double x = a * cosFromLat * cos(fromLng) + b * cosToLat
						* cos(toLng);
				double y = a * cosFromLat * sin(fromLng) + b * cosToLat
						* sin(toLng);
				double z = a * sin(fromLat) + b * sin(toLat);

				// Converts interpolated vector back to polar.
				double lat = atan2(z, sqrt(x * x + y * y));
				double lng = atan2(y, x);
				return new LatLng(toDegrees(lat), toDegrees(lng));
			}

			/**
			 * Compute angle between.
			 * 
			 * @param fromLat
			 *            the from lat
			 * @param fromLng
			 *            the from lng
			 * @param toLat
			 *            the to lat
			 * @param toLng
			 *            the to lng
			 * @return the double
			 */
			private double computeAngleBetween(double fromLat, double fromLng,
					double toLat, double toLng) {
				// Haversine's formula
				double dLat = fromLat - toLat;
				double dLng = fromLng - toLng;
				return 2 * asin(sqrt(pow(sin(dLat / 2), 2) + cos(fromLat)
						* cos(toLat) * pow(sin(dLng / 2), 2)));
			}
		}
	}

	/**
	 * Gets the top position.
	 *
	 * @param map the map
	 * @return the top position
	 */
	public static final double getTopPosition(GoogleMap map) {

		return map.getProjection().getVisibleRegion().farLeft.latitude;

	}

	/**
	 * The Class LocationUpdater.
	 */
	public class LocationUpdater implements LocationListener,LocationSource{
		
		/** The location changed listener. */
		private OnLocationChangedListener locationChangedListener;
		
		/** The map. */
		private GoogleMap map;
		

		/**
		 * Gets the location changed listener.
		 *
		 * @return the location changed listener
		 */
		public OnLocationChangedListener getLocationChangedListener() {
			return locationChangedListener;
		}

		/**
		 * Sets the location changed listener.
		 *
		 * @param locationChangedListener the new location changed listener
		 */
		public void setLocationChangedListener(
				OnLocationChangedListener locationChangedListener) {
			this.locationChangedListener = locationChangedListener;
		}

		/* (non-Javadoc)
		 * @see com.google.android.gms.maps.LocationSource#activate(com.google.android.gms.maps.LocationSource.OnLocationChangedListener)
		 */
		@Override
		public void activate(OnLocationChangedListener locationChangedListener) {
			this.locationChangedListener = locationChangedListener;
			
		}

		/* (non-Javadoc)
		 * @see com.google.android.gms.maps.LocationSource#deactivate()
		 */
		@Override
		public void deactivate() {
			this.locationChangedListener=null;
			
		}

		/* (non-Javadoc)
		 * @see android.location.LocationListener#onLocationChanged(android.location.Location)
		 */
		@Override
		public void onLocationChanged(Location location) {
			 if (locationChangedListener != null) {
				 locationChangedListener.onLocationChanged(location);

			      LatLng latlng=
			          new LatLng(location.getLatitude(), location.getLongitude());
			      CameraUpdate cu=CameraUpdateFactory.newLatLng(latlng);

			      map.animateCamera(cu);
			    }
			
		}

		/* (non-Javadoc)
		 * @see android.location.LocationListener#onProviderDisabled(java.lang.String)
		 */
		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see android.location.LocationListener#onProviderEnabled(java.lang.String)
		 */
		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see android.location.LocationListener#onStatusChanged(java.lang.String, int, android.os.Bundle)
		 */
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	/**
	 * Sets the zoom level.
	 *
	 * @param map the map
	 * @param zoomLevel the zoom level
	 */
	public static void setZoomLevel(GoogleMap map,float zoomLevel){
		map.animateCamera(CameraUpdateFactory.zoomTo(zoomLevel));
	}
	
	public static ArrayList<LatLng> makeCircle(GoogleMap map,LatLng centre, double radius) 
    { 
    ArrayList<LatLng> points = new ArrayList<LatLng>(); 

    double EARTH_RADIUS = 6378100.0; 
    // Convert to radians. 
    double lat = centre.latitude * Math.PI / 180.0; 
    double lon = centre.longitude * Math.PI / 180.0; 

    for (double t = 0; t <= Math.PI * 2; t += 0.3) 
    { 
    // y 
    double latPoint = lat + (radius / EARTH_RADIUS) * Math.sin(t); 
    // x 
    double lonPoint = lon + (radius / EARTH_RADIUS) * Math.cos(t) / Math.cos(lat);

    // saving the location on circle as a LatLng point
    LatLng point =new LatLng(latPoint * 180.0 / Math.PI, lonPoint * 180.0 / Math.PI);

    // here mMap is my GoogleMap object 
    map.addMarker(new MarkerOptions().position(point).icon(BitmapDescriptorFactory
			.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

    // now here note that same point(lat/lng) is used for marker as well as saved in the ArrayList 
    points.add(point);

    } 

    return points; 
    }
	
	
	
	public static Marker addInCircle(GoogleMap map,LatLng centre, double radius,double t,String title,String snippet) 
    { 
   // ArrayList<LatLng> points = new ArrayList<LatLng>(); 

    double EARTH_RADIUS = 6378100.0; 
    // Convert to radians. 
    double lat = centre.latitude * Math.PI / 180.0; 
    double lon = centre.longitude * Math.PI / 180.0; 
   // t += 0.3
    
    // y 
    double latPoint = lat + (radius / EARTH_RADIUS) * Math.sin(t); 
    // x 
    double lonPoint = lon + (radius / EARTH_RADIUS) * Math.cos(t) / Math.cos(lat);

    // saving the location on circle as a LatLng point
    LatLng point =new LatLng(latPoint * 180.0 / Math.PI, lonPoint * 180.0 / Math.PI);

    // here mMap is my GoogleMap object 
   

    // now here note that same point(lat/lng) is used for marker as well as saved in the ArrayList 
   // points.add(point);

   

    return  map.addMarker(new MarkerOptions().position(point).icon(BitmapDescriptorFactory
			.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).title(title).snippet(snippet).anchor(0.5f, 0.5f));
    }
	
}
