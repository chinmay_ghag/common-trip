package com.drive.share.globals.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.drive.share.businesslayer.DeviceDetailLogic;
import com.drive.share.globals.App;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.auth.AuthenticationActivity;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.sql.Timestamp;

/**
 * Contains constants used for push notification.
 */
public class NotificationHelper {

    /**
     * Constants used for GCM setup
     */
    public static final String SENDER_ID = "503869927042";//"250185011819"; //old sender id - "584998777190";
    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";
    public static final String PROPERTY_ON_SERVER_EXPIRATION_TIME = "onServerExpirationTimeMs";
    private static GoogleCloudMessaging gcm;
    private static String regid;

    private static String KEY_GCM = "gcm_id";

    /**
     * Default life span (7 days) of a reservation until it is considered
     * expired.
     */
    public static final long REGISTRATION_EXPIRY_TIME_MS = 1000 * 3600 * 24 * 7;

    // Put the GCM message into a notification and post it.
    public static void sendNotification(Context ctx, String msg) {
        NotificationManager mNotificationManager = (NotificationManager)
                ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0,
                new Intent(ctx, AuthenticationActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(ctx)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(ctx.getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg)
                        .setAutoCancel(true);

        /*Uri sound = Uri.parse("android.resource://" + ctx.getPackageName() + "/" + R.raw.book_page_turn);
        mBuilder.setSound(sound);*/

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify((int) System.currentTimeMillis(), mBuilder.build());
    }

    /**
     * **************
     * GCM initialisation
     * <p/>
     * Return Registration id
     * *******************
     */
    public static String initGCM(Context context) {
        regid = getRegistrationId(context);
        if (regid.length() == 0) {
            registerBackground(context);
        }
        gcm = GoogleCloudMessaging.getInstance(context);
        return regid;
    }


    /**
     * Gets the current registration id for application on GCM service.
     * <p/>
     * If result is empty, the registration has failed.
     *
     * @return registration id, or empty string if the registration is not
     * complete.
     */
    private static String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(NotificationHelper.PROPERTY_REG_ID, "");
        if (registrationId.length() == 0) {
            //ToastShort("Registration not found.");
            return "";
        }
        // check if app was updated; if so, it must clear registration id to
        // avoid a race condition if GCM sends a message
        int registeredVersion = prefs.getInt(NotificationHelper.PROPERTY_APP_VERSION,
                Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion || isRegistrationExpired(context)) {
            //ToastShort("App version changed or registration expired.");
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private static SharedPreferences getGCMPreferences(Context context) {
        return context.getSharedPreferences(KEY_GCM,
                Context.MODE_PRIVATE);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Checks if the registration has expired.
     * <p/>
     * <p/>
     * To avoid the scenario where the device sends the registration to the
     * server but the server loses it, the app developer may choose to
     * re-register after REGISTRATION_EXPIRY_TIME_MS.
     *
     * @return true if the registration has expired.
     */
    private static boolean isRegistrationExpired(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        // checks if the information is not stale
        long expirationTime = prefs.getLong(NotificationHelper.PROPERTY_ON_SERVER_EXPIRATION_TIME,
                -1);
        return System.currentTimeMillis() > expirationTime;
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p/>
     * Stores the registration id, app versionCode, and expiration time in the
     * application's shared preferences.
     */
    private static void registerBackground(final Context context) {

        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                String msg;
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(NotificationHelper.SENDER_ID);
                    msg = "Device registered, registration id=" + regid;
                    setRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                Log.d("Snehal", "msg:........" + msg);

                return msg;
            }

            @Override
            protected void onPostExecute(String result) {
                Log.d("Snehal", "Result:........" + result);
                //ToastLong(msg);
                super.onPostExecute(result);


                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        DeviceDetailLogic.sendDeviceDetails(App.getInstance(), regid);
                        return null;
                    }
                }.execute();


            }

        }.execute(null, null, null);

		/*new AsyncTask() {
            @Override
			protected String doInBackground(Object... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					regid = gcm.register(SENDER_ID);
					msg = "Device registered, registration id=" + regid;

					// You should send the registration ID to your server over
					// HTTP,
					// so it can use GCM/HTTP or CCS to send messages to your
					// app.

					// For this demo: we don't need to send it because the
					// device
					// will send upstream messages to a server that echo back
					// the message
					// using the 'from' address in the message.

					// Save the regid - no need to register again.
					setRegistrationId(context, regid);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(Object msg) {
				mDisplay.append(msg + "\n");
			}
		}.execute(null, null, null);*/
    }

    /**
     * Stores the registration id, app versionCode, and expiration time in the
     * application's {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration id
     */
    private static void setRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.d("app version ", "" + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(NotificationHelper.PROPERTY_REG_ID, regId);
        editor.putInt(NotificationHelper.PROPERTY_APP_VERSION, appVersion);
        long expirationTime = System.currentTimeMillis()
                + NotificationHelper.REGISTRATION_EXPIRY_TIME_MS;

        Log.d("expiry time to ", ""
                + new Timestamp(expirationTime));
        editor.putLong(NotificationHelper.PROPERTY_ON_SERVER_EXPIRATION_TIME, expirationTime);
        editor.commit();
    }

}
