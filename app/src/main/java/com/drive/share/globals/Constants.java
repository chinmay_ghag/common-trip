package com.drive.share.globals;

/**
 * Created by yashesh on 31/1/15.
 */
public class Constants {
    // numbers
    public static final int UI_HANDLER_STANDARD_DELAY_MS = 300;

    public static final int HANDLER_MSG_OK = 1;
    public static final int HANDLER_MSG_NOT_OK = 0;
    public static final int REQ_INVITE_USERS = 4565;
    public static final int REQ_LOAD_CONTACTS = 45;
    public static final int REQ_LOAD_TRIPS = 46;
    public static final int REQ_JOIN_TRIP = 47;
    public static final int REQ_THIRD_PARTY_DETAIL = 48;
    public static final int REQ_THIRD_PARTY_TRIPS = 49;
    public static final int REQ_VERIFICATION_CODE = 50;

    public static final int CAMERA_CAPTURE = 452;
    public static final int GALLERY_CAPTURE = 78;
    public static final int DEVICE_DETAILS = 7845;
    public static final int OTHER_USER_DETAILS = 54;

    public static final String KEY_COUNTRY_CODE = "countryCode";
    public static final String KEY_IS_SOURCE_ADDRESS = "source address";

    // strings
    public static final String WEB_BASE_URL = "http://ec2-54-169-228-14.ap-southeast-1.compute.amazonaws.com/commontrip/index.php/";
    public static final String METHOD_SIGN_UP = WEB_BASE_URL + "registration/usersignup";
    public static final String METHOD_UPLOAD_IMAGE = WEB_BASE_URL + "registration/uploadimage";
    public static final String METHOD_REGISTER_VEHICLE = WEB_BASE_URL + "vehicle/registervehical";
    public static final String METHOD_DEVICE_DETAILS = WEB_BASE_URL + "user/devicedetails";
    public static final String METHOD_CONTACTS_SYNC = WEB_BASE_URL + "/user/synccontacts";
    public static final String METHOD_TRIP_CREATE = WEB_BASE_URL + "trip/createtrip";
    public static final String METHOD_EMAIL_REGISTRATION = WEB_BASE_URL + "registration/emailregistration";
    public static final String METHOD_CONTACT_RESYNC = WEB_BASE_URL + "user/resynccontacts";
    public static final String METHOD_BLOCK_USERS = WEB_BASE_URL + "user/blockuser";
    public static final String METHOD_GET_TRIPS = WEB_BASE_URL + "trip/usertriplist";
    public static final String METHOD_FETCH_SYNC = WEB_BASE_URL + "user/fetchsyncedcontacts";
    public static final String METHOD_OTHERS_PROFILE = WEB_BASE_URL + "trip/otherprofileview";
    public static final String METHOD_INVITE_USERS = WEB_BASE_URL + "trip/invitefriend";
    public static final String METHOD_TRIP_DETAILS = WEB_BASE_URL + "trip/tripdetails";
    public static final String METHOD_JOIN_TRIP = WEB_BASE_URL + "trip/sendtripjoinrequest";
    public static final String METHOD_RESPOND_TO_REQUEST = WEB_BASE_URL + "trip/respondtorequest";
    public static final String METHOD_SEND_TRIP_ACTION  = WEB_BASE_URL + "trip/tripaction";

    public static final String METHOD_THIRD_PARTY_DETAILS = WEB_BASE_URL + "trip/otherprofileview";
    public static final String METHOD_THIRD_PARTY_TRIPS = WEB_BASE_URL + "trip/otherprofiletriplist";


    public static final String METHOD_REGISTER_VERFICATION_CODE = WEB_BASE_URL + "registration/smsverified";

    public static final String METHOD_LEAVE_TRIP = WEB_BASE_URL + "trip/tripleave";


    public static final String METHOD_USER_TRIP_LIST = WEB_BASE_URL + "trip/usertriplist";


}
