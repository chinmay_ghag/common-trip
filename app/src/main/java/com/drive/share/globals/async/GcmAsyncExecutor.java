package com.drive.share.globals.async;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.drive.share.businesslayer.DeviceDetailLogic;
import com.drive.share.globals.App;
import com.drive.share.globals.utils.GcmHelper;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

/**
 * Created by atulOholic on 24/2/15.
 */
public class GcmAsyncExecutor {

    private Activity mActivity;
    private String regId;
    private GoogleCloudMessaging gcm;

    public GcmAsyncExecutor(Activity activity) {
        mActivity = activity;


    }

    public void register() {

        /**
         * Registers the application with GCM servers asynchronously.
         * <p>
         * Stores the registration id, app versionCode, and expiration time in the
         * application's shared preferences.
         */

        if (GcmHelper.checkPlayServices(mActivity)) {


            if (regId.isEmpty()) {

                new AsyncTask<Void, Void, String>() {

                    @Override
                    protected String doInBackground(Void... params) {
                        String msg;
                        try {
                            if (gcm == null) {
                                gcm = GoogleCloudMessaging.getInstance(mActivity);
                            }

                            regId = gcm.register(GcmHelper.SENDER_ID);

                            msg = "Device registered, registration id=" + regId;

                            GcmHelper.setRegistrationId(regId);


                        } catch (IOException ex) {
                            msg = "Error :" + ex.getMessage();
                        }

                        return msg;
                    }

                    @Override
                    protected void onPostExecute(String result) {
                        super.onPostExecute(result);

                        DeviceDetailLogic.sendDeviceDetails(App.getInstance(), regId);
                    }

                }.execute(null, null, null);


            } else {
                DeviceDetailLogic.sendDeviceDetails(App.getInstance(), regId);
            }
            Log.i("HB GCM:", regId + " GCM ID.");
        } else {
            Log.i(getClass().getSimpleName(), "No valid Google Play Services APK found.");
        }


    }


}
