package com.drive.share.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.drive.share.sharedrive.R;
import com.drive.share.ui.fragments.tripcard.info.TripCardDetailsFragment;
import com.drive.share.ui.fragments.tripcard.info.TripCardPeopleFragment;

/**
 * Created by webwerks on 29/1/15.
 */
public class TripCardInfoPagerAdapter extends FragmentPagerAdapter {


    private final int ITEM_COUNT = 2, DETAILS_SCREEN = 0, PEOPLE_SCREEN = 1;
    private final Integer[] titles = new Integer[]{R.string.screen_summary, R.string.screen_people};


    Context mContext;

    public TripCardInfoPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment screen = null;
        switch (position) {

            case DETAILS_SCREEN:
                screen = new TripCardDetailsFragment();
                break;
            case PEOPLE_SCREEN:
                screen = new TripCardPeopleFragment();
                break;
            default:

                break;


        }

        return screen;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(titles[position]);
    }

    @Override
    public int getCount() {
        return ITEM_COUNT;
    }
}
