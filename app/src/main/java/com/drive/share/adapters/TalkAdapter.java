package com.drive.share.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.drive.share.sharedrive.R;
import com.drive.share.ui.fragments.tripcard.ChatElement;

import java.util.ArrayList;
import java.util.List;

public class TalkAdapter extends ArrayAdapter<ChatElement> {

	private TextView tv_chatText;
	private List<ChatElement> countries = new ArrayList<ChatElement>();
	private LinearLayout wrapper;

	@Override
	public void add(ChatElement object) {
		countries.add(object);
		super.add(object);
	}

	public TalkAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
	}

	public int getCount() {
		return this.countries.size();
	}

	public ChatElement getItem(int index) {
		return this.countries.get(index);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.talk_list_item, parent, false);
		}

		wrapper = (LinearLayout) row.findViewById(R.id.wrapper);

        ChatElement coment = getItem(position);

		tv_chatText = (TextView) row.findViewById(R.id.comment);

		tv_chatText.setText(coment.comment);

		tv_chatText.setBackgroundResource(coment.left ? R.drawable.bubble_yellow : R.drawable.bubble_green);
		wrapper.setGravity(coment.left ? Gravity.LEFT : Gravity.RIGHT);

		return row;
	}

	public Bitmap decodeToBitmap(byte[] decodedByte) {
		return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
	}

}