package com.drive.share.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.drive.share.data.DrawerItem;
import com.drive.share.sharedrive.R;

import java.util.List;

/**
 * Created by webwerks on 28/2/15.
 */
public class DrawerAdapter extends ArrayAdapter<DrawerItem> {

    Context context;
    List<DrawerItem> drawerItemList;

    public DrawerAdapter(Context context,List<DrawerItem> listItems) {
        super(context, 0, listItems);
        this.context = context;
        this.drawerItemList = listItems;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        DrawerItemHolder drawerHolder=null;

        if (convertView == null) {
            convertView= LayoutInflater.from(context).inflate(R.layout.drawer_list_item,null);
            drawerHolder = new DrawerItemHolder();
            drawerHolder.lblItemName=(TextView) convertView.findViewById(R.id.lblTitle);
            drawerHolder.icon=(ImageView) convertView.findViewById(R.id.imgIcon);
            drawerHolder.lblCount=(TextView)convertView.findViewById(R.id.lblCount);
            convertView.setTag(drawerHolder);
        } else {
            drawerHolder = (DrawerItemHolder) convertView.getTag();
        }

        DrawerItem dItem = (DrawerItem) this.drawerItemList.get(position);
        drawerHolder.icon.setImageDrawable(convertView.getResources().getDrawable(dItem.getImgRes()));
        drawerHolder.lblItemName.setText(dItem.getName());

        if(dItem.getcountVisibility()){
            drawerHolder.lblCount.setVisibility(View.VISIBLE);
        }else{
            drawerHolder.lblCount.setVisibility(View.GONE);
        }

        return convertView;
    }

    private static class DrawerItemHolder {
        TextView lblItemName,lblCount;
        ImageView icon;
    }
}
