package com.drive.share.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.drive.share.sharedrive.R;
import com.drive.share.ui.fragments.home.ContactsFragment;
import com.drive.share.ui.fragments.home.TripsFragment;

/**
 * Created by webwerks on 29/1/15.
 */
public class HomePagerAdapter extends FragmentPagerAdapter {


    private final int ITEM_COUNT = 2, HOME_SCREEN = 0, TRIP_SCREEN = 1;
    private final Integer[] titles = new Integer[]{R.string.screen_home, R.string.screen_trip};


    Context mContext;

    public HomePagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment screen = null;
        switch (position) {

            case HOME_SCREEN:
                screen = new ContactsFragment();
                break;
            case TRIP_SCREEN:
                screen = new TripsFragment();
                break;
            default:

                break;


        }

        return screen;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(titles[position]);
    }

    @Override
    public int getCount() {
        return ITEM_COUNT;
    }
}
