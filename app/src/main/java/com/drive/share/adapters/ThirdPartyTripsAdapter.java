package com.drive.share.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.drive.share.data.webapi.TripModel;
import com.drive.share.globals.utils.Commons;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.tripcard.TripInviteActivity;

import java.util.List;

/**
 * Created by yashesh on 2/2/15.
 */
public class ThirdPartyTripsAdapter extends ArrayAdapter<TripModel> {

    private Context mContext;

    public ThirdPartyTripsAdapter(Context context, List<TripModel> objects) {
        super(context, 0, 0, objects);
        mContext = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.thirdparty_trip_list, null);
            holder = new ViewHolder();
            holder.txtTripTitle = (TextView) convertView.findViewById(R.id.txtTripTitle);
            holder.txtRole = (TextView) convertView.findViewById(R.id.txtRole);
            holder.txtPassengerCount = (TextView) convertView.findViewById(R.id.txtPassengerCount);
            holder.txtTripDistance = (TextView) convertView.findViewById(R.id.txtDistance);
            holder.imgTripImage = (ImageView) convertView.findViewById(R.id.imgTripImage);
            holder.btnInvite = (Button) convertView.findViewById(R.id.btnInvite);
            holder.btnInvite.setOnClickListener(myButtonClickListener);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        TripModel tripResponse = getItem(position);

        Commons.setItemData(holder.txtTripTitle, tripResponse.getTrip_name(), "trip " + (position + 1));

        Commons.setItemData(holder.txtRole, tripResponse.getTripUserRole(), Commons.VIEW_GONE);

        Commons.setItemData(holder.txtPassengerCount, tripResponse.getTripTotalPassenger(), Commons.VIEW_GONE);

        Commons.setItemData(holder.txtTripDistance, tripResponse.getTripDistance(), Commons.VIEW_GONE);


        return convertView;
    }

    private View.OnClickListener myButtonClickListener = new View.OnClickListener() {

        @Override

        public void onClick(View v) {


            inviteOnClickHandler(v);

        }

    };


    public void inviteOnClickHandler(View view) {
        Intent intent = new Intent(mContext, TripInviteActivity.class);
        mContext.startActivity(intent);
    }


    class ViewHolder {
        TextView txtTripTitle, txtRole, txtPassengerCount, txtTripDistance;
        ImageView imgTripImage;
        Button btnInvite;
    }


}
