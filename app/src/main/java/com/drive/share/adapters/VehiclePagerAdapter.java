package com.drive.share.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.drive.share.data.webapi.VehicleModel;
import com.drive.share.sharedrive.R;

import java.util.List;

/**
 * Created by raj on 17/2/15.
 */

public class VehiclePagerAdapter extends PagerAdapter{

    Context context;
    List<VehicleModel> vehicleModelList;

   // String[] types={"vehicle 1","vehicle 2","vehicle 3","vehicle 4"};

    public VehiclePagerAdapter(Context context,List<VehicleModel> vehicles){
           this.context=context;

           this.vehicleModelList=vehicles;
    }

    @Override
    public int getCount() {
        //return vehicleModelList.size();
        return vehicleModelList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View pagerLayout= LayoutInflater.from(context).inflate(R.layout.vehicle_type_pager,null);
        TextView lblType=(TextView)pagerLayout.findViewById(R.id.lblVehicleType);
        lblType.setText(vehicleModelList.get(position).getVehicle_name());
        ((ViewPager)container).addView(pagerLayout, 0);
        return pagerLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }
}
