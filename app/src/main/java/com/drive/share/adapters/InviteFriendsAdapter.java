package com.drive.share.adapters;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.drive.share.data.webapi.UserModel;
import com.drive.share.globals.utils.Commons;
import com.drive.share.sharedrive.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yashesh on 2/2/15.
 */
public class InviteFriendsAdapter extends ArrayAdapter<UserModel> {

    private Context mContext;

    public InviteFriendsAdapter(Context context, List<UserModel> objects) {
        super(context, 0, 0, objects);
        mContext = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_invite_friend, null);
            holder = new ViewHolder();
            holder.txtFriendName = (TextView) convertView.findViewById(R.id.txtFriendName);
//            holder.chkAddFriend = (CheckBox) convertView.findViewById(R.id.chkAddFriend);
            holder.imgFriend = (CircleImageView) convertView.findViewById(R.id.imgFriends);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        UserModel friendModel = getItem(position);

        holder.chkAddFriend.setTag(position);

        holder.chkAddFriend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int pos = Integer.parseInt(buttonView.getTag().toString());
                getItem(pos).setInvited(isChecked);
            }
        });

        Commons.setItemData(holder.txtFriendName, friendModel.getFirstName(), null);

        if (!TextUtils.isEmpty(friendModel.getProfilePic())) {
            if (friendModel.getProfilePic().startsWith("http")) {
                Glide.with(mContext)
                        .load(friendModel.getProfilePic())
                        .centerCrop()
                        .crossFade()
                        .into(holder.imgFriend);
            } else {
                Glide.with(mContext)
                        .loadFromMediaStore(Uri.parse(friendModel.getProfilePic()))
                        .centerCrop()
                        .crossFade()
                        .into(holder.imgFriend);
            }
        } else {
            holder.imgFriend.setImageDrawable(null);
        }


        return convertView;
    }


    class ViewHolder {
        TextView txtFriendName;
        CheckBox chkAddFriend;
        CircleImageView imgFriend;
    }


}
