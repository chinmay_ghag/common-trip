package com.drive.share.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.drive.share.sharedrive.R;
import com.drive.share.ui.fragments.thirdparty.PartyDetailsFragment;
import com.drive.share.ui.fragments.thirdparty.PartyTripsFragment;

/**
 * Created by webwerks on 29/1/15.
 */
public class ThirdPartyPagerAdapter extends FragmentPagerAdapter {


    private static final int ITEM_COUNT = 2;
    private final int PARTY_INFO = 0, PARTY_TRIPS = 1;
    private final Integer[] titles = new Integer[]{R.string.lbl_party_info, R.string.lbl_party_trip};


    Context mContext;

    public ThirdPartyPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment screen = null;
        switch (position) {

            case PARTY_INFO:
                screen = new PartyDetailsFragment();
                break;
            case PARTY_TRIPS:
                screen = new PartyTripsFragment();
                break;

        }

        return screen;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(titles[position]);
    }

    @Override
    public int getCount() {
        return ITEM_COUNT;
    }
}
