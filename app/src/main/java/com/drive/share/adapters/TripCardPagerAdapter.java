package com.drive.share.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.drive.share.sharedrive.R;
import com.drive.share.ui.fragments.tripcard.TripCardInfoFragment;
import com.drive.share.ui.fragments.tripcard.TripCardStatusFragment;
import com.drive.share.ui.fragments.tripcard.TripCardTalkFragment;

/**
 * Created by webwerks on 29/1/15.
 */
public class TripCardPagerAdapter extends FragmentPagerAdapter {


    private static final int ITEM_COUNT = 3;
    private final int TRIP_TALK = 2, TRIP_INFO = 0, TRIP_STATUS = 1;
    private final Integer[] titles = new Integer[]{R.string.lbl_party_info, R.string.screen_trip, R.string.screen_talk};


    Context mContext;

    public TripCardPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment screen = null;
        switch (position) {

            case TRIP_INFO:
                screen = new TripCardInfoFragment();
                break;
            case TRIP_STATUS:
                screen = new TripCardStatusFragment();
                break ;
            case TRIP_TALK:
                screen = new TripCardTalkFragment();
                break;
        }

        return screen;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(titles[position]);
    }

    @Override
    public int getCount() {
        return ITEM_COUNT;
    }
}
