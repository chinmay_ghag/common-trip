package com.drive.share.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.drive.share.ui.fragments.editor.TripInfoFragment;
import com.drive.share.ui.fragments.editor.TripMapFragment;

/**
 * Created by webwerks on 29/1/15.
 */
public class TripEditorAdapter extends FragmentPagerAdapter {

    private final int ITEM_SIZE = 3, TRIP_INFO = 0, TRIP_DESTINATION = 1, TRIP_INVITE = 2;


    public TripEditorAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment editorScreen = null;
        switch (position) {
            case TRIP_INFO:
                editorScreen = TripInfoFragment.newInstance(null);
                break;
            case TRIP_DESTINATION:
                editorScreen = new TripMapFragment();
                break;
            case TRIP_INVITE:
                editorScreen = TripInfoFragment.newInstance(null);
                break;

        }

        return editorScreen;
    }

    @Override
    public int getCount() {
        return ITEM_SIZE;
    }
}
