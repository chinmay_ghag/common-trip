package com.drive.share.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.utilities.Validation;
import com.bumptech.glide.Glide;
import com.drive.share.businesslayer.UserLogic;
import com.drive.share.data.webapi.UserModel;
import com.drive.share.globals.utils.Commons;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.fragments.home.ContactsFragment;

import java.util.List;

/**
 * Created by yashesh on 2/2/15.
 */
public class HomeContactsAdapter extends ArrayAdapter<UserModel> {

    private Context mContext;
    private ContactsFragment mContactsFragment;

    public HomeContactsAdapter(Context context, List<UserModel> objects, ContactsFragment contactsFragment) {
        super(context, 0, 0, objects);
        mContext = context;
        mContactsFragment = contactsFragment;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_home_contacts, null);
            holder = new ViewHolder();
            holder.btnMenu = convertView.findViewById(R.id.btnContactsMenu);
            holder.lblName = (TextView) convertView.findViewById(R.id.lblContactName);
            holder.lblNumber = (TextView) convertView.findViewById(R.id.lblContactNumber);
            holder.imgProfile = (ImageView) convertView.findViewById(R.id.imgContact);
            holder.btnInvite = (Button) convertView.findViewById(R.id.btnInvite);

            holder.btnInvite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Commons.appShareIntent(mContext);
                }
            });

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.lblName.setText("");
        holder.lblNumber.setText("");

        final UserModel contact = getItem(position);


        if (!TextUtils.isEmpty(contact.getFirstName()) || !TextUtils.isEmpty(contact.getLastName())) {
            holder.lblName.setVisibility(View.VISIBLE);
            holder.lblName.setText(contact.getFirstName() + " " + contact.getLastName());
            holder.lblNumber.setText(Commons.getFormattedContactNumber(contact.getMobNumber()));
        } else {
            holder.lblName.setVisibility(View.GONE);
            holder.lblNumber.setText(Commons.getFormattedContactNumber(contact.getMobNumber()));
        }

        holder.btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(mContext, v);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_thirdpart, popup.getMenu());
                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.block_user:
                                if(Validation.Network.isConnected(mContext)) {
                                    Toast.makeText(mContext, "Inside menu " + contact.getUserId(), Toast.LENGTH_SHORT).show();
                                    UserLogic.blockUser(contact.getUserId(), mContactsFragment);
                                    break;
                                }else{
                                    Commons.errorNoInternet(mContext);
                                }
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });


        /*if (!TextUtils.isEmpty(contact.getFirstName()) && !TextUtils.isEmpty(contact.getLastName())) {
            holder.lblName.setText(contact.getFirstName() + " " + contact.getLastName());
            holder.lblNumber.setText(Commons.getFormattedContactNumber(contact.getMobNumber()));
        } else if (!TextUtils.isEmpty(contact.getFirstName())) {
            holder.lblName.setText(contact.getFirstName());
            holder.lblNumber.setText(Commons.getFormattedContactNumber(contact.getMobNumber()));
        } else if (!TextUtils.isEmpty(contact.getLastName())) {
            holder.lblName.setText(contact.getLastName());
            holder.lblNumber.setText(Commons.getFormattedContactNumber(contact.getMobNumber()));
        } else {
            holder.lblName.setVisibility(View.GONE);
            holder.lblNumber.setText(Commons.getFormattedContactNumber(contact.getMobNumber()));
        }*/


        if (contact.isRegistered()) {
            holder.showMenu(true);
        } else {
            holder.showMenu(false);
        }


        if (!TextUtils.isEmpty(contact.getProfilePic())) {
            if (contact.getProfilePic().startsWith("http")) {
                Glide.with(mContext)
                        .load(contact.getProfilePic())
                        .centerCrop()
                        .crossFade()
                        .into(holder.imgProfile);
            } else {
                Glide.with(mContext)
                        .loadFromMediaStore(Uri.parse(contact.getProfilePic()))
                        .centerCrop()
                        .crossFade()
                        .into(holder.imgProfile);
            }

        } else {
            holder.imgProfile.setImageDrawable(null);
        }

        return convertView;
    }


    class ViewHolder {
        TextView lblName, lblNumber;
        ImageView imgProfile;
        View btnMenu, btnInvite;

        private void showMenu(boolean show) {
            if (show) {
                btnMenu.setVisibility(View.VISIBLE);
                btnInvite.setVisibility(View.GONE);
            } else {
                btnMenu.setVisibility(View.GONE);
                btnInvite.setVisibility(View.VISIBLE);
            }

        }

    }


}
