package com.drive.share.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.drive.share.data.webapi.TripModel;
import com.drive.share.globals.utils.Commons;
import com.drive.share.sharedrive.R;

import java.util.List;

/**
 * Created by yashesh on 2/2/15.
 */
public class HomeTripsAdapter extends ArrayAdapter<TripModel> {

    private Context mContext;

    public HomeTripsAdapter(Context context, List<TripModel> objects) {
        super(context, 0, 0, objects);
        mContext = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_trip_list, null);
            holder = new ViewHolder();
            holder.txtTripTitle = (TextView) convertView.findViewById(R.id.txtTripTitle);
            holder.txtRole = (TextView) convertView.findViewById(R.id.txtRole);
            holder.txtPassengerCount = (TextView) convertView.findViewById(R.id.txtPassengerCount);
            holder.txtTripDistance = (TextView) convertView.findViewById(R.id.txtDistance);
            holder.imgTripImage = (ImageView) convertView.findViewById(R.id.imgTripImage);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        TripModel tripResponse = getItem(position);

        Commons.setItemData(holder.txtTripTitle, tripResponse.getTrip_name(), "trip " + (position + 1));

        Commons.setItemData(holder.txtRole, tripResponse.getTripUserRole(), Commons.VIEW_GONE);

        Commons.setItemData(holder.txtPassengerCount, tripResponse.getTripTotalPassenger(), Commons.VIEW_GONE);

        Commons.setItemData(holder.txtTripDistance, tripResponse.getTripDistance(), Commons.VIEW_GONE);


        return convertView;
    }


    class ViewHolder {
        TextView txtTripTitle, txtRole, txtPassengerCount, txtTripDistance;
        ImageView imgTripImage;
    }


}
