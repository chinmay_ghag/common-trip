package com.drive.share.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.utilities.datareader.DataReader;
import com.drive.share.businesslayer.UserRequestLogic;
import com.drive.share.data.webapi.Passenger;
import com.drive.share.globals.App;
import com.drive.share.globals.utils.Commons;
import com.drive.share.sharedrive.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by yashesh on 2/2/15.
 */
public class PeopleAdapter extends ArrayAdapter<Passenger> {

    private Context mContext;
    private String tripUserRelation;
    private String tripCardId;
    private List<Passenger> lstPassengers;

    HashMap<Passenger, Integer> mIdMap = new HashMap<Passenger, Integer>();

    final int INVALID_ID = -1;

    public PeopleAdapter(Context context, List<Passenger> objects) {
        super(context, 0, 0, objects);
        for (int i = 0; i < objects.size(); ++i) {
            mIdMap.put(objects.get(i), i);
        }
        mContext = context;
        lstPassengers = objects;
    }

    @Override
    public long getItemId(int position) {
        if (position < 0 || position >= mIdMap.size()) {
            return INVALID_ID;
        }
        Passenger item = getItem(position);
        return mIdMap.get(item);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    public String getTripCardId() {
        return tripCardId;
    }

    public void setTripCardId(String tripCardId) {
        this.tripCardId = tripCardId;
    }

    public String getTripUserRelation() {
        return tripUserRelation;
    }

    public void setTripUserRelation(String tripUserRelation) {
        this.tripUserRelation = tripUserRelation;
    }

    @Override
    public Passenger getItem(int position) {
        return lstPassengers.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_people_list, null);
            holder = new ViewHolder();
            holder.txtTripTitle = (TextView) convertView.findViewById(R.id.txtPeopleName);
            holder.txtRole = (TextView) convertView.findViewById(R.id.txtLocation);
            holder.imgTripImage = (ImageView) convertView.findViewById(R.id.imgProfileImage);
            holder.btnAccept = (Button) convertView.findViewById(R.id.btnAccept);
            holder.btnReject = (Button) convertView.findViewById(R.id.btnReject);
            holder.imgRemove = (ImageView) convertView.findViewById(R.id.imgRemoveUser);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Passenger peopleResponse = getItem(position);

        if("driver".equalsIgnoreCase(tripUserRelation)){
            if("accepted".equalsIgnoreCase(peopleResponse.getPassenger_status())){
//                holder.imgRemove.setVisibility(View.VISIBLE);
            }else{
                holder.btnAccept.setVisibility(View.VISIBLE);
                holder.btnReject.setVisibility(View.VISIBLE);
            }

        }

        holder.btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserRequestLogic.respondToRequest(peopleResponse.getPassengerId(),tripCardId,"0", App.getInstance().getLoggedInUser().getUserId(), new DataReader.DataClient() {
                    @Override
                    public void onDataReceived(int readRequestCode, Object data) {
                        Toast.makeText(mContext,"Requst rejected.", Toast.LENGTH_SHORT).show();
                        remove(peopleResponse);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onDataEmpty(int readRequestCode) {

                    }

                    @Override
                    public void onError(int readRequestCode, Object error) {
                        Toast.makeText(mContext,"Something went wrong.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public boolean needResponse() {
                        return true;
                    }
                });
            }
        });

        holder.btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserRequestLogic.respondToRequest(peopleResponse.getPassengerId(),tripCardId,"1", App.getInstance().getLoggedInUser().getUserId(), new DataReader.DataClient() {
                    @Override
                    public void onDataReceived(int readRequestCode, Object data) {
                        Toast.makeText(mContext,"Requst rejected.", Toast.LENGTH_SHORT).show();
                        remove(peopleResponse);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onDataEmpty(int readRequestCode) {

                    }

                    @Override
                    public void onError(int readRequestCode, Object error) {
                        Toast.makeText(mContext,"Passenger added.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public boolean needResponse() {
                        return true;
                    }
                });
            }
        });

        /*holder.imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserRequestLogic.respondToRequest(peopleResponse.getPassengerId(),tripCardId,"0", App.getInstance().getLoggedInUser().getUserId(), new DataReader.DataClient() {
                    @Override
                    public void onDataReceived(int readRequestCode, Object data) {
                        Toast.makeText(mContext,"Passenger removed.", Toast.LENGTH_SHORT).show();
                        remove(peopleResponse);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onDataEmpty(int readRequestCode) {

                    }

                    @Override
                    public void onError(int readRequestCode, Object error) {
                        Toast.makeText(mContext,"Something went wrong.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public boolean needResponse() {
                        return true;
                    }
                });
            }
        });*/

        Commons.setItemData(holder.txtTripTitle, peopleResponse.getPassengerName(), "passenger " + (position + 1));

        Commons.setItemData(holder.txtRole, peopleResponse.getPassengerLocation(), Commons.VIEW_GONE);



        return convertView;
    }


    class ViewHolder {
        TextView txtTripTitle, txtRole;
        ImageView imgTripImage , imgRemove;
        Button btnAccept, btnReject;
    }


}
