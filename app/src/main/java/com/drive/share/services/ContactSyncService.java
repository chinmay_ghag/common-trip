package com.drive.share.services;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.android.utilities.datareader.DataReader;
import com.drive.share.businesslayer.ContactLogic;
import com.drive.share.dao.UserDao;
import com.drive.share.data.webapi.ContactSyncModel;
import com.drive.share.data.webapi.UserModel;
import com.drive.share.globals.App;
import com.drive.share.globals.Constants;
import com.drive.share.globals.utils.Commons;
import com.drive.share.loaders.ContactsLoader;
import com.opendroid.db.DbHelper;
import com.opendroid.db.dao.DAOException;

import java.util.List;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */

public class ContactSyncService extends Service implements DataReader.DataClient {


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("LOADING CONTACTS", "SYNC");

        new ContactsLoader(this, this).read(Constants.REQ_LOAD_CONTACTS);

        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * On data received.
     *
     * @param readRequestCode the read request code
     * @param data            the data
     */
    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        switch (readRequestCode) {
            case Constants.REQ_LOAD_CONTACTS:

                if ((List<UserModel>) data != null && ((List<UserModel>) data).size() > 0) {
                    ContactLogic.uploadContacts((List<UserModel>) data, this);
                } else {
                    //stop the service once sync finished.
                    stopSelf();
                }
                break;

            case ContactLogic.REQ_UPLOAD_CONTACTS:

                // insert into db
                ContactSyncModel respModel = (ContactSyncModel) data;

                List<UserModel> models = respModel.getLstSyncData();
                Log.e("Alpha", "RESPONSE UPLOAD "+ models.size() + "");
                int counter = 0;
                UserDao dao = new UserDao(App.getInstance(), DbHelper.getInstance(App.getInstance()).getSQLiteDatabase());
                for (UserModel model : models) {
                    try {
                        //Linus here
                        model.setMobNumber(Commons.getValidatedPhoneNumber(model.getMobNumber()));
                        Log.e("Alpha", "RESPONSE UPLOAD "+model.getMobNumber() + " -- " + model.getUserId());
                        dao.createOrUpdate(model);
                        counter++;
                    } catch (DAOException e) {
                        e.printStackTrace();
                        continue;
                    }
                }
                Log.e("Alpha","INSERT COUNT"+ counter + "...");

                stopSelf();
                break;
        }

    }

    /**
     * On data empty.
     *
     * @param readRequestCode the read request code
     */
    @Override
    public void onDataEmpty(int readRequestCode) {

    }

    @Override
    public void onError(int readRequestCode, Object error) {
    }

    @Override
    public boolean needResponse() {
        return true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
