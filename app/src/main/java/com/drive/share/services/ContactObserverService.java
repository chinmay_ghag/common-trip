package com.drive.share.services;

import android.app.Service;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.util.Log;

import com.android.utilities.datareader.DataReader;
import com.drive.share.businesslayer.ContactLogic;
import com.drive.share.data.webapi.ContactSyncModel;

/**
 * Created by yashesh on 20/2/15.
 */
public class ContactObserverService extends Service implements DataReader.DataClient {


    private ContactObserver mObserver;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mObserver = new ContactObserver();

        ContactLogic.saveLastContactUpdatedTime(this);
        //register the content observer.
        getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, mObserver);

        Log.e("CONTACT OBSERVER", "STARTED");

        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * On data received.
     *
     * @param readRequestCode the read request code
     * @param data            the data
     */
    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        if (data != null) {
            switch (readRequestCode) {
                case ContactLogic.REQ_UPLOAD_CONTACTS:
                    ContactLogic.processContactSyncData((ContactSyncModel) data);
                    break;
            }
        }
    }

    /**
     * On data empty.
     *
     * @param readRequestCode the read request code
     */
    @Override
    public void onDataEmpty(int readRequestCode) {

    }

    /**
     * On error.
     *
     * @param readRequestCode the read request code
     * @param error
     */
    @Override
    public void onError(int readRequestCode, Object error) {

    }

    @Override
    public boolean needResponse() {
        return true;
    }


    private class ContactObserver extends ContentObserver {
        //  DataReader.DataClient mCLient;

        /**
         * Creates a content observer.
         *
         * @param handler The handler to run {@link #onChange} on, or null if none.
         */
        public ContactObserver(Handler handler) {
            super(handler);
        }

        public ContactObserver() {
            super(null);
            // mCLient=client;
        }

        @Override
        public void onChange(boolean selfChange) {
            onChange(selfChange, null);
            Log.e("CONTACT CHANGED", "null");
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            //todo:sync this contact with server.
            Log.e("CONTACT CHANGED", uri + "");
            ContactLogic.syncModifiedContacts(ContactObserverService.this, ContactObserverService.this);
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }
    }

    private void saveLastContactUpdatedTime() {

    }


    @Override
    public void onDestroy() {
        // unregister currently active content observer.
        getContentResolver().unregisterContentObserver(mObserver);
        super.onDestroy();
    }
}
