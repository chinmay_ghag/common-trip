package com.drive.share.data.webapi;

import com.android.utilities.http.WebResponse;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 3/14/2015.
 */
public class TalkResponseModel implements WebResponse<TalkModel> {

    @Expose
    private Integer responseCode;
    @Expose
    private List<TalkModel> tripData = new ArrayList<TalkModel>();

    /**
     *
     * @return
     * The responseCode
     */
    public Integer getResponseCode() {
        return responseCode;
    }

    /**
     *
     * @param responseCode
     * The responseCode
     */
    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    /**
     *
     * @return
     * The tripData
     */
    public List<TalkModel> getTripData() {
        return tripData;
    }

    /**
     *
     * @param tripData
     * The tripData
     */
    public void setTripData(List<TalkModel> tripData) {
        this.tripData = tripData;
    }

    @Override
    public String getCompareField() {
        return null;
    }

    @Override
    public int compareTo(TalkModel another) {
        return 0;
    }
}
