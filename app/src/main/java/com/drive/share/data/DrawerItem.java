package com.drive.share.data;

/**
 * Created by webwerks on 28/2/15.
 */
public class DrawerItem {

    String itemName;
    int imgResId,count;
    boolean countVisibility;

    public boolean isCountVisibility() {
        return countVisibility;
    }

    public DrawerItem(String itemName,int imgResId,boolean countVisibility){
        this.itemName=itemName;
        this.imgResId=imgResId;
        this.countVisibility=countVisibility;
    }

    public String getName() {
        return itemName;
    }

    public int getImgRes() {
        return imgResId;
    }

    public int getCount() {
        return count;
    }

    public boolean getcountVisibility(){
        return countVisibility;
    }
}
