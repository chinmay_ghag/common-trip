package com.drive.share.data.webapi;

import android.text.TextUtils;

import com.android.utilities.http.WebResponse;
import com.drive.share.globals.utils.Commons;
import com.opendroid.db.DbModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by yashesh on 16/2/15.
 */
public class TripModel implements WebResponse<TripModel>, DbModel ,Serializable{

    public static final String TABLE_NAME = "trip";

    public static String IS_PICK_UP = "1";
    public static String IS_DROP_OUT = "0";
    public static String IS_RETURN_TRIP = "1";
    public static String IS_NOT_RETURN_TRIP = "0";
    public static String FREQ_NEVER = "3";
    public static String FREQ_WEEKLY = "1";

    private String user_id, trip_card_id, trip_name, trip_frequency, vehicle_id, origin_latitude, origin_longitude, origin_name, destination_latitude, destination_longitude, destination_name, create_departure_trip, start_date, start_time, end_date, end_time, trip_days, is_pick_up, trip_type;

    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_TRIP_CARD_ID = "trip_card_id";
    public static final String COLUMN_TRIP_NAME = "trip_name";
    public static final String COLUMN_TRIP_FREQUENCY = "trip_frequency";
    public static final String COLUMN_VEHICLE_ID = "vehicle_id";
    public static final String COLUMN_ORIGIN_LATITUDE = "origin_latitude";
    public static final String COLUMN_ORIGIN_LONGITUDE = "origin_longitude";
    public static final String COLUMN_ORIGIN_NAME = "origin_name";
    public static final String COLUMN_DESTINATION_LATITUDE = "destination_latitude";
    public static final String COLUMN_DESTINATION_LONGITUDE = "destination_longitude";
    public static final String COLUMN_DESTINATION_NAME = "destination_name";
    public static final String COLUMN_CREATE_DEPARTURE_TRIP = "create_departure_trip";
    public static final String COLUMN_START_DATE = "start_date";
    public static final String COLUMN_START_TIME = "start_time";
    public static final String COLUMN_END_DATE = "end_date";
    public static final String COLUMN_TRIP_DAYS = "trip_days";
    public static final String COLUMN_TRIP_DISTANCE = "trip_distance";
    public static final String COLUMN_TRIP_TOTAL_PASSENGER = "trip_total_passenger";
    public static final String COLUMN_TRIP_USER_ROLE = "trip_user_role";
    public static final String COLUMN_IS_PICK_UP = "is_pick_up";
    public static final String COLUMN_TRIP_TYPE = "trip_type";
    public static final String COLUMN_END_TIME = "end_time";


    private String trip_userRole;
    private String trip_distance;
    private String trip_total_passenger;

    private String message;
    private boolean status;

    @SerializedName("trip_data")
    @Expose
    private TripData tripData;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The tripData
     */
    public TripData getTripData() {
        return tripData;
    }

    /**
     *
     * @param tripData
     * The trip_data
     */
    public void setTripData(TripData tripData) {
        this.tripData = tripData;
    }

    /*public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    private String tripId;*/

    public String getTripImage() {
        return tripImage;
    }

    public void setTripImage(String tripImage) {
        this.tripImage = tripImage;
    }

    private String tripImage;

    public String getTripUserRole() {

        if (TextUtils.isEmpty(trip_userRole)) {
            return trip_userRole;
        } else {
            return "@ " + trip_userRole;
        }

    }

    public String getTrip_card_id() {
        return trip_card_id;
    }

    public void setTrip_card_id(String trip_card_id) {
        this.trip_card_id = trip_card_id;
    }

    public String getIs_pick_up() {
        return is_pick_up;
    }

    public void setTripUserRole(String trip_userRole) {
        this.trip_userRole = trip_userRole;
    }

    public String getTripDistance() {
        if (TextUtils.isEmpty(trip_distance)) {
            return trip_distance;
        } else {
            return Commons.appendKM(trip_distance);
        }
    }

    public void setTripDistance(String trip_distance) {
        this.trip_distance = trip_distance;
    }

    public String getTripTotalPassenger() {
        if (TextUtils.isEmpty(trip_total_passenger)) {
            return trip_total_passenger;
        } else {
            return Commons.appendKM(trip_total_passenger);
        }
    }

    public void setTripTotalPassenger(String trip_total_passenger) {
        this.trip_total_passenger = trip_total_passenger;
    }

    public String is_pick_up() {
        return is_pick_up;
    }

    public void setIs_pick_up(String is_pick_up) {
        this.is_pick_up = is_pick_up;
    }

    public String getTrip_days() {
        return trip_days;
    }

    public void setTrip_days(String trip_days) {
        this.trip_days = trip_days;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTrip_name() {
        return trip_name;
    }

    public void setTrip_name(String trip_name) {
        this.trip_name = trip_name;
    }

    public String getTrip_frequency() {
        return trip_frequency;
    }

    public void setTrip_frequency(String trip_frequency) {
        this.trip_frequency = trip_frequency;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getOrigin_latitude() {
        return origin_latitude;
    }

    public void setOrigin_latitude(String origin_latitude) {
        this.origin_latitude = origin_latitude;
    }

    public String getOrigin_longitude() {
        return origin_longitude;
    }

    public void setOrigin_longitude(String origin_longitude) {
        this.origin_longitude = origin_longitude;
    }

    public String getOrigin_name() {
        return origin_name;
    }

    public void setOrigin_name(String origin_name) {
        this.origin_name = origin_name;
    }

    public String getDestination_latitude() {
        return destination_latitude;
    }

    public void setDestination_latitude(String destination_latitude) {
        this.destination_latitude = destination_latitude;
    }

    public String getDestination_longitude() {
        return destination_longitude;
    }

    public void setDestination_longitude(String destination_longitude) {
        this.destination_longitude = destination_longitude;
    }

    public String getDestination_name() {
        return destination_name;
    }

    public void setDestination_name(String destination_name) {
        this.destination_name = destination_name;
    }

    public String getCreate_departure_trip() {
        return create_departure_trip;
    }

    public void setCreate_departure_trip(String create_departure_trip) {
        this.create_departure_trip = create_departure_trip;
    }

    public String getTrip_type() {
        return trip_type;
    }

    public void setTrip_type(String trip_type) {
        this.trip_type = trip_type;
    }

    @Override
    public String getCompareField() {
        return null;
    }

    @Override
    public int compareTo(TripModel tripModel) {
        return 0;
    }


    @Override
    public long getId() {
        return 0;//Long.parseLong(user_id);
    }

    @Override
    public void setId(long id) {

    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getCreateStatement() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + COLUMN_TRIP_CARD_ID + " TEXT PRIMARY KEY," +
                COLUMN_TRIP_NAME + " VARCHAR(255)," +
                COLUMN_TRIP_FREQUENCY + " VARCHAR(255)," +
                COLUMN_VEHICLE_ID + " VARCHAR(255)," +
                COLUMN_ORIGIN_LATITUDE + " VARCHAR(255)," +
                COLUMN_ORIGIN_LONGITUDE + " VARCHAR(5)," +
                COLUMN_ORIGIN_NAME + " VARCHAR(50)," +
                COLUMN_USER_ID + " TEXT," +
                COLUMN_DESTINATION_LATITUDE + " TEXT, " +
                COLUMN_DESTINATION_LONGITUDE + " TEXT, " +
                COLUMN_DESTINATION_NAME + " TEXT, " +
                COLUMN_CREATE_DEPARTURE_TRIP + " TEXT, " +
                COLUMN_START_DATE + " TEXT, " +
                COLUMN_START_TIME + " TEXT, " +
                COLUMN_END_DATE + " TEXT, " +
                COLUMN_END_TIME + " TEXT, " +
                COLUMN_TRIP_DAYS + " TEXT, " +
                COLUMN_TRIP_DISTANCE + " TEXT, " +
                COLUMN_TRIP_TOTAL_PASSENGER + " TEXT, "+
                COLUMN_TRIP_USER_ROLE + " TEXT, "+
                COLUMN_IS_PICK_UP + " TEXT, " +
                COLUMN_TRIP_TYPE + " INTEGER)";
    }

    @Override
    public String getPrimaryColumnName() {
        return COLUMN_TRIP_CARD_ID;
    }
}
