package com.drive.share.data.webapi;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashesh on 10/2/15.
 */
public class VehicleCountModel extends BaseResponseModel {

    @SerializedName("vehicle_type")
    private int vehicleType;

    @SerializedName("count")
    private int count;

    public int getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(int vehicleType) {
        this.vehicleType = vehicleType;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
