package com.drive.share.data.webapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.opendroid.db.DbModel;

public class Passenger implements DbModel {

    public static final String TABLE_NAME = "passenger";
    public static final String COLUMN_PASSENGER_ID = "passenger_id";
    public static final String COLUMN_PASSENGER_NAME = "passenger_name";
    public static final String COLUMN_TRIP_CARD_ID = "trip_card_id";

    private String trip_card_id;

    @SerializedName("passenger_id")
    @Expose
    private String passengerId;
    @SerializedName("passenger_name")
    @Expose
    private String passengerName;
    @SerializedName("passenger_status")
    @Expose
    private String passenger_status;
    @SerializedName("passengerLocation")
    @Expose
    private String passengerLocation;

    public String getTrip_card_id() {
        return trip_card_id;
    }

    public void setTrip_card_id(String trip_card_id) {
        this.trip_card_id = trip_card_id;
    }

    /**
     * @return The passengerId
     */
    public String getPassengerId() {
        return passengerId;
    }

    public String getPassengerLocation() {
        return passengerLocation;
    }

    public void setPassengerLocation(String passengerLocation) {
        this.passengerLocation = passengerLocation;
    }

    /**
     * @param passengerId The passenger_id
     */
    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }

    /**
     * @return The passengerName
     */
    public String getPassengerName() {
        return passengerName;
    }

    /**
     * @param passengerName The passenger_name
     */
    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    @Override
    public long getId() {
        return 0;
    }

    @Override
    public void setId(long id) {

    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getCreateStatement() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + COLUMN_PASSENGER_ID + " TEXT PRIMARY KEY," +
                COLUMN_PASSENGER_NAME + " VARCHAR(255)," +
                COLUMN_TRIP_CARD_ID + " VARCHAR(255)," +
                COLUMN_TRIP_CARD_ID + " TEXT)";
    }

    @Override
    public String getPrimaryColumnName() {
        return COLUMN_PASSENGER_ID;
    }

    public String getPassenger_status() {
        return passenger_status;
    }

    public void setPassenger_status(String passenger_status) {
        this.passenger_status = passenger_status;
    }
}