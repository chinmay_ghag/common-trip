package com.drive.share.data.webapi;

import com.android.utilities.http.WebResponse;
import com.opendroid.db.DbModel;

/**
 * Created by yashesh on 10/2/15.
 */
public class VehicleModel implements WebResponse<VehicleModel>, DbModel {


    public final static int TYPE_TWO_WHEELER = 1;
    public final static int TYPE_FOUR_WHEELER = 2;

    public static final String TABLE_NAME = "vehicle";
    public static final String COLUMN_VEHICLE_ID = "vehicle_id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_VEHICLE_REGISTRATION_NUMBER = "vehicle_registration_number";
    public static final String COLUMN_VEHICLE_NAME = "vehicle_name";
    public static final String COLUMN_VEHICLE_TYPE = "vehicle_type";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_IMAGE = "image";

    private String vehicle_registration_number, vehicle_name, description, image, user_id,
            vehicle_id;
    private int vehicle_type;


    public String getVehicleId() {
        return vehicle_id;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicle_id = vehicleId;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getVehicle_registration_number() {
        return vehicle_registration_number;
    }

    public void setVehicle_registration_number(String vehicle_registration_number) {
        this.vehicle_registration_number = vehicle_registration_number;
    }

    public String getVehicle_name() {
        return vehicle_name;
    }

    public void setVehicle_name(String vehicle_name) {
        this.vehicle_name = vehicle_name;
    }

    public int getVehicle_type() {
        return vehicle_type;
    }

    public void setVehicle_type(int vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String getCompareField() {
        return null;
    }

    @Override
    public int compareTo(VehicleModel vehicleModel) {
        return 0;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @Override
    public long getId() {
        return Long.parseLong(vehicle_id);
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    @Override
    public void setId(long id) {
    }

    /**
     * returns table name to which the model corresponds.
     *
     * @return the table name
     */
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    /**
     * Gets the creates the statement for table.
     *
     * @return the creates the statement
     */
    @Override
    public String getCreateStatement() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + COLUMN_VEHICLE_ID + " INTEGER PRIMARY KEY," +
                COLUMN_USER_ID + " VARCHAR(255)," +
                COLUMN_VEHICLE_REGISTRATION_NUMBER + " VARCHAR(255)," +
                COLUMN_VEHICLE_NAME + " VARCHAR(255)," +
                COLUMN_VEHICLE_TYPE + " INTEGER," +
                COLUMN_DESCRIPTION + " TEXT," +
                COLUMN_IMAGE + " TEXT)";
    }

    @Override
    public String getPrimaryColumnName() {
        return null;
    }
}
