package com.drive.share.data.webapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by yashesh on 5/2/15.
 */
public class SignUpResponseModel extends BaseResponseModel {

    private String user_id;
    private String verification_code;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    private String countryCode;


    private boolean is_new_user;

    @SerializedName("user_data")
    @Expose
    private UserData userData;

    public String getUserId() {
        return user_id;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public String getVerificationCode() {
        return verification_code;
    }


    public boolean isIs_new_user() {
        return is_new_user;
    }

    public void setIs_new_user(boolean is_new_user) {
        this.is_new_user = is_new_user;
    }

    public void setVerificationCode(String verification_code) {
        this.verification_code = verification_code;
    }

    /**
     *
     * @return
     * The userData
     */
    public UserData getUserData() {
        return userData;
    }

    /**
     *
     * @param userData
     * The user_data
     */
    public void setUserData(UserData userData) {
        this.userData = userData;
    }
}
