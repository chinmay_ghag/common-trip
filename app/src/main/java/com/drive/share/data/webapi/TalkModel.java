package com.drive.share.data.webapi;

import com.opendroid.db.DbModel;

/**
 * Created by user on 3/14/2015.
 */
public class TalkModel implements  DbModel {

    /**
     *
     * tripId -- VARCHAR   (TripId to uniquely identify chats for particular trip)
     * tripName -- VARCHAR  (Trip name w.r.t tripId)
     * messageOwnerId -- VARCHAR  (Id of the person who initiated the chat)
     * messageOwnerName -- VARCHAR  (Name of the person who initiated the chat)
     * message -- VARCHAR  (Chat message)
     * receptionType  -- boolean  (To identify if the message is selfOriginated or received, if true then received else selfOriginated)
     * timestamp -- VARCHAR (Server timestamp w.r.t to the message)
     */

    public static final String TABLE_NAME = "talk";

    public static final String COLUMN_ID = "Id";
    public static final String COLUMN_TRIP_ID = "tripId";
    public static final String COLUMN_TRIP_NAME = "tripName";
    public static final String COLUMN_MESSAGE_OWNER_ID = "messageOwnerId";
    public static final String COLUMN_MESSAGE_OWNER_NAME = "messageOwnerName";
    public static final String COLUMN_MESSAGE = "message";
    public static final String COLUMN_RECEPTION_TYPE = "receptionType";
    public static final String COLUMN_TIMESTAMP = "timestamp";

    private long id;
    private String tripId = "";
    private String tripName = "";
    private String messageOwnerId = "";
    private String messageOwnerName = "";
    private String message = "";
    private String receptionType = "";
    private String timestamp = "";


    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public String getMessageOwnerId() {
        return messageOwnerId;
    }

    public void setMessageOwnerId(String messageOwnerId) {
        this.messageOwnerId = messageOwnerId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageOwnerName() {
        return messageOwnerName;
    }

    public void setMessageOwnerName(String messageOwnerName) {
        this.messageOwnerName = messageOwnerName;
    }

    public String getReceptionType() {
        return receptionType;
    }

    public void setReceptionType(String receptionType) {
        this.receptionType = receptionType;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getTableName() {
        return TalkModel.TABLE_NAME;
    }

    @Override
    public String getCreateStatement() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_TRIP_ID + " VARCHAR(255)," +
                COLUMN_TRIP_NAME + " VARCHAR(255)," +
                COLUMN_MESSAGE_OWNER_ID + " VARCHAR(255)," +
                COLUMN_MESSAGE_OWNER_NAME + " VARCHAR(255)," +
                COLUMN_MESSAGE + " VARCHAR(5)," +
                COLUMN_RECEPTION_TYPE + " VARCHAR(50)," +
                COLUMN_TIMESTAMP + " VARCHAR(255))";
    }

    @Override
    public String getPrimaryColumnName() {
        return COLUMN_ID;
    }


}
