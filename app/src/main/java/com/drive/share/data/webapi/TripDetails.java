package com.drive.share.data.webapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TripDetails {

@SerializedName("trip_card_details")
@Expose
private TripCardDetails tripCardDetails;
@Expose
private List<Passenger> passengers = new ArrayList<Passenger>();

    @SerializedName("trip_card_status")
    @Expose
    private TripCardStatus tripCardStatus;

/**
* 
* @return
* The tripCardDetails
*/
public TripCardDetails getTripCardDetails() {
return tripCardDetails;
}

/**
* 
* @param tripCardDetails
* The trip_card_details
*/
public void setTripCardDetails(TripCardDetails tripCardDetails) {
this.tripCardDetails = tripCardDetails;
}

/**
* 
* @return
* The passengers
*/
public List<Passenger> getPassengers() {
return passengers;
}

/**
* 
* @param passengers
* The passengers
*/
public void setPassengers(List<Passenger> passengers) {
this.passengers = passengers;
}

    /**
     *
     * @return
     * The tripCardStatus
     */
    public TripCardStatus getTripCardStatus() {
        return tripCardStatus;
    }

    /**
     *
     * @param tripCardStatus
     * The trip_card_status
     */
    public void setTripCardStatus(TripCardStatus tripCardStatus) {
        this.tripCardStatus = tripCardStatus;
    }

}