package com.drive.share.data.webapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripCardStatus {

@SerializedName("is_trip_started")
@Expose
private Object isTripStarted;
@SerializedName("trip_action_id")
@Expose
private Object tripActionId;
@SerializedName("is_trip_joined")
@Expose
private Object isTripJoined;

/**
* 
* @return
* The isTripStarted
*/
public Object getIsTripStarted() {
return isTripStarted;
}

/**
* 
* @param isTripStarted
* The is_trip_started
*/
public void setIsTripStarted(Object isTripStarted) {
this.isTripStarted = isTripStarted;
}

/**
* 
* @return
* The tripActionId
*/
public Object getTripActionId() {
return tripActionId;
}

/**
* 
* @param tripActionId
* The trip_action_id
*/
public void setTripActionId(Object tripActionId) {
this.tripActionId = tripActionId;
}

/**
* 
* @return
* The isTripJoined
*/
public Object getIsTripJoined() {
return isTripJoined;
}

/**
* 
* @param isTripJoined
* The is_trip_joined
*/
public void setIsTripJoined(Object isTripJoined) {
this.isTripJoined = isTripJoined;
}

}