package com.drive.share.data.webapi;

import com.android.utilities.http.WebResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by atulOholic on 26/2/15.
 */
public class CreatedTripResponse implements WebResponse<CreatedTripResponse> {


    @SerializedName("created")
    private List<TripModel> tripsCreated;

    @SerializedName("request_sent")
    private List<TripModel> tripsInvitation;

    public List<TripModel> getTripsCreated() {
        return tripsCreated;
    }

    public void setTripsCreated(List<TripModel> tripsCreated) {
        this.tripsCreated = tripsCreated;
    }

    public List<TripModel> getTripsInvitation() {
        return tripsInvitation;
    }

    public void setTripsInvitation(List<TripModel> tripsInvitation) {
        this.tripsInvitation = tripsInvitation;
    }

    @Override
    public String getCompareField() {
        return null;
    }

    @Override
    public int compareTo(CreatedTripResponse userTripResponse) {
        return 0;
    }

    private String message;
    private boolean status;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
