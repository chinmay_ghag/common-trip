package com.drive.share.data.webapi;

import com.android.utilities.http.WebResponse;
import com.google.gson.annotations.SerializedName;
import com.opendroid.db.DbModel;

/**
 * Created by yashesh on 4/2/15.
 */
public class UserModel implements WebResponse<UserModel>, DbModel {


    public static final int USER_REGISTERED = 1;

    public static final String TABLE_NAME = "user";

    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_LAST_NAME = "last_name";
    public static final String COLUMN_USER_NAME = "user_name";
    public static final String COLUMN_MOB_NUMBER = "mobile_number";
    public static final String COLUMN_EMAIL_ID = "email_id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_DOB = "dob";
    public static final String COLUMN_GENDER = "gender";
    public static final String COLUMN_PROFILE_PIC = "profile_pic";
    public static final String COLUMN_IS_REGISTERED = "is_registered";

    @SerializedName(value = "firstname")
    private String firstName = "";
    @SerializedName(value = "lastname")
    private String lastName = "";

    @SerializedName(value = "is_registered")
    private int registered;


    private String userName = "";

    @SerializedName(value = "contact_number")
    private String mobNumber = "";
    private String email = "";

    @SerializedName(value = "user_id")
    private String userId = "";
    private String dob = "";
    private String gender = "";
    @SerializedName(value = "image_url")
    private String profilePic = "";


    @SerializedName(value = "country_code")
    private String country_code;


    private boolean isInvited;

    public boolean isInvited() {
        return isInvited;
    }

    public void setInvited(boolean isInvited) {
        this.isInvited = isInvited;
    }

    public String getCountryCode() {
        return country_code;
    }

    public void setCountryCode(String country_code) {
        this.country_code = country_code;
    }

    public boolean isRegistered() {
        return registered == 1;
    }


    public void setIsRegistered(int registered) {
        this.registered = registered;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobNumber() {
        return mobNumber;
    }

    public void setMobNumber(String mobNumber) {
        this.mobNumber = mobNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getCompareField() {
        return null;
    }

    @Override
    public int compareTo(UserModel userModel) {
        return 0;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @Override
    public long getId() {
        return 0;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    @Override
    public void setId(long id) {

    }

    /**
     * returns table name to which the model corresponds.
     *
     * @return the table name
     */
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    /**
     * Gets the creates the statement for table.
     *
     * @return the creates the statement
     */
    @Override
    public String getCreateStatement() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + COLUMN_MOB_NUMBER + " TEXT PRIMARY KEY," +
                COLUMN_FIRST_NAME + " VARCHAR(255)," +
                COLUMN_LAST_NAME + " VARCHAR(255)," +
                COLUMN_EMAIL_ID + " VARCHAR(255)," +
                COLUMN_USER_NAME + " VARCHAR(255)," +
                COLUMN_GENDER + " VARCHAR(5)," +
                COLUMN_DOB + " VARCHAR(50)," +
                COLUMN_USER_ID + " VARCHAR(255)," +
                COLUMN_PROFILE_PIC + " TEXT, " +
                COLUMN_IS_REGISTERED + " INTEGER)";
    }

    @Override
    public String getPrimaryColumnName() {
        return COLUMN_MOB_NUMBER;
    }
}
