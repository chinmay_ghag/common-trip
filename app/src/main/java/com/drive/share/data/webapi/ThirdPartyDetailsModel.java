package com.drive.share.data.webapi;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by atuloholic on 5/2/15.
 */
public class ThirdPartyDetailsModel extends BaseResponseModel {

    @SerializedName("vehicle_count")
    private List<VehicleCountModel> vehicleCountModels;

    @SerializedName("trip_count")
    private int tripCount;

    public List<VehicleCountModel> getVehicleCountModels() {
        return vehicleCountModels;
    }

    public void setVehicleCountModels(List<VehicleCountModel> vehicleCountModels) {
        this.vehicleCountModels = vehicleCountModels;
    }

    public int getTripCount() {
        return tripCount;
    }

    public void setTripCount(int tripCount) {
        this.tripCount = tripCount;
    }
}
