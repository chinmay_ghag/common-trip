package com.drive.share.data.webapi;

import com.android.utilities.http.WebResponse;

/**
 * Created by YASHESH on 9/2/15.
 */
public class UserPicUploadResponse implements WebResponse<UserPicUploadResponse> {

    String url = null;

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }

    @Override
    public String getCompareField() {
        return null;
    }

    @Override
    public int compareTo(UserPicUploadResponse userPicUploadResponse) {
        return 0;
    }

    private String message;
    private boolean status;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
