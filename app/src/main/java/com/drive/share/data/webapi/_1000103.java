package com.drive.share.data.webapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class _1000103 {

@SerializedName("user_name")
@Expose
private String userName;
@SerializedName("pick_up_priority")
@Expose
private String pickUpPriority;
@SerializedName("is_driver")
@Expose
private Integer isDriver;
@SerializedName("trip_card_id")
@Expose
private String tripCardId;
@SerializedName("updated_date")
@Expose
private Object updatedDate;
@SerializedName("origin_latitude")
@Expose
private Object originLatitude;
@SerializedName("origin_longitude")
@Expose
private Object originLongitude;
@SerializedName("pick_up_latitude")
@Expose
private String pickUpLatitude;
@SerializedName("pick_up_longitude")
@Expose
private String pickUpLongitude;
@SerializedName("fence_distance")
@Expose
private String fenceDistance;
@SerializedName("trip_joined")
@Expose
private Boolean tripJoined;

/**
* 
* @return
* The userName
*/
public String getUserName() {
return userName;
}

/**
* 
* @param userName
* The user_name
*/
public void setUserName(String userName) {
this.userName = userName;
}

/**
* 
* @return
* The pickUpPriority
*/
public String getPickUpPriority() {
return pickUpPriority;
}

/**
* 
* @param pickUpPriority
* The pick_up_priority
*/
public void setPickUpPriority(String pickUpPriority) {
this.pickUpPriority = pickUpPriority;
}

/**
* 
* @return
* The isDriver
*/
public Integer getIsDriver() {
return isDriver;
}

/**
* 
* @param isDriver
* The is_driver
*/
public void setIsDriver(Integer isDriver) {
this.isDriver = isDriver;
}

/**
* 
* @return
* The tripCardId
*/
public String getTripCardId() {
return tripCardId;
}

/**
* 
* @param tripCardId
* The trip_card_id
*/
public void setTripCardId(String tripCardId) {
this.tripCardId = tripCardId;
}

/**
* 
* @return
* The updatedDate
*/
public Object getUpdatedDate() {
return updatedDate;
}

/**
* 
* @param updatedDate
* The updated_date
*/
public void setUpdatedDate(Object updatedDate) {
this.updatedDate = updatedDate;
}

/**
* 
* @return
* The originLatitude
*/
public Object getOriginLatitude() {
return originLatitude;
}

/**
* 
* @param originLatitude
* The origin_latitude
*/
public void setOriginLatitude(Object originLatitude) {
this.originLatitude = originLatitude;
}

/**
* 
* @return
* The originLongitude
*/
public Object getOriginLongitude() {
return originLongitude;
}

/**
* 
* @param originLongitude
* The origin_longitude
*/
public void setOriginLongitude(Object originLongitude) {
this.originLongitude = originLongitude;
}

/**
* 
* @return
* The pickUpLatitude
*/
public String getPickUpLatitude() {
return pickUpLatitude;
}

/**
* 
* @param pickUpLatitude
* The pick_up_latitude
*/
public void setPickUpLatitude(String pickUpLatitude) {
this.pickUpLatitude = pickUpLatitude;
}

/**
* 
* @return
* The pickUpLongitude
*/
public String getPickUpLongitude() {
return pickUpLongitude;
}

/**
* 
* @param pickUpLongitude
* The pick_up_longitude
*/
public void setPickUpLongitude(String pickUpLongitude) {
this.pickUpLongitude = pickUpLongitude;
}

/**
* 
* @return
* The fenceDistance
*/
public String getFenceDistance() {
return fenceDistance;
}

/**
* 
* @param fenceDistance
* The fence_distance
*/
public void setFenceDistance(String fenceDistance) {
this.fenceDistance = fenceDistance;
}

/**
* 
* @return
* The tripJoined
*/
public Boolean getTripJoined() {
return tripJoined;
}

/**
* 
* @param tripJoined
* The trip_joined
*/
public void setTripJoined(Boolean tripJoined) {
this.tripJoined = tripJoined;
}

}