package com.drive.share.data.webapi;

import com.opendroid.db.DbModel;

/**
 * Created by Pervacio-Dev on 3/8/2015.
 */
public class PassengerModel implements DbModel{

    String passenger_id, passenger_name, trip_card_id;
    public static final String TABLE_NAME = "passenger";
    public static final String COLUMN_PASSENGER_ID = "passenger_id";
    public static final String COLUMN_PASSENGER_NAME = "passenger_name";
    public static final String COLUMN_TRIP_CARD_ID = "trip_card_id";

    public String getPassenger_id() {
        return passenger_id;
    }

    public void setPassenger_id(String passenger_id) {
        this.passenger_id = passenger_id;
    }

    public String getPassenger_name() {
        return passenger_name;
    }

    public String getTrip_card_id() {
        return trip_card_id;
    }

    public void setTrip_card_id(String trip_card_id) {
        this.trip_card_id = trip_card_id;
    }

    public void setPassenger_name(String passenger_name) {
        this.passenger_name = passenger_name;
    }

    @Override
    public long getId() {
        return 0;
    }

    @Override
    public void setId(long id) {

    }

    @Override
    public String getTableName() {
        return null;
    }

    @Override
    public String getCreateStatement() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + COLUMN_PASSENGER_ID + " TEXT PRIMARY KEY," +
                COLUMN_PASSENGER_NAME + " VARCHAR(255)," +
                COLUMN_TRIP_CARD_ID + " VARCHAR(255)," +
                COLUMN_TRIP_CARD_ID + " TEXT)";
    }

    @Override
    public String getPrimaryColumnName() {
        return COLUMN_PASSENGER_ID;
    }
}
