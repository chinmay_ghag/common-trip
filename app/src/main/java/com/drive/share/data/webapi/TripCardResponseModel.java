package com.drive.share.data.webapi;

import com.android.utilities.http.WebResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.opendroid.db.DbModel;

public class TripCardResponseModel implements WebResponse<TripCardResponseModel>, DbModel {

    @SerializedName("trip_details")
    @Expose
    private TripDetails tripDetails;

    /**
     * @return The tripDetails
     */
    public TripDetails getTripDetails() {
        return tripDetails;
    }

    /**
     * @param tripDetails The trip_details
     */
    public void setTripDetails(TripDetails tripDetails) {
        this.tripDetails = tripDetails;
    }

    @Override
    public String getCompareField() {
        return null;
    }

    @Override
    public int compareTo(TripCardResponseModel another) {
        return 0;
    }

    @Override
    public long getId() {
        return 0;
    }

    @Override
    public void setId(long id) {

    }

    @Override
    public String getTableName() {
        return null;
    }

    @Override
    public String getCreateStatement() {
        return null;
    }

    @Override
    public String getPrimaryColumnName() {
        return null;
    }

    private String message;
    private boolean status;
    private String trip_user_relation;

    public String getTrip_user_relation() {
        return trip_user_relation;
    }

    public void setTrip_user_relation(String trip_user_relation) {
        this.trip_user_relation = trip_user_relation;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}