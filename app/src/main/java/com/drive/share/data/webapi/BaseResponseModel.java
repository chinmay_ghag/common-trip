package com.drive.share.data.webapi;

import com.android.utilities.http.WebResponse;

/**
 * Created by webwerks on 9/2/15.
 */
public class BaseResponseModel implements WebResponse<BaseResponseModel> {

    private String message;
    private boolean status;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getCompareField() {
        return null;
    }

    @Override
    public int compareTo(BaseResponseModel o) {
        return 0;
    }
}
