package com.drive.share.data.webapi;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by atuloholic on 5/2/15.
 */
public class ThirdPartyTripsResponseModel extends BaseResponseModel {

    @SerializedName("user_trips_list")
    private List<TripModel> userTripList;


    public List<TripModel> getUserTripList() {
        return userTripList;
    }

    public void setUserTripList(List<TripModel> userTripList) {
        this.userTripList = userTripList;
    }



}
