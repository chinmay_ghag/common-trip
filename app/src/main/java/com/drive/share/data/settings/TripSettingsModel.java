package com.drive.share.data.settings;

import com.opendroid.db.DbModel;

/**
 * Created by yashesh on 2/4/15.
 */
public class TripSettingsModel implements DbModel{


    private long locationAccessInterval,serverAccessInterval,geoLocationRange;
    private String tripId;
    private boolean privateEnabled,notificationsEnabled;


    public long getGeoLocationRange() {
        return geoLocationRange;
    }

    public void setGeoLocationRange(long geoLocationRange) {
        this.geoLocationRange = geoLocationRange;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public long getLocationAccessInterval() {
        return locationAccessInterval;
    }

    public void setLocationAccessInterval(long locationAccessInterval) {
        this.locationAccessInterval = locationAccessInterval;
    }

    public boolean isNotificationsEnabled() {
        return notificationsEnabled;
    }

    public void setNotificationsEnabled(boolean notificationsEnabled) {
        this.notificationsEnabled = notificationsEnabled;
    }

    public boolean isPrivateEnabled() {
        return privateEnabled;
    }

    public void setPrivateEnabled(boolean privateEnabled) {
        this.privateEnabled = privateEnabled;
    }

    public long getServerAccessInterval() {
        return serverAccessInterval;
    }

    public void setServerAccessInterval(long serverAccessInterval) {
        this.serverAccessInterval = serverAccessInterval;
    }

    @Override
    public long getId() {
        return 0;
    }

    @Override
    public void setId(long id) {

    }

    @Override
    public String getTableName() {
        return null;
    }

    @Override
    public String getCreateStatement() {
        return null;
    }

    @Override
    public String getPrimaryColumnName() {
        return null;
    }
}
