package com.drive.share.data.webapi;

import com.android.utilities.http.WebResponse;
import com.google.gson.annotations.SerializedName;

/**
 * Created by atulOholic on 26/2/15.
 */
public class UserTripResponse implements WebResponse<UserTripResponse> {


    @SerializedName("trip_list")
    private CreatedTripResponse trip_list;


    public CreatedTripResponse getTrip_list() {
        return trip_list;
    }

    public void setTrip_list(CreatedTripResponse trip_list) {
        this.trip_list = trip_list;
    }

    @Override
    public String getCompareField() {
        return null;
    }

    @Override
    public int compareTo(UserTripResponse userTripResponse) {
        return 0;
    }

    private String message;
    private boolean status;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
