package com.drive.share.data.webapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripData {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("trip_name")
    @Expose
    private String tripName;
    @SerializedName("trip_frequency")
    @Expose
    private String tripFrequency;
    @SerializedName("vehicle_id")
    @Expose
    private String vehicleId;
    @SerializedName("origin_latitude")
    @Expose
    private String originLatitude;
    @SerializedName("origin_longitude")
    @Expose
    private String originLongitude;
    @SerializedName("origin_name")
    @Expose
    private String originName;
    @SerializedName("destination_latitude")
    @Expose
    private String destinationLatitude;
    @SerializedName("destination_longitude")
    @Expose
    private String destinationLongitude;
    @SerializedName("destination_name")
    @Expose
    private String destinationName;
    @SerializedName("create_departure_trip")
    @Expose
    private String createDepartureTrip;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("trip_days")
    @Expose
    private String tripDays;
    @SerializedName("is_pick_up")
    @Expose
    private String isPickUp;
    @SerializedName("trip_card_id")
    @Expose
    private Integer tripCardId;
    @SerializedName("opposite_trip_card_id")
    @Expose
    private Integer oppositeTripCardId;

    /**
     * @return The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return The tripName
     */
    public String getTripName() {
        return tripName;
    }

    /**
     * @param tripName The trip_name
     */
    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    /**
     * @return The tripFrequency
     */
    public String getTripFrequency() {
        return tripFrequency;
    }

    /**
     * @param tripFrequency The trip_frequency
     */
    public void setTripFrequency(String tripFrequency) {
        this.tripFrequency = tripFrequency;
    }

    /**
     * @return The vehicleId
     */
    public String getVehicleId() {
        return vehicleId;
    }

    /**
     * @param vehicleId The vehicle_id
     */
    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    /**
     * @return The originLatitude
     */
    public String getOriginLatitude() {
        return originLatitude;
    }

    /**
     * @param originLatitude The origin_latitude
     */
    public void setOriginLatitude(String originLatitude) {
        this.originLatitude = originLatitude;
    }

    /**
     * @return The originLongitude
     */
    public String getOriginLongitude() {
        return originLongitude;
    }

    /**
     * @param originLongitude The origin_longitude
     */
    public void setOriginLongitude(String originLongitude) {
        this.originLongitude = originLongitude;
    }

    /**
     * @return The originName
     */
    public String getOriginName() {
        return originName;
    }

    /**
     * @param originName The origin_name
     */
    public void setOriginName(String originName) {
        this.originName = originName;
    }

    /**
     * @return The destinationLatitude
     */
    public String getDestinationLatitude() {
        return destinationLatitude;
    }

    /**
     * @param destinationLatitude The destination_latitude
     */
    public void setDestinationLatitude(String destinationLatitude) {
        this.destinationLatitude = destinationLatitude;
    }

    /**
     * @return The destinationLongitude
     */
    public String getDestinationLongitude() {
        return destinationLongitude;
    }

    /**
     * @param destinationLongitude The destination_longitude
     */
    public void setDestinationLongitude(String destinationLongitude) {
        this.destinationLongitude = destinationLongitude;
    }

    /**
     * @return The destinationName
     */
    public String getDestinationName() {
        return destinationName;
    }

    /**
     * @param destinationName The destination_name
     */
    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    /**
     * @return The createDepartureTrip
     */
    public String getCreateDepartureTrip() {
        return createDepartureTrip;
    }

    /**
     * @param createDepartureTrip The create_departure_trip
     */
    public void setCreateDepartureTrip(String createDepartureTrip) {
        this.createDepartureTrip = createDepartureTrip;
    }

    /**
     * @return The startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate The start_date
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return The endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate The end_date
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return The startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * @param startTime The start_time
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * @return The endTime
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * @param endTime The end_time
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * @return The tripDays
     */
    public String getTripDays() {
        return tripDays;
    }

    /**
     * @param tripDays The trip_days
     */
    public void setTripDays(String tripDays) {
        this.tripDays = tripDays;
    }

    /**
     * @return The isPickUp
     */
    public String getIsPickUp() {
        return isPickUp;
    }

    /**
     * @param isPickUp The is_pick_up
     */
    public void setIsPickUp(String isPickUp) {
        this.isPickUp = isPickUp;
    }

    /**
     * @return The tripCardId
     */
    public Integer getTripCardId() {
        return tripCardId;
    }

    /**
     * @param tripCardId The trip_card_id
     */
    public void setTripCardId(Integer tripCardId) {
        this.tripCardId = tripCardId;
    }

    /**
     * @return The oppositeTripCardId
     */
    public Integer getOppositeTripCardId() {
        return oppositeTripCardId;
    }

    /**
     * @param oppositeTripCardId The opposite_trip_card_id
     */
    public void setOppositeTripCardId(Integer oppositeTripCardId) {
        this.oppositeTripCardId = oppositeTripCardId;
    }

}