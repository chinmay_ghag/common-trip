package com.drive.share.data.webapi;

import com.android.utilities.http.WebResponse;

import java.util.List;

/**
 * Created by yashesh on 23/2/15.
 */
public class UserProfileResponse implements WebResponse<UserProfileResponse>{

    private boolean status;
    private boolean message;
    private ProfileDetails other_profile_details;


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ProfileDetails getOther_profile_details() {
        return other_profile_details;
    }

    public void setOther_profile_details(ProfileDetails other_profile_details) {
        this.other_profile_details = other_profile_details;
    }

    public boolean isMessage() {
        return message;
    }

    public void setMessage(boolean message) {
        this.message = message;
    }

    public class ProfileDetails{

        private String trip_count;
        private List<VehicleDetails> vehicle_count;

        public class VehicleDetails{

            private  String vehicle_type,count;
        }

        public String getTrip_count() {
            return trip_count;
        }

        public void setTrip_count(String trip_count) {
            this.trip_count = trip_count;
        }

        public List<VehicleDetails> getVehicle_count() {
            return vehicle_count;
        }

        public void setVehicle_count(List<VehicleDetails> vehicle_count) {
            this.vehicle_count = vehicle_count;
        }
    }












    @Override
    public String getCompareField() {
        return null;
    }

    @Override
    public int compareTo(UserProfileResponse userProfileResponse) {
        return 0;
    }
}
