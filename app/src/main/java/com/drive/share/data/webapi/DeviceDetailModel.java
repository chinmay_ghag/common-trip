package com.drive.share.data.webapi;

import com.android.utilities.http.WebResponse;

/**
 * Created by yashesh on 10/2/15.
 */
public class DeviceDetailModel implements WebResponse<DeviceDetailModel> {


    @Override
    public String getCompareField() {
        return null;
    }

    @Override
    public int compareTo(DeviceDetailModel deviceDetailModel) {
        return 0;
    }

    private String message;
    private boolean status;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
