package com.drive.share.data.webapi;

import com.android.utilities.http.WebResponse;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yashesh on 10/2/15.
 */
public class VehicleRegistrationResponse implements WebResponse<VehicleRegistrationResponse> {


    @SerializedName("vehicle_data")
    private VehicleModel vehicle;


    @Override
    public String getCompareField() {
        return null;
    }

    @Override
    public int compareTo(VehicleRegistrationResponse vehicleRegistrationResponse) {
        return 0;
    }


    public VehicleModel getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleModel vehicle) {
        this.vehicle = vehicle;
    }

    private String message;
    private boolean status;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
