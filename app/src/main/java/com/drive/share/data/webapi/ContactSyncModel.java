package com.drive.share.data.webapi;

import com.android.utilities.http.WebResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by YASHESH on 9/2/15.
 */
public class ContactSyncModel implements WebResponse<ContactSyncModel> {


    @SerializedName(value = "synced_data")
    List<UserModel> lstSyncData = null;

    private String message;
    private boolean status;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserModel> getLstSyncData() {
        return lstSyncData;
    }

    public void setLstSyncData(List<UserModel> lstSyncData) {
        this.lstSyncData = lstSyncData;
    }

    @Override
    public String getCompareField() {
        return null;
    }

    @Override
    public int compareTo(ContactSyncModel userPicUploadResponse) {
        return 0;
    }

}
