package com.drive.share.data.webapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {

@SerializedName("country_code")
@Expose
private Integer countryCode;
@SerializedName("mobile_number")
@Expose
private String mobileNumber;
@Expose
private String firstname;
@Expose
private String lastname;
@SerializedName("device_id")
@Expose
private String deviceId;
@SerializedName("user_id")
@Expose
private String userId;
@SerializedName("country_short_name")
@Expose
private String countryShortName;
@SerializedName("is_vehicle_registered")
@Expose
private Boolean isVehicleRegistered;

/**
* 
* @return
* The countryCode
*/
public Integer getCountryCode() {
return countryCode;
}

/**
* 
* @param countryCode
* The country_code
*/
public void setCountryCode(Integer countryCode) {
this.countryCode = countryCode;
}

/**
* 
* @return
* The mobileNumber
*/
public String getMobileNumber() {
return mobileNumber;
}

/**
* 
* @param mobileNumber
* The mobile_number
*/
public void setMobileNumber(String mobileNumber) {
this.mobileNumber = mobileNumber;
}

/**
* 
* @return
* The firstname
*/
public String getFirstname() {
return firstname;
}

/**
* 
* @param firstname
* The firstname
*/
public void setFirstname(String firstname) {
this.firstname = firstname;
}

/**
* 
* @return
* The lastname
*/
public String getLastname() {
return lastname;
}

/**
* 
* @param lastname
* The lastname
*/
public void setLastname(String lastname) {
this.lastname = lastname;
}

/**
* 
* @return
* The deviceId
*/
public String getDeviceId() {
return deviceId;
}

/**
* 
* @param deviceId
* The device_id
*/
public void setDeviceId(String deviceId) {
this.deviceId = deviceId;
}

/**
* 
* @return
* The userId
*/
public String getUserId() {
return userId;
}

/**
* 
* @param userId
* The user_id
*/
public void setUserId(String userId) {
this.userId = userId;
}

/**
* 
* @return
* The countryShortName
*/
public String getCountryShortName() {
return countryShortName;
}

/**
* 
* @param countryShortName
* The country_short_name
*/
public void setCountryShortName(String countryShortName) {
this.countryShortName = countryShortName;
}

/**
* 
* @return
* The isVehicleRegistered
*/
public Boolean getIsVehicleRegistered() {
return isVehicleRegistered;
}

/**
* 
* @param isVehicleRegistered
* The is_vehicle_registered
*/
public void setIsVehicleRegistered(Boolean isVehicleRegistered) {
this.isVehicleRegistered = isVehicleRegistered;
}

}