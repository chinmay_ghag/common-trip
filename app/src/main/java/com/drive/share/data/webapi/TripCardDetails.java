package com.drive.share.data.webapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.opendroid.db.DbModel;

public class TripCardDetails implements DbModel {
    public static final String TABLE_NAME = "trip_card_details";
    @SerializedName("trip_card_id")
    @Expose
    private String tripCardId;
    @SerializedName("trip_name")
    @Expose
    private String tripName;
    @Expose
    private String firstname;
    @Expose
    private String lasttname;
    @SerializedName("origin_name")
    @Expose
    private String originName;
    @SerializedName("destination_name")
    @Expose
    private String destinationName;
    @Expose
    private String distance;

    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("seats_available")
    @Expose
    private String seatsAvail;

    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("end_time")
    @Expose
    private String endTime;

    @SerializedName("driver_id")
    @Expose
    private String driver_id;

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public static final String COLUMN_TRIP_CARD_ID = "trip_card_id";
    public static final String COLUMN_TRIP_NAME = "trip_name";
    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_LAST_NAME = "last_name";
    public static final String COLUMN_ORIGIN_NAME = "origin_name";
    public static final String COLUMN_DESTINATION_NAME = "destination_name";
    public static final String COLUMN_DISTANCE = "distance";

    public String getSeatsAvail() {
        return seatsAvail;
    }

    public void setSeatsAvail(String seatsAvail) {
        this.seatsAvail = seatsAvail;
    }

    /**
     * @return The tripCardId
     */
    public String getTripCardId() {
        return tripCardId;
    }

    /**
     * @param tripCardId The trip_card_id
     */
    public void setTripCardId(String tripCardId) {
        this.tripCardId = tripCardId;
    }

    /**
     * @return The tripName
     */
    public String getTripName() {
        return tripName;
    }

    /**
     * @param tripName The trip_name
     */
    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    /**
     * @return The firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname The firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }


    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     * The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return The lasttname
     */
    public String getLasttname() {
        return lasttname;
    }

    /**
     * @param lasttname The lasttname
     */
    public void setLasttname(String lasttname) {
        this.lasttname = lasttname;
    }

    /**
     * @return The originName
     */
    public String getOriginName() {
        return originName;
    }

    /**
     * @param originName The origin_name
     */
    public void setOriginName(String originName) {
        this.originName = originName;
    }

    /**
     * @return The destinationName
     */
    public String getDestinationName() {
        return destinationName;
    }

    /**
     * @param destinationName The destination_name
     */
    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    /**
     * @return The distance
     */
    public String getDistance() {
        return distance;
    }

    /**
     * @param distance The distance
     */
    public void setDistance(String distance) {
        this.distance = distance;
    }


    @Override
    public long getId() {
        return 0;
    }

    @Override
    public void setId(long id) {

    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getCreateStatement() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + COLUMN_TRIP_CARD_ID + " TEXT PRIMARY KEY," +
                COLUMN_FIRST_NAME + " VARCHAR(255)," +
                COLUMN_LAST_NAME + " VARCHAR(255)," +
                COLUMN_TRIP_NAME + " VARCHAR(255)," +
                COLUMN_ORIGIN_NAME + " VARCHAR(255)," +
                COLUMN_DESTINATION_NAME + " VARCHAR(255)," +
                COLUMN_DISTANCE + " VARCHAR(5))";
    }

    @Override
    public String getPrimaryColumnName() {
        return COLUMN_TRIP_CARD_ID;
    }
}


