package com.drive.share.data.webapi;


import com.google.gson.annotations.SerializedName;

/**
 * Created by atuloholic on 5/2/15.
 */
public class ThirdPartyDetailResponseModel extends BaseResponseModel {

    @SerializedName("other_profile_details")
    private ThirdPartyDetailsModel thirdPartyDetailsModel;

    public ThirdPartyDetailsModel getThirdPartyDetailsModel() {
        return thirdPartyDetailsModel;
    }

    public void setThirdPartyDetailsModel(ThirdPartyDetailsModel thirdPartyDetailsModel) {
        this.thirdPartyDetailsModel = thirdPartyDetailsModel;
    }
}
