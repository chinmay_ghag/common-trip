package com.drive.share.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.drive.share.globals.App;
import com.google.gson.Gson;

/**
 * Created by yashesh on 5/2/15.
 */
public class PreferenceUtils {

    public static final String KEY_VERIFICATION_CODE = "SIGNUP_VERIFICATION";
    public static final String KEY_IS_USER_LOGGED_IN ="USER_LOGGED_IN";
    public static final String KEY_IS_UESR_VERIFIED ="USER_VERIFIED";
    public static final String KEY_USER_ID_UNIQUE = "USER_ID_UNIQUE";
    public static final String KEY_USER_MODEL = "USER_MODEL";
    public static final String KEY_USER_NEW = "KEY_USER_NEW";
    public static final String KEY_COUNTRY_CODE = "KEY_COUNTRY_CODE";

    public static final String KEY_VEHICLE_ID = "KEY_VEHICLE_ID";
    public static final String NAME = "app_pref";
    public static final String KEY_LAST_SYNC_TIME = "KEY_LAST_SYNC_TIME";
    public static final String KEY_LAST_CONTACT_UPDATE_TIME = "KEY_LAST_CONTACT_UPDATE_TIME";

    public static synchronized final void insertString(String key, String value) {
        getPreferences().edit().putString(key, value).commit();
    }

    public synchronized static final void remove(String key) {
        getPreferences().edit().remove(key).commit();
    }

    public synchronized static final String getString(String key) {
        return getPreferences().getString(key, null);
    }

    public static synchronized final void insertBoolean(String key, boolean value) {
        getPreferences().edit().putBoolean(key, value).commit();
    }

    public synchronized static final boolean getBoolean(String key) {
        return getPreferences().getBoolean(key, false);
    }

    public synchronized static final SharedPreferences getPreferences() {
        return App.getInstance().getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    public static synchronized final void insertObject(String key, Object model) {
        getPreferences().edit().putString(key, new Gson().toJson(model)).commit();
    }

    public static synchronized final Object getObject(String key, Class<?> modelClass) {
        return new Gson().fromJson(getPreferences().getString(key, null), modelClass);
    }

}
