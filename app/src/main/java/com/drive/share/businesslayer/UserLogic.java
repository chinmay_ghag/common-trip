package com.drive.share.businesslayer;

import android.util.Log;

import com.android.utilities.datareader.DataReader;
import com.android.utilities.http.HttpDataReader;
import com.drive.share.data.webapi.BaseResponseModel;
import com.drive.share.data.webapi.UserProfileResponse;
import com.drive.share.globals.App;
import com.drive.share.globals.Constants;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashesh on 19/2/15.
 */
public class UserLogic {

    public static final int BLOCK_USER = 425;


    public static final void blockUser(String userId, DataReader.DataClient client) {


        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id", App.getInstance().getAppUserId()));
        params.add(new BasicNameValuePair("block_user_id", userId));

        Log.d("user_id", "" + App.getInstance().getAppUserId());
        Log.d("block_user_id", "" + userId);

        try {
            AbstractHttpEntity entity = new UrlEncodedFormEntity(params);

            new HttpDataReader.Request.Builder(client)
                    .onUrl(Constants.METHOD_BLOCK_USERS)
                    .ofType(HttpDataReader.RequestType.POST)
                    .setPostEntity(entity).setRequestCode(BLOCK_USER)
                    .ofResponseType(BaseResponseModel.class)
                    .build()
                    .executeAsync();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }


    public static final void sendJoinTripRequest(String userId, DataReader.DataClient client) {


    }


    public static final void showProfile(String userId, DataReader.DataClient client) {


        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id", App.getInstance().getAppUserId()));
        params.add(new BasicNameValuePair("other_profile_user_id", userId));


        try {
            AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params);


            new HttpDataReader.Request.Builder(client)
                    .ofType(HttpDataReader.RequestType.POST)
                    .onUrl(Constants.METHOD_OTHERS_PROFILE)
                    .ofResponseType(UserProfileResponse.class)
                    .setRequestCode(Constants.OTHER_USER_DETAILS)
                    .setPostEntity(postEntity)
                    .build().executeAsync();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }

    public static final void showOwnProflie() {
    }

}
