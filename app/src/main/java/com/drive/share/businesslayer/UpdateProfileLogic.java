package com.drive.share.businesslayer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.android.utilities.datareader.DataReader;
import com.android.utilities.http.HttpDataReader;
import com.drive.share.data.webapi.BaseResponseModel;
import com.drive.share.data.webapi.UserModel;
import com.drive.share.data.webapi.UserPicUploadResponse;
import com.drive.share.globals.App;
import com.drive.share.globals.Constants;
import com.drive.share.globals.utils.BitmapUtils;
import com.drive.share.sharedrive.R;
import com.drive.share.storage.PreferenceUtils;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashesh on 5/2/15.
 */
public class UpdateProfileLogic {

    public static final int UPLOAD_PIC = 867;

    public static void showPictureSelector(final Context context, final Fragment fragment) {

        new AlertDialog.Builder(context).setTitle(R.string.title_select_picture).setPositiveButton(R.string.prompt_camera, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //open camera
                openPhotoPicker(fragment, Constants.CAMERA_CAPTURE);

            }
        }).setNegativeButton(R.string.prompt_gallery, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // open gallery
                openPhotoPicker(fragment, Constants.GALLERY_CAPTURE);
            }
        }).create().show();


    }


    public static void openPhotoPicker(Fragment context, int type) {

        Intent captureIntent = new Intent();
        int reqCode = 0;


        switch (type) {

            case Constants.CAMERA_CAPTURE:
                captureIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                reqCode = Constants.CAMERA_CAPTURE;
                break;
            case Constants.GALLERY_CAPTURE:
                captureIntent = new Intent(Intent.ACTION_PICK);
                captureIntent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                reqCode = Constants.GALLERY_CAPTURE;

                break;
        }

        context.startActivityForResult(captureIntent, reqCode);


    }




    public static Bitmap getCapturedImage(Fragment context, Intent data, int requestCode) {
        Bitmap bmp = null;
        if (requestCode == Constants.CAMERA_CAPTURE) {
            File f = new File(Environment.getExternalStorageDirectory().toString());
            for (File temp : f.listFiles()) {
                if (temp.getName().equals("temp.jpg")) {
                    f = temp;
                    break;
                }
            }
            try {
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                bmp = BitmapUtils.decodeBitmapFromFile(f.getAbsolutePath(), 100, 100);
               /* bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                        bitmapOptions);*/

                // viewImage.setImageBitmap(bitmap);
                String path = android.os.Environment
                        .getExternalStorageDirectory()
                        + File.separator
                        + "commontrip" + File.separator + "profiles";
                f.delete();
                OutputStream outFile = null;
                File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                try {
                    outFile = new FileOutputStream(file);
                    bmp.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                    outFile.flush();
                    outFile.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == Constants.GALLERY_CAPTURE) {

            Uri selectedImage = data.getData();
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor c = context.getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(filePath[0]);
            String picturePath = c.getString(columnIndex);
            c.close();
//            Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
            bmp = BitmapUtils.decodeBitmapFromFile(picturePath, 100, 100);

        }

        return bmp;
    }

    public static void validateAndUploadProfilePic(Context context, Bitmap bitmap, DataReader.DataClient client) {
        if (bitmap != null) {
            uploadPic(BitmapUtils.encodeTobase64(bitmap), client);
        } else {
            /*List<Validation.UI.FieldError> errorList = new ArrayList<Validation.UI.FieldError>();
            Validation.UI.FieldError error = new Validation.UI.FieldError(0, App.getInstance().getString(R.string.error_upload_image));
<<<<<<< HEAD
            errorList.add(error);
            client.onError(UPLOAD_PIC, errorList);
        }
    }

    public static void uploadPic(String image, DataReader.DataClient client) {
=======
            errorList.add(error);*/
            client.onError(UPLOAD_PIC, null);
        }
    }

    private static void uploadPic(String image, DataReader.DataClient client) {

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id", PreferenceUtils.getString(PreferenceUtils.KEY_USER_ID_UNIQUE)));
        params.add(new BasicNameValuePair("image", image));
        try {
            AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");
            new HttpDataReader.Request.Builder(client).onUrl(Constants.METHOD_UPLOAD_IMAGE)
                    .ofType(HttpDataReader.RequestType.POST)
                    .setRequestCode(UPLOAD_PIC)
                    .setPostEntity(postEntity)
                    .ofResponseType(UserPicUploadResponse.class)// response type
                    .build()
                    .executeAsync();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void updateProfile(View rootView, DataReader.DataClient client) {

      //  EditText txtUsername = (EditText) rootView.findViewById(R.id.txtUserName);
        EditText txtEmailAddress = (EditText) rootView.findViewById(R.id.txtEmailId);
      /*  RadioButton radioMale = (RadioButton) rootView.findViewById(R.id.radioMale);
        RadioButton radioFemale = (RadioButton) rootView.findViewById(R.id.radioFemale);
        TextView txtDob = (TextView) rootView.findViewById(R.id.txtDob);*/

        UserModel user = App.getInstance().getLoggedInUser();
      //  user.setUserName(txtUsername.getText().toString());
       // user.setDob(txtDob.getText().toString());

        if (!TextUtils.isEmpty(txtEmailAddress.getText()) &&
                android.util.Patterns.EMAIL_ADDRESS.matcher(txtEmailAddress.getText().toString().trim()).matches()) {
            user.setEmail(txtEmailAddress.getText().toString());

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("user_id", App.getInstance().getAppUserId()));
            params.add(new BasicNameValuePair("email_address", txtEmailAddress.getText().toString().trim()));
            try {
                AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params);
                new HttpDataReader.Request.Builder(client)
                        .onUrl(Constants.METHOD_EMAIL_REGISTRATION)
                        .ofType(HttpDataReader.RequestType.POST)
                        .ofResponseType(BaseResponseModel.class)
                        .setPostEntity(postEntity).build()
                        .executeAsync();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        }

       /* if (radioMale.isChecked()) {
            user.setGender("M");
        } else if (radioFemale.isChecked()) {
            user.setGender("F");
        }*/
        PreferenceUtils.insertObject(PreferenceUtils.KEY_USER_MODEL, user);
        //Todo make service call here
        //  client.onDataReceived(0,null);

    }
}





