package com.drive.share.businesslayer;

import android.provider.Settings;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.utilities.Validation;
import com.android.utilities.datareader.DataReader;
import com.android.utilities.http.HttpDataReader;
import com.drive.share.data.webapi.BaseResponseModel;
import com.drive.share.data.webapi.ContactSyncModel;
import com.drive.share.data.webapi.SignUpResponseModel;
import com.drive.share.data.webapi.UserModel;
import com.drive.share.globals.App;
import com.drive.share.globals.Constants;
import com.drive.share.sharedrive.R;
import com.drive.share.storage.PreferenceUtils;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashesh on 5/2/15.
 */
public class AuthLogic {

    public static final class SignUp {

        public static final int VALIDATE_INPUT = 555;
        public static final int SIGNUP_WEB = 422;
        private static String countryCode;

        public static UserModel getValidUser(View rootView, DataReader.DataClient client) {

            EditText txtFname, txtLname, txtMobNum;
            Spinner spnCountryCode;

            txtFname = (EditText) rootView.findViewById(R.id.txtFirstName);
            txtLname = (EditText) rootView.findViewById(R.id.txtLastName);
            txtMobNum = (EditText) rootView.findViewById(R.id.txtMobileNumber);
            spnCountryCode = (Spinner) rootView.findViewById(R.id.spnCountryCode);


            boolean valid = true;


            List<Validation.UI.FieldError> errorList = new ArrayList<Validation.UI.FieldError>();

            if (txtFname.getText().toString().trim().isEmpty()) {
                valid = false;
                Validation.UI.FieldError error = new Validation.UI.FieldError(txtFname.getId(), App.getInstance().getString(R.string.error_first_name));
                errorList.add(error);
            }
            if (txtLname.getText().toString().trim().isEmpty()) {
                valid = false;
                Validation.UI.FieldError error = new Validation.UI.FieldError(txtLname.getId(), App.getInstance().getString(R.string.error_last_name));
                errorList.add(error);
            }
           /* if (!android.util.Patterns.EMAIL_ADDRESS.matcher(txtEmail.getText().toString().trim()).matches()) {
                valid = false;
                Validation.UI.FieldError error = new Validation.UI.FieldError(txtEmail.getId(), App.getInstance().getString(R.string.error_email));
                errorList.add(error);

            }*/
            if (!Patterns.PHONE.matcher(txtMobNum.getText().toString().trim()).matches()) {
                valid = false;
                Validation.UI.FieldError error = new Validation.UI.FieldError(txtMobNum.getId(), App.getInstance().getString(R.string.error_phone));
                errorList.add(error);
            } /*else if (!txtMobNum.getText().toString().trim().startsWith("91")) {
                valid = false;
                Validation.UI.FieldError error = new Validation.UI.FieldError(txtMobNum.getId(), App.getInstance().getString(R.string.error_phone_start));
                errorList.add(error);
            }*/


            if (valid) {
                UserModel model = new UserModel();
                model.setFirstName(txtFname.getText().toString().trim());
                model.setLastName(txtLname.getText().toString().trim());

                countryCode = String.valueOf(spnCountryCode.getSelectedItem());

                PreferenceUtils.insertString(Constants.KEY_COUNTRY_CODE, countryCode);

                model.setMobNumber(txtMobNum.getText().toString().trim());

                Log.e("Mobile", model.getMobNumber());


                return model;

            } else {
                client.onError(VALIDATE_INPUT, errorList);
            }

            return null;
        }

        public static final void signUp(UserModel model, DataReader.DataClient client) {

            String android_id = Settings.Secure.getString(App.getInstance().getContentResolver(),
                    Settings.Secure.ANDROID_ID);


            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("country_code", countryCode));
            params.add(new BasicNameValuePair("mobile_number", model.getMobNumber()));
            params.add(new BasicNameValuePair("firstname", model.getFirstName()));
            params.add(new BasicNameValuePair("lastname", model.getLastName()));

            // params.add(new BasicNameValuePair("username", model.getUserName()));
            // params.add(new BasicNameValuePair("email_address", model.getEmail()));
            params.add(new BasicNameValuePair("device_id", android_id));


            try {
                AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");
                new HttpDataReader.Request.Builder(client).onUrl(Constants.METHOD_SIGN_UP)
                        .ofType(HttpDataReader.RequestType.POST)
                        .setRequestCode(SIGNUP_WEB)
                        .setPostEntity(postEntity)
                        .ofResponseType(SignUpResponseModel.class)// response type
                        .build()
                        .executeAsync();
                Log.e("REq sent", Constants.METHOD_SIGN_UP);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        }

        /*public static final void signUp(Context context, UserModel model, Response.Listener listener, Response.ErrorListener errorListener) {

            String android_id = Settings.Secure.getString(App.getInstance().getContentResolver(),
                    Settings.Secure.ANDROID_ID);


//            List<NameValuePair> params = new ArrayList<NameValuePair>();

            Map<String, String> params = new HashMap<String, String>();
            params.put("country_code", countryCode);
            params.put("mobile_number", model.getMobNumber());
            params.put("firstname", model.getFirstName());
            params.put("lastname", model.getLastName());

            // params.add(new BasicNameValuePair("username", model.getUserName()));
            // params.add(new BasicNameValuePair("email_address", model.getEmail()));
            params.put("device_id", android_id);

            EntityRequest<SignUpResponseModel> signUpRequest = new EntityRequest<SignUpResponseModel>(Request.Method.POST, Constants.METHOD_SIGN_UP, listener, errorListener, SignUpResponseModel.class);

            *//*try {
                AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");
                new HttpDataReader.Request.Builder(client).onUrl(Constants.METHOD_SIGN_UP)
                        .ofType(HttpDataReader.RequestType.POST)
                        .setRequestCode(SIGNUP_WEB)
                        .setPostEntity(postEntity)
                        .ofResponseType(SignUpResponseModel.class)// response type
                        .build()
                        .executeAsync();
                Log.e("REq sent", Constants.METHOD_SIGN_UP);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }*//*
            signUpRequest.setParams(params);

//            Commons.setRetryPolicy(userAuthRequest);

            Volley.newRequestQueue(context).add(signUpRequest);

        }*/

        public static boolean processResponse(Object data, DataReader.DataClient client, UserModel user) {
            if (data instanceof SignUpResponseModel) {
                SignUpResponseModel signUp = (SignUpResponseModel) data;

                if (signUp.getStatus()) {
                    Log.e("V_CODE", signUp.getVerificationCode());
                    user.setUserId(signUp.getUserId());
                    user.setCountryCode("" + signUp.getUserData().getCountryCode());
                    PreferenceUtils.insertString(PreferenceUtils.KEY_USER_ID_UNIQUE, signUp.getUserId());
                    PreferenceUtils.insertObject(PreferenceUtils.KEY_USER_MODEL, user);
                    PreferenceUtils.insertBoolean(PreferenceUtils.KEY_USER_NEW, signUp.isIs_new_user());

                    /**
                     * Until the new logic is implemented
                     */

                    PreferenceUtils.insertString(PreferenceUtils.KEY_VERIFICATION_CODE, signUp.getVerificationCode());


                    /**
                     * Do not mark user as logged in until the verification is done
                     */
                    // App.getInstance().setLoggedInUser(user);

                    if (!signUp.isIs_new_user()) {
                        try {
                            Log.e("METHOD_FETCH_SYNC call","user -- "+signUp.isIs_new_user());
                            Log.e("METHOD_FETCH_SYNC call","Started------------");
                            List<NameValuePair> params = new ArrayList<NameValuePair>();
                            params.add(new BasicNameValuePair("user_id", signUp.getUserId()));
                            Log.d("user_id", signUp.getUserId());
                            AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params);
                            new HttpDataReader.Request.Builder(App.getInstance())
                                    .onUrl(Constants.METHOD_FETCH_SYNC)
                                    .ofResponseType(ContactSyncModel.class)
                                    .ofType(HttpDataReader.RequestType.POST).setPostEntity(postEntity).setRequestCode(ContactLogic.REQ_UPLOAD_CONTACTS)
                                    .build().executeAsync();
                            Log.e("METHOD_FETCH_SYNC call","Ended------------");
                        } catch (UnsupportedEncodingException usee) {
                            usee.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    return true;
                }
            } else {
                client.onError(SIGNUP_WEB, data);
                return false;
            }

            return false;

        }


    }

    public static final void sendVerificationCode(String verificationCode, DataReader.DataClient client) {

        String android_id = Settings.Secure.getString(App.getInstance().getContentResolver(),
                Settings.Secure.ANDROID_ID);


        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair(" user_id", App.getInstance().getAppUserId()));
        //params.add(new BasicNameValuePair("verification_status", model.getMobNumber()));
        params.add(new BasicNameValuePair("verification_code", verificationCode));


        try {
            AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");
            new HttpDataReader.Request.Builder(client).onUrl(Constants.METHOD_REGISTER_VERFICATION_CODE)
                    .ofType(HttpDataReader.RequestType.POST)
                    .setRequestCode(Constants.REQ_VERIFICATION_CODE)
                    .setPostEntity(postEntity)
                    .ofResponseType(BaseResponseModel.class)// response type
                    .build()
                    .executeAsync();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }


    public static final class Verification {

        public static final boolean verifyCode(String code) {
            String storedCode = null;
            if ((storedCode = PreferenceUtils.getString(PreferenceUtils.KEY_VERIFICATION_CODE)) != null && storedCode.equals(code)) {
                PreferenceUtils.insertBoolean(PreferenceUtils.KEY_IS_UESR_VERIFIED,true);
                return true;
            }
            return false;
        }

    }


}
