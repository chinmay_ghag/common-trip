package com.drive.share.businesslayer;

import com.drive.share.data.webapi.TripModel;

/**
 * Created by yashesh on 21/2/15.
 */
public class TripTimeSettings {

    public static enum RepeatType {

        NEVER, WEEKLY, RANDOM;

    }

    private RepeatType repeatType;
    String repeatDays;
    String startDate, startTime, endDate, endTime;

    public RepeatType getRepeatType() {
        return repeatType;
    }

    public void setRepeatType(RepeatType repeatType) {
        this.repeatType = repeatType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getRepeatDays() {
        return repeatDays;
    }

    public void setRepeatDays(String repeatDays) {
        this.repeatDays = repeatDays;
    }

    public void setTripTime(TripModel trip) {

        switch (repeatType) {

            case NEVER:
                trip.setStart_date(startDate);
                trip.setEnd_date(startDate);
                trip.setStart_time(startTime);
                trip.setEnd_time(endTime);
                trip.setTrip_frequency("3");
                break;
            case WEEKLY:
                trip.setStart_date(startDate);
                trip.setEnd_date(null);
                trip.setStart_time(startTime);
                trip.setEnd_time(endTime);
                trip.setTrip_frequency("1");
                trip.setTrip_days(repeatDays);

                break;
        }


    }

}
