package com.drive.share.businesslayer;

import android.util.Log;

import com.android.utilities.datareader.DataReader;
import com.android.utilities.http.HttpDataReader;
import com.drive.share.data.webapi.BaseResponseModel;

//import com.drive.share.data.webapi.TripAction;
import com.drive.share.globals.Constants;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pervacio-Dev on 3/11/2015.
 */
public class UserRequestLogic {

    private static final int REMOVE_USER = 102;
    public static final int TRIP_ACTION = 103;
    public static final void respondToRequest(String request_user_id, String trip_card_id, String response, String user_id , DataReader.DataClient client){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("request_user_id", request_user_id));
        params.add(new BasicNameValuePair("trip_card_id", trip_card_id));
        params.add(new BasicNameValuePair("response", response));
        params.add(new BasicNameValuePair("user_id", user_id));

        Log.d("Data request", request_user_id + "-- " +  trip_card_id  + "-- " + response  + "-- " +  user_id);

        try {
            AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");
            new HttpDataReader.Request.Builder(client).onUrl(Constants.METHOD_RESPOND_TO_REQUEST)
                    .ofType(HttpDataReader.RequestType.POST)
                    .setRequestCode(REMOVE_USER)
                    .setPostEntity(postEntity)
                    .ofResponseType(BaseResponseModel.class)// response type
                    .build()
                    .executeAsync();
            Log.e("REq sent", Constants.METHOD_RESPOND_TO_REQUEST);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    //1 - if driver
    //else 0
    public static final void sendTripAction(String user_id, String trip_card_id, String trip_status,String user_name, String lat, String lon, int ifDriver, DataReader.DataClient client){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id", user_id));
        params.add(new BasicNameValuePair("trip_card_id", trip_card_id));
        params.add(new BasicNameValuePair("trip_status", trip_status));
        params.add(new BasicNameValuePair("user_name", user_name));//TODO: need to passusername confirm driver or user ?
        params.add(new BasicNameValuePair("origin_latitude", lat));
        params.add(new BasicNameValuePair("origin_longitude", lon));
        params.add(new BasicNameValuePair("is_driver", Integer.toString(ifDriver)));
        try {
            AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");
//            new HttpDataReader.Request.Builder(client).onUrl(Constants.METHOD_SEND_TRIP_ACTION)
//                    .ofType(HttpDataReader.RequestType.POST)
//                    .setRequestCode(TRIP_ACTION)
//                    .setPostEntity(postEntity)
//                    .ofResponseType(TripAction.class)// response type
//                    .build()
//                    .executeAsync();
            Log.e("REq sent", Constants.METHOD_SEND_TRIP_ACTION);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
