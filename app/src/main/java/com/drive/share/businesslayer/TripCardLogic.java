package com.drive.share.businesslayer;

import com.android.utilities.datareader.DataReader;
import com.android.utilities.http.HttpDataReader;
import com.drive.share.data.webapi.BaseResponseModel;
import com.drive.share.data.webapi.TripCardResponseModel;
import com.drive.share.globals.App;
import com.drive.share.globals.Constants;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pervacio-Dev on 3/4/2015.
 */
public class TripCardLogic {
    public static final int TRIP_DETAILS = 201;
    public static final int LEAVE_TRIP = 202;

   public static final void tripDetails(String tripCardId,DataReader.DataClient client){
       List<NameValuePair> params=new ArrayList<NameValuePair>();
       params.add(new BasicNameValuePair("user_id", App.getInstance().getAppUserId()));//1000068 //"1000105"
       params.add(new BasicNameValuePair("trip_card_id",tripCardId));
       try {
           AbstractHttpEntity entity=new UrlEncodedFormEntity(params);

           new HttpDataReader.Request.Builder(client)
                   .onUrl(Constants.METHOD_TRIP_DETAILS)
                   .ofType(HttpDataReader.RequestType.POST)
                   .setPostEntity(entity).setRequestCode(LEAVE_TRIP)
                   .ofResponseType(TripCardResponseModel.class)
                   .build()
                   .executeAsync();

       } catch (UnsupportedEncodingException e) {
           e.printStackTrace();
       }
   }

    public static final void leaveTrip(String tripCardId,DataReader.DataClient client){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id",App.getInstance().getAppUserId()));//1000068 "1000105"
        params.add(new BasicNameValuePair("trip_card_id",tripCardId));
        try {
            AbstractHttpEntity entity=new UrlEncodedFormEntity(params);

            new HttpDataReader.Request.Builder(client)
                    .onUrl(Constants.METHOD_LEAVE_TRIP)
                    .ofType(HttpDataReader.RequestType.POST)
                    .setPostEntity(entity).setRequestCode(TRIP_DETAILS)
                    .ofResponseType(BaseResponseModel.class)
                    .build()
                    .executeAsync();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
