package com.drive.share.businesslayer;

import android.os.Build;

import com.android.utilities.datareader.DataReader;
import com.android.utilities.http.HttpDataReader;
import com.drive.share.data.webapi.DeviceDetailModel;
import com.drive.share.globals.App;
import com.drive.share.globals.Constants;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashesh on 10/2/15.
 */
public class DeviceDetailLogic {


    public static void sendDeviceDetails(DataReader.DataClient client, String registrationId) {


        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id", App.getInstance().getAppUserId()));
        params.add(new BasicNameValuePair("registration_id", registrationId));
        params.add(new BasicNameValuePair("os_type", "android"));
        params.add(new BasicNameValuePair("device_name", Build.DEVICE));
        params.add(new BasicNameValuePair("os_version", Build.VERSION.CODENAME));


        try {
            AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");


            new HttpDataReader.Request.Builder(client).onUrl(Constants.METHOD_DEVICE_DETAILS).setRequestCode(Constants.DEVICE_DETAILS).ofType(HttpDataReader.RequestType.POST).setPostEntity(postEntity).ofResponseType(DeviceDetailModel.class).build().execute();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NullPointerException npe) {
            npe.printStackTrace();

        }

    }


}
