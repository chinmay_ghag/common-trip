package com.drive.share.businesslayer;

import com.android.utilities.datareader.DataReader;
import com.android.utilities.http.HttpDataReader;
import com.drive.share.data.webapi.ThirdPartyDetailResponseModel;
import com.drive.share.globals.App;
import com.drive.share.globals.Constants;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by atulOholic on 9/3/15.
 */
public class ThirdPartyLogic {

    public static void loadThirdParyDetails(
            DataReader.DataClient client, String thirdPartyId) {


        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id", App.getInstance().getAppUserId()));
        params.add(new BasicNameValuePair("other_profile_user_id", thirdPartyId));


        try {
            AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");
            new HttpDataReader.Request.Builder(client).onUrl(Constants.METHOD_CONTACTS_SYNC)
                    .ofType(HttpDataReader.RequestType.POST)
                    .setRequestCode(Constants.REQ_THIRD_PARTY_DETAIL)
                    .setPostEntity(postEntity)
                    .ofResponseType(ThirdPartyDetailResponseModel.class)// response type
                    .build()
                    .executeAsync();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
}
