package com.drive.share.businesslayer;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;
import android.util.Patterns;

import com.android.utilities.datareader.DataReader;
import com.android.utilities.http.HttpDataReader;
import com.drive.share.dao.UserDao;
import com.drive.share.data.webapi.ContactSyncModel;
import com.drive.share.data.webapi.UserModel;
import com.drive.share.globals.App;
import com.drive.share.globals.Constants;
import com.drive.share.globals.utils.Commons;
import com.drive.share.loaders.ContactsLoader;
import com.drive.share.storage.PreferenceUtils;
import com.opendroid.db.DbHelper;
import com.opendroid.db.dao.DAOException;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <<<<<<< HEAD
 * Created by webwerks on 13/2/15.
 * =======
 * Created by yashesh on 13/2/15.
 * >>>>>>> 8877a545da73a492a64947a836574b1296ac1a9d
 */
public class ContactLogic {

    public static final short REQ_UPLOAD_CONTACTS = 654;


    public static final void resyncContact(java.util.List<UserModel> lstContactModels,
                                           DataReader.DataClient client) {
        {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            try {
                Log.e("resync req params start", ".........................");
                params.add(new BasicNameValuePair("user_id", PreferenceUtils.getString(PreferenceUtils.KEY_USER_ID_UNIQUE)));
                Log.e("user_id", PreferenceUtils.getString(PreferenceUtils.KEY_USER_ID_UNIQUE));

                Date date = new Date(System.currentTimeMillis());
                if (PreferenceUtils.getString(PreferenceUtils.KEY_LAST_SYNC_TIME) != null) {
                    params.add(new BasicNameValuePair("last_updated_date", PreferenceUtils.getString(PreferenceUtils.KEY_LAST_SYNC_TIME)));
                    Log.e("last_updated_date", PreferenceUtils.getString(PreferenceUtils.KEY_LAST_SYNC_TIME));
                }
                Log.e("resync req params end", ".........................");
            } catch (Exception e) {
                e.printStackTrace();
            }


            /*if (lstContactModels != null && lstContactModels.size() > 0) {
                java.lang.StringBuilder strContactno = new java.lang.StringBuilder();
                for (UserModel model : lstContactModels) {


                    model.setMobNumber(Commons.getValidatedPhoneNumber(model.getMobNumber()));

                    // String mobNum=model.getMobilePhone();
                    // Log.e("CONTACT",model.getMobilePhone());
                    if (Patterns.PHONE.matcher(model.getMobNumber().toString().trim()).matches()) {
                        strContactno.append(model.getMobNumber() + ",");
                    }
                }

                params.add(new BasicNameValuePair("contacts", strContactno.substring(0, strContactno.length() - 1)));
                Log.e("Contacts", " - User ID - " + App.getInstance().getAppUserId() + " Mobile Nos - " +
                        strContactno.substring(0, strContactno.length() - 1));
            }*/


            //Log.e("params", strContactno.substring(0, strContactno.length() - 1) + "\n" + PreferenceUtils.getString(PreferenceUtils.KEY_USER_ID_UNIQUE));

            try {
                AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");
                new HttpDataReader.Request.Builder(client).onUrl(Constants.METHOD_CONTACT_RESYNC)
                        .ofType(HttpDataReader.RequestType.POST)
                        .setRequestCode(REQ_UPLOAD_CONTACTS)
                        .setPostEntity(postEntity)
                        .ofResponseType(ContactSyncModel.class)// response type
                        .build()
                        .executeAsync();

                Log.d("Alpha","resync exceuted");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        }
    }

    public static void processContactSyncData(ContactSyncModel data) {

        Log.e("processContactSyncData","start db opertaion");
        ContactSyncModel contacts = ((ContactSyncModel) data);
        if (contacts.getLstSyncData() != null) {

            Log.d("Alpha", "resync data " + contacts.getLstSyncData().size());

            UserDao dao =
                    new UserDao(App.getInstance(),
                            DbHelper.getInstance(App.getInstance()).getSQLiteDatabase());

            for (UserModel user : contacts.getLstSyncData()) {
                try {
                    //Linus here
                    user.setMobNumber(Commons.getValidatedPhoneNumber(user.getMobNumber()));
                    dao.createOrUpdate(user);
                } catch (DAOException e) {
                    e.printStackTrace();
                }
            }
        }else{
            Log.d("Alpha", "resync data is null");
        }

        PreferenceUtils.insertString(PreferenceUtils.KEY_LAST_SYNC_TIME, Commons.getUnixTime());

    }


    public static void uploadContacts(java.util.List<UserModel> lstContactModels,
                                      DataReader.DataClient client) {


        if (lstContactModels != null && lstContactModels.size() > 0) {


            Log.e("Alpha", "uploadContacts Size - " + lstContactModels.size() + " - " + App.getInstance().getAppUserId());

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            java.lang.StringBuilder strContactno = new java.lang.StringBuilder();
            Log.e("uploadContacts req on",".................................");
            for (UserModel model : lstContactModels) {

                model.setMobNumber(Commons.getValidatedPhoneNumber(model.getMobNumber()));
                // String mobNum=model.getMobilePhone();
                // Log.e("CONTACT",model.getMobilePhone());
                if (Patterns.PHONE.matcher(model.getMobNumber().toString().trim()).matches()) {
                    strContactno.append(model.getMobNumber() + ",");
                }


                try {

                    params.add(new BasicNameValuePair("user_id", PreferenceUtils.getString(PreferenceUtils.KEY_USER_ID_UNIQUE)));
                    params.add(new BasicNameValuePair("contacts", strContactno.substring(0, strContactno.length() - 1)));

//                    Log.e("user_id", PreferenceUtils.getString(PreferenceUtils.KEY_USER_ID_UNIQUE));
//                    Log.e("contacts", strContactno.substring(0, strContactno.length() - 1));


                } catch (Exception e) {
                    e.printStackTrace();
                }
                //Log.e("params", strContactno.substring(0, strContactno.length() - 1) + "\n" + PreferenceUtils.getString(PreferenceUtils.KEY_USER_ID_UNIQUE));

            }
            Log.e("user_id", PreferenceUtils.getString(PreferenceUtils.KEY_USER_ID_UNIQUE));
            Log.e("contacts", ""+strContactno);
            Log.e("uploadContacts req end",".................................");
//            Log.e("Alpha", "uploadContacts Size - " + strContactno);

            try {
                AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");
                new HttpDataReader.Request.Builder(client).onUrl(Constants.METHOD_CONTACTS_SYNC)
                        .ofType(HttpDataReader.RequestType.POST)
                        .setRequestCode(REQ_UPLOAD_CONTACTS)
                        .setPostEntity(postEntity)
                        .ofResponseType(ContactSyncModel.class)// response type
                        .build()
                        .executeAsync();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        } else {
            client.onDataEmpty(REQ_UPLOAD_CONTACTS);
        }
    }


    public static void saveLastContactUpdatedTime(Context context) {

        try {
            Log.e("saveLastContUpdteTime","start db opertaion");
            Cursor contactsCursor = context.getContentResolver()
                    .query(ContactsContract.Contacts.CONTENT_URI,
                            new String[]{ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP}, null
                            , null, null);
            // content://com.android.contacts
            long lastUpdatedTime = 0;
            if (contactsCursor != null && contactsCursor.getCount() > 0) {
                contactsCursor.moveToFirst();
                while (!contactsCursor.isAfterLast()) {

                    if (lastUpdatedTime < contactsCursor.getLong(contactsCursor.
                            getColumnIndex(ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP)))
                        lastUpdatedTime = contactsCursor.getLong(contactsCursor.
                                getColumnIndex(ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP));
                    contactsCursor.moveToNext();
                }
                contactsCursor.close();
            }

            // save in preference
            Log.e("Alpha", "PREF TIME ::" + lastUpdatedTime);
            PreferenceUtils.insertString(PreferenceUtils.KEY_LAST_CONTACT_UPDATE_TIME, lastUpdatedTime + "");
        } catch (IllegalArgumentException iae) {
            iae.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void syncModifiedContacts(Context context, DataReader.DataClient client) {

        Cursor contactsCursor = context.getContentResolver()
                .query(ContactsContract.Contacts.CONTENT_URI,
                        null, ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP + " > ?",
                        new String[]{PreferenceUtils.
                                getString(PreferenceUtils.KEY_LAST_CONTACT_UPDATE_TIME)}, null);
        // content://com.android.contacts


        List<UserModel> contacts = new ArrayList<UserModel>();

        if (contactsCursor != null && contactsCursor.getCount() > 0) {
            contactsCursor.moveToFirst();
            while (!contactsCursor.isAfterLast()) {

                int phoneCount = contactsCursor.getInt(contactsCursor
                        .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                if (phoneCount > 0) {
                    contacts.add(ContactsLoader.parseCursor(contactsCursor));
                }
                contactsCursor.moveToNext();
            }

            contactsCursor.close();

        }
        saveLastContactUpdatedTime(context);
        // re sync contacts
        resyncContact(contacts, client);
    }


}
