package com.drive.share.businesslayer;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.android.utilities.datareader.DataReader;
import com.android.utilities.http.HttpDataReader;
import com.drive.share.dao.TripDao;
import com.drive.share.dao.VehicleDao;
import com.drive.share.data.webapi.BaseResponseModel;
import com.drive.share.data.webapi.ThirdPartyTripsResponseModel;
import com.drive.share.data.webapi.TripModel;
import com.drive.share.data.webapi.UserTripResponse;
import com.drive.share.data.webapi.VehicleModel;
import com.drive.share.globals.App;
import com.drive.share.globals.Constants;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.CreateTripActivity;
import com.drive.share.ui.base.BaseActivity;
import com.drive.share.ui.fragments.editor.TripMapFragment;
import com.opendroid.db.DbHelper;
import com.opendroid.db.dao.DAOException;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashesh on 16/2/15.
 */
public class TripEdtiorLogic {

    public static final int CREATE_TRIP = 564;
    public static final int GET_ADDRESS = 71;

    public static final TripModel getTrip(String name) {
        TripModel trip = new TripModel();
        trip.setUser_id(App.getInstance().getAppUserId());
        //  trip.setTrip_frequency("3");
        //  trip.setVehicle_id(PreferenceUtils.getString(PreferenceUtils.KEY_VEHICLE_ID));
        trip.setTrip_name(name);

        return trip;
    }


    public static List<VehicleModel> getLocalVhicles() {
        return new VehicleDao(App.getInstance(), DbHelper.getInstance(App.getInstance())
                .getSQLiteDatabase())
                .findAll();
    }

/**
 * TODO: Hardcoded done here for test purpose
 * */
    public static void createTrip(FragmentActivity activity, DataReader.DataClient client) {

        TripModel trip = getCreateTripActivity(activity).getTripModel();

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id", trip.getUser_id()));
        params.add(new BasicNameValuePair("trip_name", trip.getTrip_name()));
        params.add(new BasicNameValuePair("trip_frequency", trip.getTrip_frequency()));
//        params.add(new BasicNameValuePair("vehicle_id", trip.getVehicle_id()));
        params.add(new BasicNameValuePair("vehicle_id", "100095"));
//        params.add(new BasicNameValuePair("origin_latitude", trip.getOrigin_latitude()));
//        params.add(new BasicNameValuePair("origin_longitude", trip.getOrigin_longitude()));
        params.add(new BasicNameValuePair("origin_latitude", "1.1"));
        params.add(new BasicNameValuePair("origin_longitude", "1.1"));
        params.add(new BasicNameValuePair("origin_name", trip.getOrigin_name()));
        params.add(new BasicNameValuePair("destination_latitude", trip.getDestination_latitude()));
        params.add(new BasicNameValuePair("destination_longitude", trip.getDestination_longitude()));
        params.add(new BasicNameValuePair("destination_name", trip.getDestination_name()));
        params.add(new BasicNameValuePair("create_departure_trip", trip.getCreate_departure_trip()));
        params.add(new BasicNameValuePair("start_date", trip.getStart_date()));
//        params.add(new BasicNameValuePair("end_date", trip.getEnd_date()));
        params.add(new BasicNameValuePair("end_date", trip.getStart_date()));
        params.add(new BasicNameValuePair("start_time", trip.getStart_time()));
        params.add(new BasicNameValuePair("end_time", trip.getEnd_time()));
        params.add(new BasicNameValuePair("trip_days", trip.getTrip_days()));
        params.add(new BasicNameValuePair("is_pick_up", trip.is_pick_up()));

        Log.e("trip params", ".................................");
        Log.e("user_id", trip.getUser_id());
        Log.e("trip_name", trip.getTrip_name());
        Log.e("trip_frequency", trip.getTrip_frequency());
        Log.e("origin_latitude", trip.getOrigin_latitude());
        Log.e("origin_longitude", trip.getOrigin_longitude());
        Log.e("destination_latitude", trip.getDestination_latitude());
        Log.e("destination_name", trip.getDestination_name());
        Log.e("origin_name", trip.getOrigin_name());
        Log.e("destination_longitude", trip.getDestination_longitude());
        Log.e("create_departure_trip", trip.getCreate_departure_trip());
        Log.e("start_date", "" + trip.getStart_date());
        Log.e("end_date", "" + trip.getEnd_date() + "");
        Log.e("start_time", "" + trip.getStart_time());
        Log.e("end_time", "" + trip.getEnd_time());
        Log.e("trip_days", "" + trip.getTrip_days());
        Log.e("vehicle_id", "" + trip.getVehicle_id());
        Log.e("is_pick_up", "" + trip.is_pick_up());

        Log.e("/trip params", ".................................");
        try {
            AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");
            new HttpDataReader.Request.Builder(client)
                    .ofType(HttpDataReader.RequestType.POST)
                    .onUrl(Constants.METHOD_TRIP_CREATE)
                    .ofResponseType(TripModel.class)
                    .setRequestCode(CREATE_TRIP)
                    .setPostEntity(postEntity)
                    .build()
                    .executeAsync();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }

    public static void getLocationName(final double currentLat, final double currentLong, final Geocoder geoCoder, final DataReader.DataClient client) {

        final Handler handlerCurrentAddress = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                String currentAddress = (String) msg.obj;
                currentAddress = currentAddress.replace("null", "");
                //send this to dataclient
                client.onDataReceived(GET_ADDRESS, currentAddress);
            }
        };

        new Thread() {
            public void run() {

                List<Address> addresses;
                try {
                    String result = null;
                    addresses = geoCoder.getFromLocation(currentLat,
                            currentLong, 1);
                    String newAddress = "";
                    if (addresses.size() > 0) {
                        if (addresses.get(0).getFeatureName() != null) {
                            newAddress += addresses.get(0).getFeatureName();
                        }

                        newAddress.replace("null", "Unable to get address for this lat-long.");
                        Message msg = new Message();
                        msg.obj = newAddress;
                        handlerCurrentAddress.sendMessage(msg);
                    }
                } catch (IOException e) {
                    Message msg = new Message();
                    msg.obj = "Unable to get address for this lat-long.";
                    handlerCurrentAddress.sendMessage(msg);
                    client.onError(GET_ADDRESS,e);
                    e.printStackTrace();
                }

            }
        }.start();
    }


    public static void getUserTrip(DataReader.DataClient client) {

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id", App.getInstance().getAppUserId()));
        try {
            AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");

            new HttpDataReader.Request.Builder(client).onUrl(Constants.METHOD_GET_TRIPS)
                    .ofType(HttpDataReader.RequestType.POST).setPostEntity(postEntity)
                    .ofResponseType(UserTripResponse.class).setRequestCode(Constants.REQ_LOAD_TRIPS)
                    .build().executeAsync();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }

    public static void getThirdPartyUserTrip(DataReader.DataClient client, String user_id) {

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id", App.getInstance().getAppUserId()));
        params.add(new BasicNameValuePair("other_profile_user_id", user_id));
        try {
            AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");

            new HttpDataReader.Request.Builder(client).onUrl(Constants.METHOD_THIRD_PARTY_TRIPS)
                    .ofType(HttpDataReader.RequestType.POST).setPostEntity(postEntity)
                    .ofResponseType(ThirdPartyTripsResponseModel.class).setRequestCode(Constants.REQ_LOAD_TRIPS)
                    .build().executeAsync();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }

    public static void joinTripRequest(DataReader.DataClient client, String driverId, String tripId, String latitude, String longitude, String placeName) {

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id", App.getInstance().getAppUserId()));
        params.add(new BasicNameValuePair("trip_card_id", tripId));
        params.add(new BasicNameValuePair("to_user_id", driverId));
        params.add(new BasicNameValuePair("pick_up_latitude", latitude));
        params.add(new BasicNameValuePair("pick_up_longitude", longitude));
        params.add(new BasicNameValuePair("pick_up_location_name", placeName));

        try {
            AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");

            new HttpDataReader.Request.Builder(client).onUrl(Constants.METHOD_JOIN_TRIP)
                    .ofType(HttpDataReader.RequestType.POST).setPostEntity(postEntity)
                    .ofResponseType(BaseResponseModel.class).setRequestCode(Constants.REQ_LOAD_TRIPS)
                    .build().executeAsync();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }


    public static void inviteUsers(DataReader.DataClient client, String tripId, List<String> userIds) {

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id", App.getInstance().getAppUserId()));
        params.add(new BasicNameValuePair("trip_card_id", tripId));

        String users = "";

        for (String id : userIds) {
            users += id + ",";
        }


        users = users.substring(0, users.length() - 1);

        params.add(new BasicNameValuePair("invited_user_id", users));

        Log.d("user_id", "" + App.getInstance().getAppUserId());
        Log.d("trip_card_id", "" + tripId);
        Log.d("invited_user_id", "" + users);

        try {
            AbstractHttpEntity postEntity = new UrlEncodedFormEntity(params, "UTF-8");

            new HttpDataReader.Request.Builder(client).onUrl(Constants.METHOD_INVITE_USERS)
                    .ofType(HttpDataReader.RequestType.POST).setPostEntity(postEntity)
                    .ofResponseType(BaseResponseModel.class).setRequestCode(Constants.REQ_INVITE_USERS)
                    .build().executeAsync();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }


    public static void showLocationSettings(BaseActivity activity, boolean isStartTrip) {


        TripMapFragment mapFrag = new TripMapFragment();
        Bundle argsMap = new Bundle();
        argsMap.putBoolean(Constants.KEY_IS_SOURCE_ADDRESS, isStartTrip);
        argsMap.putInt(TripMapFragment.COMING_FROM, TripMapFragment.COMING_FROM_CREATE);
        mapFrag.setArguments(argsMap);
        activity.fragmentTransaction(BaseActivity.REPLACE_FRAGMENT, mapFrag, R.id.frameContainer, true);


    }

    public static CreateTripActivity getCreateTripActivity(FragmentActivity activity) {
        return ((CreateTripActivity) activity);
    }


    public static void setLocation(FragmentActivity activity, double startLat, double startLong, String locationName, boolean isSourceAddress) {

        CreateTripActivity editorActivity = getCreateTripActivity(activity);
        TripModel trip = editorActivity.getTripModel();
        if (isSourceAddress) {
            //    trip.setCreate_departure_trip("0");
            trip.setOrigin_latitude(startLat + "");
            trip.setOrigin_longitude(startLong + "");
            trip.setOrigin_name(locationName);
        } else {
            trip.setDestination_latitude(startLat + "");
            trip.setDestination_longitude(startLong + "");
            trip.setDestination_name(locationName);
        }
    }

    public static void storeTripLocal(TripModel model) {
        try {
            new TripDao(App.getInstance(), DbHelper.getInstance(App.getInstance()).getSQLiteDatabase()).create(model);
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }
}
