package com.drive.share.businesslayer;

import android.view.View;
import android.widget.EditText;

import com.android.utilities.Validation;
import com.android.utilities.datareader.DataReader;
import com.android.utilities.http.HttpDataReader;
import com.drive.share.dao.VehicleDao;
import com.drive.share.data.webapi.VehicleModel;
import com.drive.share.data.webapi.VehicleRegistrationResponse;
import com.drive.share.globals.App;
import com.drive.share.globals.Constants;
import com.drive.share.sharedrive.R;
import com.opendroid.db.DbHelper;
import com.opendroid.db.dao.DAOException;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashesh on 10/2/15.
 */
public class VehicleLogic {

    public static final int REQUEST_REGISTER_VEHICLE = 7824;

    public static final void registerVehicle(DataReader.DataClient client, VehicleModel vehicle) {

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("vehicle_registration_number", vehicle.getVehicle_registration_number()));
        params.add(new BasicNameValuePair("vehicle_name", vehicle.getVehicle_name()));
        params.add(new BasicNameValuePair("vehicle_type", "" + vehicle.getVehicle_type()));
        params.add(new BasicNameValuePair("description", vehicle.getDescription()));
        params.add(new BasicNameValuePair("image", vehicle.getImage()));
        params.add(new BasicNameValuePair("user_id", vehicle.getUser_id()));


        try {
            AbstractHttpEntity postEntitiy = new UrlEncodedFormEntity(params, "UTF-8");
            new HttpDataReader.Request.Builder(client)
                    .onUrl(Constants.METHOD_REGISTER_VEHICLE)
                    .setRequestCode(REQUEST_REGISTER_VEHICLE)
                    .setPostEntity(postEntitiy)
                    .ofType(HttpDataReader.RequestType.POST)
                    .ofResponseType(VehicleRegistrationResponse.class)
                    .build().executeAsync();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }

    public static VehicleModel validateVehicleData(View rootView, DataReader.DataClient client) {

        boolean valid = true;
        EditText txtVname = (EditText) rootView.findViewById(R.id.txtVehicleName);
        EditText txtRegNo = (EditText) rootView.findViewById(R.id.txtRegistrationNumber);
        EditText txtVdes = (EditText) rootView.findViewById(R.id.txtDescription);

        List<Validation.UI.FieldError> errorList = new ArrayList<Validation.UI.FieldError>();


        if (!(txtVname.getText().toString()
                .trim().length() > 0)) {
            valid = false;
            errorList.add(new Validation.UI.FieldError(txtVname.getId(), ""));

        }

        if (!(txtRegNo.getText().toString()
                .trim().length() > 0)) {
            valid = false;
            errorList.add(new Validation.UI.FieldError(txtRegNo.getId(), ""));
        }


        if (valid) {
            VehicleModel vehicle = new VehicleModel();
            //fill the data with fields.
            vehicle.setDescription(txtVdes.getText().toString().trim());
            vehicle.setUser_id(App.getInstance().getAppUserId());
            vehicle.setVehicle_name(txtVname.getText().toString().trim());

            vehicle.setVehicle_registration_number(txtRegNo.getText().toString().trim());
            //TODO:set vehicle image .

            /////////////////////
            return vehicle;
        } else {
            client.onError(REQUEST_REGISTER_VEHICLE, errorList);
        }
        return null;
    }


    public static void storeVehicleLocal(VehicleModel model){
        try {
            new VehicleDao(App.getInstance(),DbHelper.getInstance(App.getInstance()).getSQLiteDatabase()).create(model);
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }


}
