package com.drive.share.ui.fragments.tripcard;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.drive.share.adapters.TripCardInfoPagerAdapter;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.tripcard.TripCardActivity;
import com.drive.share.ui.base.BaseFragment;
import com.drive.share.ui.customviews.CustomViewPager;
import com.drive.share.ui.customviews.SlidingTabLayout;

/**
 * Created by Pervacio-Dev on 3/4/2015.
 */
public class TripCardInfoFragment extends BaseFragment {
    private CustomViewPager mViewPager;
    private SlidingTabLayout mSlidingTabLayout;
    private Toolbar mToolbar;
    private TextView lblTripTitle, lblTripCreator;
    private ImageView imgTripCardDetails;

    @Override
    protected void getAllArguments() {

    }

    @Override
    protected void initializeUI(View rootView) {
        mToolbar = (Toolbar) rootView.findViewById(R.id.tool_bar);

        mViewPager = (CustomViewPager) rootView.findViewById(R.id.tripInfoViewPager);
        mViewPager.setAdapter(new TripCardInfoPagerAdapter(getActivity(), getActivity().getSupportFragmentManager()));

        lblTripTitle = (TextView) rootView.findViewById(R.id.lbl_trip_title);
        lblTripCreator = (TextView) rootView.findViewById(R.id.lbl_trip_creator);

        mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.color_primary_dark));
        imgTripCardDetails = (ImageView) rootView.findViewById(R.id.img_trip_card_detail);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (((TripCardActivity) getActivity()).getCardResponseModel() != null) {
            Glide.with(this)
                    .load(((TripCardActivity) getActivity()).getTripCardDetails().getImageUrl())
                    .centerCrop()
                    .crossFade()
                    .into(imgTripCardDetails);
            lblTripTitle.setText(((TripCardActivity) getActivity()).getTripCardDetails().getTripName());
            lblTripCreator.setText("@" + ((TripCardActivity) getActivity()).getTripCardDetails().getFirstname());
        }
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_trip_card_info;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }


}
