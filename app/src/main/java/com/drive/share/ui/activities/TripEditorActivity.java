package com.drive.share.ui.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.drive.share.adapters.TripEditorAdapter;
import com.drive.share.data.webapi.TripModel;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.base.BaseActivity;

public class TripEditorActivity extends BaseActivity {


    ViewPager pagerEditors=null;
    TripEditorAdapter adapter=null;

   private TripModel mTripModel=null;

    public ViewPager getPagerEditors() {
        return pagerEditors;
    }

    public TripModel getTripModel() {
        return mTripModel;
    }

    public void setTripModel(TripModel mTripModel) {
        this.mTripModel = mTripModel;
    }

    int mStep = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void releaseUi() {

    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_trip_editor);
    }

    @Override
    protected void initializeUi() {

        //pagerEditors = (ViewPager) findViewById(R.id.frameContainer);

        adapter = new TripEditorAdapter(getSupportFragmentManager());
       // pagerEditors.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cerate_trip, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            updateProgress();
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }


    private void updateProgress() {
        if (pagerEditors.getCurrentItem() < 3) {

            switch ((pagerEditors.getCurrentItem() + 1)) {

                /*case 1:

                    findViewById(R.id.step1).setVisibility(View.VISIBLE);

                    break;
                case 2:
                    findViewById(R.id.step2).setVisibility(View.VISIBLE);
                    break;
                case 3:
                    findViewById(R.id.step3).setVisibility(View.VISIBLE);
                    break;*/


            }
        }

        pagerEditors.setCurrentItem(pagerEditors.getCurrentItem() + 1);
    }

}
