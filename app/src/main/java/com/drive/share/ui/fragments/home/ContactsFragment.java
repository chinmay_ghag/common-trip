package com.drive.share.ui.fragments.home;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.utilities.datareader.DataReader;
import com.drive.share.adapters.HomeContactsAdapter;
import com.drive.share.businesslayer.UserLogic;
import com.drive.share.data.webapi.BaseResponseModel;
import com.drive.share.data.webapi.UserModel;
import com.drive.share.globals.Constants;
import com.drive.share.globals.utils.Commons;
import com.drive.share.loaders.ContactsLoader;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.thirdparty.ThirdPartyActivity;
import com.drive.share.ui.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by webwerks on 2/2/15.
 */
public class ContactsFragment extends BaseFragment implements DataReader.DataClient {

    private ListView lstContacts;
    private HomeContactsAdapter adapter;
    private List<UserModel> mContactList;


    @Override
    protected void getAllArguments() {

    }

    @Override
    protected void initializeUI(View rootView) {
        lstContacts = (ListView) rootView.findViewById(R.id.lstContacts);
        lstContacts.setEmptyView(rootView.findViewById(android.R.id.empty));
        mContactList = new ArrayList<UserModel>();
        adapter = new HomeContactsAdapter(getActivity(),
                mContactList, this);
        lstContacts.setAdapter(adapter);
        lstContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startThirdParty(mContactList.get(position).getUserId());
            }
        });


        // load contacts info.
        new ContactsLoader(getActivity(), this).loadLocal();


    }

    private void startThirdParty(String userId) {
        Intent intent = new Intent(getActivity(), ThirdPartyActivity.class);
        intent.putExtra(ThirdPartyActivity.FLAG_EXTRA, userId);
        startActivity(intent);
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_contacts;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        switch (readRequestCode) {

            case UserLogic.BLOCK_USER:
                if (null != data) {
                    Commons.toastShort(getActivity(), ((BaseResponseModel) data).getMessage());
                }
                break;

            case Constants.REQ_LOAD_CONTACTS:

                if (null != data) {

                    Log.e("", "ListView::" + data);
                    mContactList.clear();
                    mContactList.addAll((ArrayList<UserModel>) data);
                    adapter.notifyDataSetChanged();

                }
                break;
        }
    }

    @Override
    public void onDataEmpty(int readRequestCode) {

    }

    /**
     * On error.
     *
     * @param readRequestCode the read request code
     * @param error
     */
    @Override
    public void onError(int readRequestCode, Object error) {

    }


    @Override
    public boolean needResponse() {
        return isAdded() && isVisible();
    }


}
