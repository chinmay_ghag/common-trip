package com.drive.share.ui.fragments.auth;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.utilities.Validation;
import com.android.utilities.datareader.DataReader;
import com.drive.share.businesslayer.UpdateProfileLogic;
import com.drive.share.businesslayer.VehicleLogic;
import com.drive.share.data.webapi.VehicleModel;
import com.drive.share.data.webapi.VehicleRegistrationResponse;
import com.drive.share.globals.utils.BitmapUtils;
import com.drive.share.globals.utils.Commons;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.HomeActivity;
import com.drive.share.ui.base.BaseFragment;

import java.util.List;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * interface
 * to handle interaction events.
 * Use the {@link com.drive.share.ui.fragments.auth.UserVehicleDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserVehicleDetailFragment extends BaseFragment implements View.OnClickListener, DataReader.DataClient, RadioGroup.OnCheckedChangeListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private int mType = 1;
    private View mrrotView;
    private Bitmap bmpVehicleImage;
    private VehicleModel vehicle = null;
    private Button btnDone;

    public final static String COMING_FROM = "comingFrom";
    public final static int COMING_FROM_LOGIN = 102;
    public final static int COMING_FROM_HOME = 103;
    private int mComingFrom = COMING_FROM_HOME;
    private ProgressDialog mDialog;

    //  private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TripInfoFragment.
     */
    public static UserVehicleDetailFragment newInstance(Bundle args) {
        UserVehicleDetailFragment fragment = new UserVehicleDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public UserVehicleDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    protected void getAllArguments() {
        mComingFrom = getArguments().getInt(COMING_FROM);
    }

    @Override
    protected void initializeUI(View rootView) {

        if (getActivity().findViewById(R.id.step2) != null)
            getActivity().findViewById(R.id.step2).setVisibility(View.VISIBLE);


        mrrotView = rootView;
        btnDone = (Button) rootView.findViewById(R.id.btnUploadPic);
        btnDone.setOnClickListener(this);

        if (mComingFrom == COMING_FROM_HOME) {
            btnDone.setText(getString(R.string.btn_save));
        }


        rootView.findViewById(R.id.imgVehiclePic).setOnClickListener(this);
        ((RadioGroup) rootView.findViewById(R.id.rgType)).setOnCheckedChangeListener(this);
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_vehicle_detail;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
       /* if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }*/
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        hideDialog();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //  mListener = null;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnUploadPic:
                // getActivity().startActivity(new Intent(getActivity(), HomeActivity.class));
                // getActivity().finish();
                //  VehicleModel vehicle = null;
                showDialog();
                if ((vehicle = VehicleLogic.validateVehicleData(mrrotView, this)) != null) {

                    // setting base 64 encoded bitmap image
                    if (bmpVehicleImage != null) {
                        vehicle.setImage(BitmapUtils.encodeTobase64(bmpVehicleImage));
                    }

                    vehicle.setVehicle_type(mType);

                    if (Validation.Network.isConnected(getActivity())) {
                        //showDialog();
                        // register vehicle data.
                        VehicleLogic.registerVehicle(this, vehicle);
                    } else {
                        Commons.errorNoInternet(getActivity());
                    }

                }


                break;
            case R.id.imgVehiclePic:

                UpdateProfileLogic.showPictureSelector(getActivity(), this);

                break;
        }
    }

    private void showDialog() {
        mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage("Please wait while we upload you vehicle details");
        mDialog.setIndeterminate(true);
        mDialog.show();
    }

    private void hideDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.hide();
        }
    }

    /**
     * On data received.
     *
     * @param readRequestCode the read request code
     * @param data            the data
     */
    @Override
    public void onDataReceived(int readRequestCode, Object data) {

        hideDialog();


        if (data != null) {
            VehicleRegistrationResponse response = (VehicleRegistrationResponse) data;

            if (response.getStatus()) {
                //PreferenceUtils.insertObject(PreferenceUtils.KEY_VEHICLE_ID,response.getVehicle_id());

                Toast.makeText(getActivity(), getString(R.string.success_saving_vehicle), Toast.LENGTH_LONG).show();
                //vehicle.setVehicleId(response.getVehicle().getVehicleId());
                VehicleLogic.storeVehicleLocal(response.getVehicle());
                // getActivity().getSupportFragmentManager().popBackStack();

                getActivity().finish();
            } else {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(getActivity(), getString(R.string.error_saving_vehicle), Toast.LENGTH_SHORT).show();
        }

        if (mComingFrom == COMING_FROM_LOGIN) {
            getActivity().startActivity(new Intent(getActivity(), HomeActivity.class));
            getActivity().finish();
        }

    }

    /**
     * On data empty.
     *
     * @param readRequestCode the read request code
     */
    @Override
    public void onDataEmpty(int readRequestCode) {
        hideDialog();
    }

    /**
     * On error.
     *
     * @param readRequestCode the read request code
     * @param error
     */
    @Override
    public void onError(int readRequestCode, Object error) {

        hideDialog();
        if (error instanceof List) {
            // show errrors
            for (Validation.UI.FieldError errorItem : (List<Validation.UI.FieldError>) error) {
                ((EditText) mrrotView.findViewById(errorItem.getFieldResourceId())).setError(errorItem.getErrorMessage());
            }
        } else {
            Toast.makeText(getActivity(), getString(R.string.error_saving_vehicle), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (resultCode) {
            case Activity.RESULT_OK:
                bmpVehicleImage = UpdateProfileLogic.getCapturedImage(this, data, requestCode);
                ((ImageView) mrrotView.findViewById(R.id.imgVehiclePic)).setImageBitmap(bmpVehicleImage);
                break;

        }


    }

    @Override
    public boolean needResponse() {
        return isAdded() && isVisible();
    }

    /**
     * <p>Called when the checked radio button has changed. When the
     * selection is cleared, checkedId is -1.</p>
     *
     * @param group     the group in which the checked radio button has changed
     * @param checkedId the unique identifier of the newly checked radio button
     */
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio2Wheel:
                mType = 1;
                break;
            case R.id.radio4Wheel:
                mType = 2;
                break;

        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
   /* public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }*/

}
