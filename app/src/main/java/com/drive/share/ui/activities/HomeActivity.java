package com.drive.share.ui.activities;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.drive.share.adapters.HomePagerAdapter;
import com.drive.share.globals.utils.NotificationHelper;
import com.drive.share.layoutmanagers.NavigationDrawerManager;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.base.BaseActivity;
import com.drive.share.ui.customviews.SlidingTabLayout;


public class HomeActivity extends BaseActivity {

    private final static int TAB_VIEW_TEXT_SIZE_SP = 16;
    private ViewPager mViewPager;
    private SlidingTabLayout mSlidingTabLayout;
    private Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void releaseUi() {

    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_home);
    }

    @Override
    protected void initializeUi() {

        mToolbar = (Toolbar) findViewById(R.id.tool_bar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPager.setAdapter(new HomePagerAdapter(this, getSupportFragmentManager()));

        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.color_primary_dark));

        NavigationDrawerManager.getInstance().initializeDrawer(this, mDrawerToggle, mToolbar);

        initiateGcmRegistration();


    }

    @Override
    protected void onResume() {
        super.onResume();
        NavigationDrawerManager.getInstance().loadUserData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            // start add trip activity
            startActivity(new Intent(this, CreateTripActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * registers device to receive Gcm notification
     */
    private void initiateGcmRegistration() {

        NotificationHelper.initGCM(this);

        /*new Thread() {
            public void run() {
                if (GcmHelper.checkPlayServices(HomeActivity.this)) {

                    String regid = GcmHelper.getRegistrationId(HomeActivity.this);

                    if (regid.isEmpty()) {
                        // registerInBackground();
                        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(HomeActivity.this);
                        try {
                            regid = gcm.register(GcmHelper.SENDER_ID);

                            GcmHelper.setRegistrationId(regid);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        DeviceDetailLogic.sendDeviceDetails(App.getInstance(), regid);


                    } else {
                        DeviceDetailLogic.sendDeviceDetails(App.getInstance(), regid);
                    }
                    Log.i("HB GCM:", regid + " GCM ID.");
                } else {
                    Log.i(getClass().getSimpleName(), "No valid Google Play Services APK found.");
                }
            }
        }.start();*/
    }
}
