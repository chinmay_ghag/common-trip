package com.drive.share.ui.activities;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.drive.share.adapters.HomePagerAdapter;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.base.BaseActivity;


public class TripDetailActivity extends BaseActivity {

    ViewPager pagerScreens;

    @Override
    protected void releaseUi() {

    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_trip_detail);
    }

    @Override
    protected void initializeUi() {

        pagerScreens = (ViewPager) findViewById(R.id.pagerFragments);
        pagerScreens.setAdapter(new HomePagerAdapter(this, getSupportFragmentManager()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            // start add trip activity
            startActivity(new Intent(this, CreateTripActivity.class));


            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
