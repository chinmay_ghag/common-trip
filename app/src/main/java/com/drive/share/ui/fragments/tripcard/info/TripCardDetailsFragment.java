package com.drive.share.ui.fragments.tripcard.info;

import android.content.Intent;
import android.content.res.Resources;
import android.location.Location;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.utilities.datareader.DataReader;
import com.drive.share.businesslayer.TripCardLogic;
import com.drive.share.businesslayer.UserRequestLogic;
import com.drive.share.data.webapi.TripCardDetails;
import com.drive.share.data.webapi.TripCardStatus;
import com.drive.share.globals.App;
import com.drive.share.globals.utils.Commons;
import com.drive.share.globals.utils.LocationHelper;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.JoinTripActivity;
import com.drive.share.ui.activities.tripcard.TripCardActivity;
import com.drive.share.ui.base.BaseFragment;

/**
 * Created by Pervacio-Dev on 3/4/2015.
 */
public class TripCardDetailsFragment extends BaseFragment implements View.OnClickListener, DataReader.DataClient {
    private TextView lblTotalDuration, lblDistRemain, lblTotalDistance, lblStartTime, lblEndTime, lblStatSumm, lblStartDate, lblEndDate;
    private Button btnDriver, btnUser;
    private boolean isTripStarted, isTripJoin;
    private Location mCurrentLocation;
    private TripCardDetails mTripCardDetails;
    @Override
    protected void getAllArguments() {

    }

    @Override
    protected void initializeUI(View rootView) {

        LocationHelper helper = new LocationHelper(getActivity());
        helper.fetchLocation(15*1000,new LocationHelper.LocationResponse() {
            @Override
            public void onLocationAquired(Location l) {
                mCurrentLocation = l;
            }
        });

        mTripCardDetails = ((TripCardActivity) getActivity()).getTripCardDetails();

        lblTotalDuration = (TextView) rootView.findViewById(R.id.lbl_total_duration);
        lblDistRemain = (TextView) rootView.findViewById(R.id.lbl_dist_remain);
        lblEndTime = (TextView) rootView.findViewById(R.id.lbl_end_time);
        lblStatSumm = (TextView) rootView.findViewById(R.id.lbl_start_time);
        lblTotalDistance = (TextView) rootView.findViewById(R.id.lbl_total_dist);
        lblStartTime = (TextView) rootView.findViewById(R.id.lbl_start_time);
        lblEndDate = (TextView) rootView.findViewById(R.id.lbl_end_date);
        lblStartDate = (TextView) rootView.findViewById(R.id.lbl_start_date);
        btnDriver = (Button) rootView.findViewById(R.id.btnTripCardAction);
        btnUser = (Button) rootView.findViewById(R.id.btnTripCardInvite);
        btnDriver.setOnClickListener(this);
        btnUser.setOnClickListener(this);

        if (((TripCardActivity) getActivity()).getCardResponseModel() != null) {
            setDefaultData(mTripCardDetails.getOriginName(), mTripCardDetails.getDestinationName(), mTripCardDetails.getEndTime(), mTripCardDetails.getSeatsAvail(), mTripCardDetails.getDistance(), mTripCardDetails.getStartTime(), mTripCardDetails.getEndDate(), mTripCardDetails.getEndDate());
        }

        if ("driver".equalsIgnoreCase(((TripCardActivity) getActivity()).getCardResponseModel().getTrip_user_relation())) {
            btnDriver.setVisibility(View.VISIBLE);
            final TripCardStatus tripCardStatus = ((TripCardActivity) getActivity()).getmTripCardStatus();
            if(tripCardStatus != null && "false".equalsIgnoreCase("" + tripCardStatus.getIsTripStarted())){
                isTripStarted = false;
                btnDriver.setText("Start");
            }else if(tripCardStatus != null && "true".equalsIgnoreCase("" + tripCardStatus.getIsTripStarted())){
                isTripStarted = true;
                btnDriver.setText("Stop");
            }else{
                Toast.makeText(getActivity(),"No response.",Toast.LENGTH_SHORT ).show();
            }
        } else if ("passenger".equalsIgnoreCase(((TripCardActivity) getActivity()).getCardResponseModel().getTrip_user_relation())) {
            btnUser.setVisibility(View.VISIBLE);
            final TripCardStatus tripCardStatus = ((TripCardActivity) getActivity()).getmTripCardStatus();
            if(tripCardStatus != null && "stopped ".equalsIgnoreCase("" + tripCardStatus.getIsTripJoined())){
                btnUser.setText("Stopped");
                btnUser.setActivated(false);
            }else if(tripCardStatus != null && "joined ".equalsIgnoreCase(""+tripCardStatus.getIsTripJoined())){
                btnUser.setText("Leave");
                isTripJoin = true;
            }else if(tripCardStatus != null && "not_joined ".equalsIgnoreCase(""+tripCardStatus.getIsTripJoined())){
                btnUser.setText("Join");
                isTripJoin = false;
            }else{
                Toast.makeText(getActivity(),"No response.",Toast.LENGTH_SHORT ).show();
            }
        }else{
            Toast.makeText(getActivity(),"No response.",Toast.LENGTH_SHORT ).show();
        }
    }

    private void setDefaultData(String totalDuration, String distRemain, String endTime, String statSum, String totalDist, String startTime, String startDate, String endDate) {
        Resources res = getResources();
        lblTotalDuration.setText(String.format(res.getString(R.string.lbl_total_duration), totalDuration));
        lblDistRemain.setText(String.format(res.getString(R.string.lbl_dist_remain), distRemain));
        lblEndTime.setText(String.format(res.getString(R.string.lbl_end_time), endTime));
        lblStatSumm.setText(String.format(res.getString(R.string.lbl_stat_summ), statSum));
        lblTotalDistance.setText(String.format(res.getString(R.string.lbl_total_dist), totalDist));
        lblStartTime.setText(String.format(res.getString(R.string.lbl_start_time), startTime));
        lblEndDate.setText(String.format(res.getString(R.string.lbl_end_date), Commons.getFormattedDate(endDate)));
        lblStartDate.setText(String.format(res.getString(R.string.lbl_start_date), Commons.getFormattedDate(startDate)));
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_card_info_details;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTripCardAction:
                Log.d("patrol","LAT--> "+(mCurrentLocation!=null?Double.toString(mCurrentLocation.getLatitude()):"72.6"));
                if(!isTripStarted) {

                    //start
                    UserRequestLogic.sendTripAction(App.getInstance().getLoggedInUser().getUserId(), ((TripCardActivity) getActivity()).getmTripCardId(), "1",mTripCardDetails.getFirstname(),mCurrentLocation!=null?Double.toString(mCurrentLocation.getLatitude()):"72.6",mCurrentLocation!=null?Double.toString(mCurrentLocation.getLongitude()):"18.72",1, this);
                }else{
                    //stop
                    UserRequestLogic.sendTripAction(App.getInstance().getLoggedInUser().getUserId(), ((TripCardActivity) getActivity()).getmTripCardId(), "3",mTripCardDetails.getFirstname(),mCurrentLocation!=null?Double.toString(mCurrentLocation.getLatitude()):"72.6",mCurrentLocation!=null?Double.toString(mCurrentLocation.getLongitude()):"18.72",1, this);
                }
                break;
            case R.id.btnTripCardInvite:
                /*if (((TripCardActivity) getActivity()).getCardResponseModel() != null) {
                    Intent intent = new Intent(getActivity(), JoinTripActivity.class);
                    intent.putExtra(JoinTripActivity.EXTRA_DRIVER_ID, mTripCardDetails.getDriver_id());
                    intent.putExtra(JoinTripActivity.EXTRA_TRIP_ID, ((TripCardActivity) getActivity()).getmTripCardId());
                    getActivity().startActivity(intent);
                }*/
                if(isTripJoin){
                    TripCardLogic.leaveTrip(((TripCardActivity) getActivity()).getmTripCardId(), this);
                }else{
                    Intent intent = new Intent(getActivity(), JoinTripActivity.class);
                    intent.putExtra(JoinTripActivity.EXTRA_DRIVER_ID, mTripCardDetails.getDriver_id());
                    intent.putExtra(JoinTripActivity.EXTRA_TRIP_ID, ((TripCardActivity) getActivity()).getmTripCardId());
                    getActivity().startActivity(intent);
                }
                break;
        }
    }

    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        if(UserRequestLogic.TRIP_ACTION  == readRequestCode) {
            if(!isTripStarted){
              isTripStarted = true;
                Toast.makeText(getActivity(), "Trip started successfully.", Toast.LENGTH_SHORT).show();
                btnDriver.setText("Stop");
            }else {
              isTripStarted = false;
                Toast.makeText(getActivity(), "Trip stopped successfully.", Toast.LENGTH_SHORT).show();
                btnDriver.setText("Start");
            }
        }else if(TripCardLogic.LEAVE_TRIP  == readRequestCode){
            if(!isTripJoin){
                Toast.makeText(getActivity(), "Trip joined successfully.", Toast.LENGTH_SHORT).show();
                isTripJoin = true;
                btnDriver.setText("Leave");
            }else{
                isTripJoin = false;
                Toast.makeText(getActivity(), "Trip leaved successfully.", Toast.LENGTH_SHORT).show();
                btnDriver.setText("Join");
            }
        }
    }

    @Override
    public void onDataEmpty(int readRequestCode) {

    }

    @Override
    public void onError(int readRequestCode, Object error) {
        Toast.makeText(getActivity(), "Something went wrong.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean needResponse() {
        return true;
    }
}
