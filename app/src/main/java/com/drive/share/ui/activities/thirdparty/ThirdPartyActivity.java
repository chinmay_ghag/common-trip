package com.drive.share.ui.activities.thirdparty;

import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import com.drive.share.adapters.ThirdPartyPagerAdapter;
import com.drive.share.globals.utils.Commons;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.base.BaseActivity;
import com.drive.share.ui.customviews.SlidingTabLayout;

/**
 * Created by Pervacio-Dev on 3/4/2015.
 */
public class ThirdPartyActivity extends BaseActivity {
    private ViewPager mViewPager;
    private SlidingTabLayout mSlidingTabLayout;
    private Toolbar mToolbar;

    public static String FLAG_EXTRA = "extra";

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    private String user_id;


    @Override
    protected void releaseUi() {

    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_third_party);
    }

    @Override
    protected void initializeUi() {
        mToolbar = (Toolbar) findViewById(R.id.tool_bar);


        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        setUser_id(getIntent().getStringExtra(ThirdPartyActivity.FLAG_EXTRA));

        if (TextUtils.isEmpty(user_id)) {
            Commons.toastShort(this, "User id is empty");
            finish();
        }

        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPager.setAdapter(new ThirdPartyPagerAdapter(this, getSupportFragmentManager()));

        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.color_primary_dark));
    }
}
