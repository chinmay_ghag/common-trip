package com.drive.share.ui.fragments.tripcard;

public class ChatElement {
	public boolean left;
	public String comment;

	public ChatElement(boolean left, String comment) {
		super();
		this.left = left;
		this.comment = comment;
	}

}