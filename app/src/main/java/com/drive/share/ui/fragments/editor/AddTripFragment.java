package com.drive.share.ui.fragments.editor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.android.utilities.Validation;
import com.android.utilities.datareader.DataReader;
import com.drive.share.businesslayer.TripEdtiorLogic;
import com.drive.share.businesslayer.UpdateProfileLogic;
import com.drive.share.data.webapi.TripModel;
import com.drive.share.data.webapi.VehicleModel;
import com.drive.share.globals.App;
import com.drive.share.globals.Constants;
import com.drive.share.globals.utils.BitmapUtils;
import com.drive.share.globals.utils.Commons;
import com.drive.share.loaders.ContactsLoader;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.CreateTripActivity;
import com.drive.share.ui.activities.profile.VehiclRegistrationActivity;
import com.drive.share.ui.base.BaseActivity;
import com.drive.share.ui.base.BaseFragment;
import com.drive.share.ui.dialogs.TripNameDialog;
import com.drive.share.ui.dialogs.TripTypeDialog;

import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by atulOholic on 4/3/15.
 */
public class AddTripFragment extends BaseFragment implements View.OnClickListener, TripNameDialog.OnCreateTripNameListener, TripTypeDialog.OnCreateTripTypeListener, TimePickerDialog.OnTimeSetListener, DataReader.DataClient {

    private TextView txtTripName, txtTripType, txtTripSource, txtTripDestination, txtTripStartTime, txtTripEndTime, txtToday;
    private LinearLayout layoutWeek;
    private CircleImageView imgTripImage;
    private CheckBox chkIfReturnTrip, chkSunday, chkMonday, chkTuesday, chkWednesday, chkThursday, chkFriday, chkSaturday;
    private Button btnSubmit;
    private Bitmap picture = null;
    private RadioButton rdPickUp, rdDropOut, rdTwoWheeler, rdFourWheeler;
    private TimePickerDialog startTimePicker, endTimePicker;
    private List<VehicleModel> vehicles;

    private int mTripType = TripTypeDialog.FLAG_SINGLE;

    private boolean startDateTime = true;


    @Override
    protected void getAllArguments() {

    }

    @Override
    protected void initializeUI(View rootView) {

        ((BaseActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.title_activity_trip_editor));

        txtTripName = (TextView) rootView.findViewById(R.id.txtTripName);
        txtTripType = (TextView) rootView.findViewById(R.id.txtTripType);
        txtTripStartTime = (TextView) rootView.findViewById(R.id.txtTripStartTime);
        txtTripEndTime = (TextView) rootView.findViewById(R.id.txtTripEndTime);

        txtTripSource = (TextView) rootView.findViewById(R.id.txtTripSource);
        txtTripDestination = (TextView) rootView.findViewById(R.id.txtTripDestination);

        layoutWeek = (LinearLayout) rootView.findViewById(R.id.layoutWeek);
        txtToday = (TextView) rootView.findViewById(R.id.txtToday);

        chkIfReturnTrip = (CheckBox) rootView.findViewById(R.id.chkReturn);
        chkSunday = (CheckBox) rootView.findViewById(R.id.chkSunday);
        chkMonday = (CheckBox) rootView.findViewById(R.id.chkMonday);
        chkTuesday = (CheckBox) rootView.findViewById(R.id.chkTuesday);
        chkWednesday = (CheckBox) rootView.findViewById(R.id.chkWednesday);
        chkThursday = (CheckBox) rootView.findViewById(R.id.chkThursday);
        chkFriday = (CheckBox) rootView.findViewById(R.id.chkFriday);
        chkSaturday = (CheckBox) rootView.findViewById(R.id.chkSaturday);

        rdPickUp = (RadioButton) rootView.findViewById(R.id.rdPickUp);
        rdDropOut = (RadioButton) rootView.findViewById(R.id.rdDrop);

        rdTwoWheeler = (RadioButton) rootView.findViewById(R.id.rdTwoWheeler);
        rdFourWheeler = (RadioButton) rootView.findViewById(R.id.rdFourWheeler);


        onCreateTripType(mTripType);

        imgTripImage = (CircleImageView) rootView.findViewById(R.id.imgTripImage);

        btnSubmit = (Button) rootView.findViewById(R.id.btnSubmit);


        txtTripName.setOnClickListener(this);
        txtTripType.setOnClickListener(this);
        txtTripSource.setOnClickListener(this);
        txtTripDestination.setOnClickListener(this);
        txtTripEndTime.setOnClickListener(this);
        txtTripStartTime.setOnClickListener(this);
        imgTripImage.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();

        vehicles = TripEdtiorLogic.getLocalVhicles();

        if (vehicles.size() < 1) {
            new AlertDialog.Builder(getActivity()).setTitle(R.string.title_vehicle_alert).setPositiveButton(R.string.prompt_select, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    getActivity().startActivity(new Intent(getActivity(), VehiclRegistrationActivity.class));
                }
            }).setNegativeButton(R.string.prompt_no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    getActivity().finish();
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    dialog.dismiss();
                    getActivity().finish();
                }
            }).create().show();
        } else {
            setUpDataIfAvailable();
        }

    }

    private void setUpDataIfAvailable() {

        CreateTripActivity activity = (CreateTripActivity) getActivity();
        TripModel model = activity.getTripModel();

        if (!TextUtils.isEmpty(model.getOrigin_name())) {
            txtTripSource.setText(model.getOrigin_name());
        }

        if (!TextUtils.isEmpty(model.getDestination_name())) {
            txtTripDestination.setText(model.getDestination_name());
        }


    }

    private void setVehicleData(TripModel model) {

        int type = 0;

        if (rdTwoWheeler.isChecked()) {
            type = VehicleModel.TYPE_TWO_WHEELER;
        } else if (rdTwoWheeler.isChecked()) {
            type = VehicleModel.TYPE_FOUR_WHEELER;
        }

        int counter = 0;

        for (VehicleModel vehicle : vehicles) {

            Log.d("Vehicle", "" + counter++);

            if (vehicle.getVehicle_type() == type) {
                model.setVehicle_id(vehicle.getVehicleId());
                Log.d("Vehicle", "" + vehicle.getVehicleId());
                break;
            }
        }

    }

    private void setTripData() {
        CreateTripActivity activity = (CreateTripActivity) getActivity();
        TripModel model = activity.getTripModel();

        model.setTrip_name("" + txtTripName.getText().toString());
        model.setIs_pick_up("" + isTripPickUp());
        model.setUser_id(App.getInstance().getAppUserId());
        model.setCreate_departure_trip((chkIfReturnTrip.isChecked()) ? TripModel.IS_RETURN_TRIP :
                TripModel.IS_NOT_RETURN_TRIP);

        setVehicleData(model);


        if (mTripType == TripTypeDialog.FLAG_SINGLE) {
            Calendar calendar = Calendar.getInstance();
            model.setStart_date(calendar.get(Calendar.YEAR) + Commons.appendZero(calendar.get(Calendar.MONTH)) + Commons.appendZero(calendar.get(Calendar.DAY_OF_MONTH)));
            model.setTrip_frequency(TripModel.FREQ_NEVER);
        } else {
            model.setTrip_frequency(TripModel.FREQ_WEEKLY);
            model.setTrip_days(getRepeatDays());
        }

    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_create_trip;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgTripImage:

                //Toast.makeText(getActivity(), "IMage Clicked", Toast.LENGTH_SHORT).show();
                UpdateProfileLogic.showPictureSelector(getActivity(), this);
                break;

            case R.id.txtTripName:
                new TripNameDialog(getActivity(), this, txtTripName.getText().toString()).show();
                break;

            case R.id.txtTripType:
                new TripTypeDialog(getActivity(), this, mTripType).show();
                break;
            case R.id.txtTripStartTime:
                startDateTime = true;
                startTimePicker = new TimePickerDialog(getActivity(),
                        this, Commons.getCurrentHour(), Commons.getCurrentMinute(), true);
                startTimePicker.show();
                break;

            case R.id.txtTripEndTime:
                startDateTime = false;
                endTimePicker = new TimePickerDialog(getActivity(),
                        this, Commons.getCurrentHour(), Commons.getCurrentMinute(), true);
                endTimePicker.show();
                break;
            case R.id.txtTripSource:
                TripEdtiorLogic.showLocationSettings((BaseActivity) getActivity(), true);
                break;
            case R.id.txtTripDestination:
                TripEdtiorLogic.showLocationSettings((BaseActivity) getActivity(), false);
                break;
            case R.id.btnSubmit:

                if (validateTripData()) {

                    try {
                        setTripData();
                    } catch (Exception e) {
                        e.printStackTrace();
                        break;
                    }

                    if (Validation.Network.isConnected(getActivity())) {
                        TripEdtiorLogic.createTrip(getActivity(), this);
                    } else {
                        Commons.errorNoInternet(getActivity());
                    }
                }

                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {

            case Activity.RESULT_OK:
                if (requestCode == Constants.CAMERA_CAPTURE || requestCode == Constants.GALLERY_CAPTURE) {
                    picture = UpdateProfileLogic.getCapturedImage(this, data, requestCode);
                    if (picture != null) {

                        TripEdtiorLogic.getCreateTripActivity(getActivity()).getTripModel().setTripImage(BitmapUtils.encodeTobase64(picture));
                        imgTripImage.setImageBitmap(picture);
                    }
                }

                break;
            case Activity.RESULT_CANCELED:

                break;
        }
    }

    private String isTripPickUp() {
        if (rdPickUp.isChecked()) {
            return TripModel.IS_DROP_OUT;
        } else if (rdDropOut.isChecked()) {
            return TripModel.IS_PICK_UP;
        }

        return null;
    }


    @Override
    public void onCreateTripName(String tripName) {

        txtTripName.setText(tripName);
        TripEdtiorLogic.getCreateTripActivity(getActivity()).getTripModel().setTrip_name(tripName);
    }


    @Override
    public void onCreateTripType(int flag) {

        mTripType = flag;

        if (flag == TripTypeDialog.FLAG_SINGLE) {
            txtTripType.setText(getString(R.string.hint_trip_type_single));
            showWeeklyView(false);

        } else if (flag == TripTypeDialog.FLAG_WEEKLY) {
            txtTripType.setText(getString(R.string.hint_trip_type_weekly));
            showWeeklyView(true);
        }
    }

    private void showWeeklyView(boolean show) {
        if (show) {
            txtToday.setVisibility(View.GONE);
            layoutWeek.setVisibility(View.VISIBLE);
        } else {
            txtToday.setVisibility(View.VISIBLE);
            layoutWeek.setVisibility(View.GONE);
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if (startDateTime) {
            TripEdtiorLogic.getCreateTripActivity(getActivity()).getTripModel().setStart_time(hourOfDay + "" + minute);
            txtTripStartTime.setText(Commons.getFormattedTime(hourOfDay, minute));
        } else {
            TripEdtiorLogic.getCreateTripActivity(getActivity()).getTripModel().setEnd_time(hourOfDay + "" + minute);
            txtTripEndTime.setText(Commons.getFormattedTime(hourOfDay, minute));
        }
    }

    private boolean validateTripData() {


        CreateTripActivity activity = (CreateTripActivity) getActivity();
        TripModel model = activity.getTripModel();

        if (model.getDestination_latitude() == null) {
            Commons.toastShort(getActivity(), getString(R.string.error_trip_source));
            return false;
        }

        if (model.getDestination_longitude() == null) {
            Commons.toastShort(getActivity(), getString(R.string.error_trip_destination));
            return false;
        }

        return true;


    }


    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        if (readRequestCode == TripEdtiorLogic.CREATE_TRIP) {

            if (((TripModel) data).getStatus()) {
                Commons.toastShort(getActivity(), getString(R.string.success_saving_trip));
                TripEdtiorLogic.storeTripLocal(((TripModel) data));
                if (ContactsLoader.areRegisteredFriendAvailable()) {
                    ((CreateTripActivity) getActivity()).inviteFriends("" + ((TripModel) data).getTripData().getTripCardId());
                } else {
                    getActivity().finish();
                }
            } else {
                Commons.toastShort(getActivity(), getString(R.string.error_creating_trip));
            }


        }
    }

    @Override
    public void onDataEmpty(int readRequestCode) {

    }

    @Override
    public void onError(int readRequestCode, Object error) {

    }

    @Override
    public boolean needResponse() {
        return isAdded() && isVisible();
    }

    private String getRepeatDays() {
        String days = "";

        if (chkSunday.isChecked()) {
            days += "sunday,";
        } else if (chkMonday.isChecked()) {
            days += "monday,";
        } else if (chkTuesday.isChecked()) {
            days += "tuesday,";
        } else if (chkWednesday.isChecked()) {
            days += "wednesday,";
        } else if (chkThursday.isChecked()) {
            days += "thursday,";
        } else if (chkFriday.isChecked()) {
            days += "friday,";
        } else if (chkSaturday.isChecked()) {
            days += "saturday,";
        }

        if (days.length() > 0)
            days = days.substring(0, days.length() - 1);

        return days;
    }
}
