package com.drive.share.ui.fragments.editor;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.utilities.Validation;
import com.android.utilities.datareader.DataReader;
import com.drive.share.businesslayer.TripEdtiorLogic;
import com.drive.share.globals.Constants;
import com.drive.share.globals.utils.Commons;
import com.drive.share.globals.utils.GoogleMapUtils;
import com.drive.share.loaders.GeoLoacationLoader;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.JoinTripActivity;
import com.drive.share.ui.base.BaseActivity;
import com.drive.share.ui.base.BaseFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by yashesh on 31/1/15.
 */
public class TripMapFragment extends BaseFragment implements OnMapReadyCallback, TextWatcher, DataReader.DataClient, GoogleMap.OnCameraChangeListener, View.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {

    private GoogleMap googleMap = null;
    private AutoCompleteTextView txtSource = null, txtDestination = null;
    private MapView mapView = null;
    private GeoLoacationLoader mLocationLoader;
    private final int LOAD_SOURCE_ADDRESSES = 12, LOAD_DESTINATION_ADDRESSES = 45;
    private Geocoder geoCoder;
    private boolean selectedFromLoacation = false;
    private double startLat, startLong, destLat, destLong;
    private ArrayAdapter<Address> sourceAdapter, destAdapter;
    private boolean isSourceAddress = true;
    private View rootView = null;
    private Button btnSave;

    public final static String COMING_FROM = "coming_from";
    public final static int COMING_FROM_JOIN = 500;
    public final static int COMING_FROM_CREATE = 600;


    @Override
    protected void getAllArguments() {

        isSourceAddress = getArguments().getBoolean(Constants.KEY_IS_SOURCE_ADDRESS);
        geoCoder = new Geocoder(getActivity());


        if (isSourceAddress) {
            ((BaseActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.hint_source));
        } else {
            ((BaseActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.hint_destination));
        }


    }

    @Override
    protected void initializeUI(View rootView) {
        this.rootView = rootView;
        txtSource = (AutoCompleteTextView) rootView.findViewById(R.id.txtTripStart);
        //txtDestination = (AutoCompleteTextView) rootView.findViewById(R.id.txtTripDestination);
        btnSave = (Button) rootView.findViewById(R.id.btnNext);
        btnSave.setOnClickListener(this);

        if (getArguments().getInt(COMING_FROM) == COMING_FROM_CREATE) {

            if (!isSourceAddress) {
                txtSource.setHint(R.string.prompt_trip_destination);
            } /*else {
            ((TextView) rootView.findViewById(R.id.btnNext)).setText(R.string.action_next);
        }*/
        } else {
            btnSave.setText(getString(R.string.hint_join));
        }

        //txtSource.addTextChangedListener(this);
        //  txtDestination.addTextChangedListener(this);


        // txtDestination.setOnItemClickListener(this);


        //txtSource.setOnItemSelectedListener(this);


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initMap(rootView);
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_trip_map;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    private void initMap(View rootView) {
        // GMS package method for initializing the map.
        MapsInitializer.initialize(getActivity());
        // get the map view .
        mapView = (MapView) rootView.findViewById(R.id.mapView);
        // call lifecycle method of map view to create.
        mapView.onCreate(null);
        // get map object asynchronously callback is implemented in this screen.
        mapView.getMapAsync(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMyLocationEnabled(true);
        centerMapOnMyLocation(this.googleMap);
        //this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);

        this.googleMap.setOnCameraChangeListener(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }


    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }


    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //  if (!isCameraMoved) {
        if (Validation.Network.isConnected(getActivity()) && s.toString().length() > 2) {
            if (mLocationLoader == null) {
                mLocationLoader = new GeoLoacationLoader(getActivity(), this);
            }
            if (txtSource.hasFocus()) {

                mLocationLoader.read(LOAD_SOURCE_ADDRESSES, s.toString());

            }/* else if (txtDestination.hasFocus()) {
                mLocationLoader.read(LOAD_DESTINATION_ADDRESSES, s.toString());
            }*/
        }
       /* } else {

            isCameraMoved = false;
        }*/

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        switch (readRequestCode) {

            case LOAD_SOURCE_ADDRESSES:
                // populate source textview

                /*sourceAdapter.clear();
                sourceAdapter.notifyDataSetChanged();*/

                sourceAdapter = new ArrayAdapter<Address>(getActivity(), android.R.layout.simple_list_item_1, (List<Address>) data) {

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        return getDropDownView(position, convertView, parent);
                    }


                    @Override
                    public View getDropDownView(int position, View convertView, ViewGroup parent) {

                        if (convertView == null) {
                            convertView = LayoutInflater.from(getActivity()).inflate(android.R.layout.simple_list_item_1, null);
                        }
                        ((TextView) convertView.findViewById(android.R.id.text1)).setText(getItem(position).getAddressLine(0) + ", " + getItem(position).getLocality() + ", " + getItem(position).getCountryName());
                        convertView.setTag(getItem(position));
                        convertView.setOnClickListener(TripMapFragment.this);
                        return convertView;
                    }
                };
                txtSource.setAdapter(sourceAdapter);

                txtSource.showDropDown();

                break;
            case LOAD_DESTINATION_ADDRESSES:
                //populate destination textview
                destAdapter = new ArrayAdapter<Address>(getActivity(), android.R.layout.simple_list_item_1, (List<Address>) data) {

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        return getDropDownView(position, convertView, parent);
                    }


                    @Override
                    public View getDropDownView(int position, View convertView, ViewGroup parent) {

                        if (convertView == null) {
                            convertView = LayoutInflater.from(getActivity()).inflate(android.R.layout.simple_list_item_1, null);
                        }
                        ((TextView) convertView.findViewById(android.R.id.text1)).setText(getItem(position).getAddressLine(0) + ", " + getItem(position).getLocality() + ", " + getItem(position).getCountryName());
                        convertView.setTag(getItem(position));
                        convertView.setOnClickListener(TripMapFragment.this);

                        return convertView;
                    }
                };
                //    txtDestination.setAdapter(destAdapter);

                //    txtDestination.showDropDown();


                //   txtDestination.showDropDown();
                break;

            case TripEdtiorLogic.GET_ADDRESS:

                //if (txtSource.hasFocus()) {
                txtSource.setText(data + "");
                /*} else if (txtDestination.hasFocus()) {
                    txtDestination.setText(data + "");
                }*/


                break;

            case TripEdtiorLogic.CREATE_TRIP:
                Toast.makeText(getActivity(), data + "", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onDataEmpty(int readRequestCode) {

    }

    /**
     * On error.
     *
     * @param readRequestCode the read request code
     * @param error
     */
    @Override
    public void onError(int readRequestCode, Object error) {
        // Toast.makeText(getActivity(), "error", Toast.LENGTH_LONG).show();
        Log.d("onError", "Error");
    }


    @Override
    public boolean needResponse() {
        return (isAdded() && isVisible());
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        // isCameraMoved = true;
        //if (!selectedFromLoacation) {
        double[] center = GoogleMapUtils.getLocationBounds(googleMap);


        startLat = center[4];
        startLong = center[5];
        TripEdtiorLogic.getLocationName(startLat, startLong, geoCoder, this);



        /*if (txtSource.hasFocus()) {
            startLat = center[4];
            startLong = center[5];
            TripEdtiorLogic.getLocationName(startLat, startLong, geoCoder, this);

        } else {
            destLat = center[4];
            destLong = center[5];
            TripEdtiorLogic.getLocationName(destLat, destLong, geoCoder, this);
        }
        selectedFromLoacation = true;*/
        //}
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnNext:

                try {

                    if (getArguments().getInt(COMING_FROM) == COMING_FROM_CREATE) {
                        TripEdtiorLogic.setLocation(getActivity(), startLat, startLong, txtSource.getText().toString().replace("\n", " "), isSourceAddress);
                        TripEdtiorLogic.getCreateTripActivity(getActivity()).onBackPressed();
                    } else {
                        OnFetchLocationListener listener = (JoinTripActivity) getActivity();
                        listener.setFetchedLocation(txtSource.getText().toString().replace("\n", " "), "" + startLat, "" + startLong);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


                //TripEditorActivity editorActivity = ((TripEditorActivity) getActivity());
                //TripEdtiorLogic.createTrip(editorActivity.getTripModel(), this);


                /*if (isSourceAddress) {
                    isSourceAddress = false;
                    googleMap.clear();
                    txtSource.getText().clear();
                    txtSource.setHint(R.string.prompt_trip_destination);
                    ((TextView) rootView.findViewById(R.id.btnNext)).setText(R.string.action_add);
                }*/
                break;
            default:
                selectedFromLoacation = true;
                Address address = (Address) v.getTag();
                if (txtSource.hasFocus()) {

                    txtSource.setText(address.getAddressLine(0) + ", " + address.getLocality());


                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(address.getLatitude(), address.getLongitude()), 14.0f));


                } /*else if (txtDestination.hasFocus()) {
                    txtDestination.setText(address.getAddressLine(0) + ", " + address.getLocality());
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(address.getLatitude(), address.getLongitude()), 14.0f));

                }*/

                break;

        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (txtSource.hasFocus()) {

            txtSource.setText(sourceAdapter.getItem(position).getAddressLine(0) + ", " + sourceAdapter.getItem(position).getLocality());


            googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(sourceAdapter.getItem(position).getLatitude(), sourceAdapter.getItem(position).getLongitude())));


        } /*else if (txtDestination.hasFocus()) {


            txtDestination.setText(destAdapter.getItem(position).getAddressLine(0) + ", " + sourceAdapter.getItem(position).getLocality());
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(destAdapter.getItem(position).getLatitude(), destAdapter.getItem(position).getLongitude())));


        }*/


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


        if (txtSource.hasFocus()) {

            txtSource.setText(sourceAdapter.getItem(position).getAddressLine(0) + ", " + sourceAdapter.getItem(position).getLocality());
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(sourceAdapter.getItem(position).getLatitude(), sourceAdapter.getItem(position).getLongitude())));
            Commons.hideKeyBoard(getActivity(), txtSource);


        } /*else if (txtDestination.hasFocus()) {
            txtDestination.setText(destAdapter.getItem(position).getAddressLine(0) + ", " + sourceAdapter.getItem(position).getLocality());
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(destAdapter.getItem(position).getLatitude(), destAdapter.getItem(position).getLongitude())));
            Commons.hideKeyBoard(getActivity(), txtDestination);

        }*/


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void centerMapOnMyLocation(GoogleMap map) {

        map.setMyLocationEnabled(true);

        Location location = map.getMyLocation();
        LatLng myLocation = null;

        if (location != null) {
            myLocation = new LatLng(location.getLatitude(),
                    location.getLongitude());
        }
        if (myLocation != null) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation,
                    14));
        }
    }

    public static interface OnFetchLocationListener {
        public void setFetchedLocation(String placeName, String latitude, String longitude);
    }
}
