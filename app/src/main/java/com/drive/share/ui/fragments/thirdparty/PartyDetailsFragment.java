package com.drive.share.ui.fragments.thirdparty;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.android.utilities.Validation;
import com.android.utilities.datareader.DataReader;
import com.drive.share.businesslayer.ThirdPartyLogic;
import com.drive.share.data.webapi.ThirdPartyDetailResponseModel;
import com.drive.share.data.webapi.ThirdPartyDetailsModel;
import com.drive.share.globals.Constants;
import com.drive.share.globals.utils.Commons;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.thirdparty.ThirdPartyActivity;
import com.drive.share.ui.base.BaseFragment;

/**
 * Created by webwerks on 2/2/15.
 */
public class PartyDetailsFragment extends BaseFragment implements DataReader.DataClient {

    private String mUserID;
    private TextView txtTwoWheelerCount, txtFourWheelerCount, txtTripCount;

    @Override
    protected void getAllArguments() {

    }

    @Override
    protected void initializeUI(View rootView) {

        mUserID = ((ThirdPartyActivity) getActivity()).getUser_id();

        txtTwoWheelerCount = (TextView) rootView.findViewById(R.id.txtBikeCount);
        txtFourWheelerCount = (TextView) rootView.findViewById(R.id.txtCarCount);
        txtTripCount = (TextView) rootView.findViewById(R.id.txtTripCount);

        if (TextUtils.isEmpty(mUserID)) {
            Commons.toastShort(getActivity(), "User id is empty");
            getActivity().finish();
        }

        if (Validation.Network.isConnected(getActivity())) {
            ThirdPartyLogic.loadThirdParyDetails(this, mUserID);
        }

    }


    @Override
    protected int getRootLayout() {
        return R.layout.fragment_party_details;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        if (readRequestCode == Constants.REQ_THIRD_PARTY_DETAIL) {
            if (data != null && ((ThirdPartyDetailResponseModel) data).getStatus()) {
                ThirdPartyDetailsModel response = ((ThirdPartyDetailResponseModel) data).getThirdPartyDetailsModel();
                txtTripCount.setText("" + response.getTripCount());
            }
        }
    }

    @Override
    public void onDataEmpty(int readRequestCode) {

    }

    @Override
    public void onError(int readRequestCode, Object error) {

    }

    @Override
    public boolean needResponse() {
        return isAdded() && isVisible();
    }
}
