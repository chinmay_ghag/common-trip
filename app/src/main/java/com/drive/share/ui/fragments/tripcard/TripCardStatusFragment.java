package com.drive.share.ui.fragments.tripcard;

import android.view.View;

import com.drive.share.sharedrive.R;
import com.drive.share.ui.base.BaseFragment;
import com.drive.share.ui.customviews.HurdleView;

/**
 * Created by Pervacio-Dev on 3/4/2015.
 */
public class TripCardStatusFragment extends BaseFragment{

    @Override
    protected void getAllArguments() {

    }

    @Override
    protected void initializeUI(View rootView) {
        int noHurdles = 2;
        HurdleView view = (HurdleView) rootView.findViewById(R.id.prb_trip);
        view.setHurdleData(noHurdles,new String[]{"Dadar" ,"Pranit","Linus", "Virar"},new String[]{"Dadar sub" ,"Pranit sub","Linus sub", "Virar sub"});
        view.invalidate();
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_trip_card_status;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }
}
