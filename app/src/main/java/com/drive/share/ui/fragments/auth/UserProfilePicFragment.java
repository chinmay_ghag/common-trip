package com.drive.share.ui.fragments.auth;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.android.utilities.Validation;
import com.android.utilities.datareader.DataReader;
import com.bumptech.glide.Glide;
import com.drive.share.businesslayer.UpdateProfileLogic;
import com.drive.share.data.webapi.UserModel;
import com.drive.share.data.webapi.UserPicUploadResponse;
import com.drive.share.globals.App;
import com.drive.share.sharedrive.R;
import com.drive.share.storage.PreferenceUtils;
import com.drive.share.ui.activities.profile.ProfileSetUpActivity;
import com.drive.share.ui.base.BaseFragment;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * interface
 * to handle interaction events.
 * Use the {@link com.drive.share.ui.fragments.auth.UserProfilePicFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserProfilePicFragment extends BaseFragment implements View.OnClickListener,
        DataReader.DataClient {


    ImageView imgProfilePic, imgEditPic;
    Bitmap picture = null;

    //  private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TripInfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserProfilePicFragment newInstance(Bundle args) {
        UserProfilePicFragment fragment = new UserProfilePicFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public UserProfilePicFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    protected void getAllArguments() {

    }

    @Override
    protected void initializeUI(View rootView) {

        getActivity().findViewById(R.id.step1).setVisibility(View.VISIBLE);

        imgEditPic = (ImageView) rootView.findViewById(R.id.imgEditPic);
        imgProfilePic = (ImageView) rootView.findViewById(R.id.imgProfilePic);

        if (App.getInstance().getLoggedInUser() != null &&
                !TextUtils.isEmpty(App.getInstance().getLoggedInUser().getProfilePic())) {
            Glide.with(this)
                    .load(App.getInstance().getLoggedInUser().getProfilePic())
                    .centerCrop()
                    .crossFade()
                    .into(imgProfilePic);
        }

        imgProfilePic.setOnClickListener(this);
        imgEditPic.setOnClickListener(this);

        rootView.findViewById(R.id.btnUploadPic).setOnClickListener(this);
        rootView.findViewById(R.id.btnPicDone).setOnClickListener(this);
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_profile_pic;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
       /* if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }*/
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //  mListener = null;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imgEditPic:
                // show in larger viewport

                break;
            case R.id.imgProfilePic:
                // open image picker options
                //UpdateProfileLogic.showPictureSelector(getActivity(), this);
                break;

            case R.id.btnUploadPic:
                //select pic and set to imageview
                UpdateProfileLogic.showPictureSelector(getActivity(), this);
                break;

            case R.id.btnPicDone:
                //upload pic to service
                if(Validation.Network.isConnected(getActivity()))
                UpdateProfileLogic.validateAndUploadProfilePic(getActivity(), picture, this);
                break;


        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {

            case Activity.RESULT_OK:
                picture = UpdateProfileLogic.getCapturedImage(this, data, requestCode);
                imgProfilePic.setImageBitmap(picture);
                break;
            case Activity.RESULT_CANCELED:

                break;
        }
    }

    /**
     * On data received.
     *
     * @param readRequestCode the read request code
     * @param data            the data
     */
    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        UserPicUploadResponse response = (UserPicUploadResponse) data;
        if (response != null && response.getStatus()) {
            UserModel user = App.getInstance().getLoggedInUser();
            user.setProfilePic(response.getUrl());
            PreferenceUtils.insertObject(PreferenceUtils.KEY_USER_MODEL, user);

            /*((BaseActivity) getActivity()).fragmentTransaction(BaseActivity.REPLACE_FRAGMENT,
                    UserVehicleDetailFragment.newInstance(null),
                    R.id.frameContainer, false);*/

            ((ProfileSetUpActivity) getActivity()).handleNext();
        }
    }

    /**
     * On data empty.
     *
     * @param readRequestCode the read request code
     */
    @Override
    public void onDataEmpty(int readRequestCode) {
        ((ProfileSetUpActivity) getActivity()).handleNext();
    }

    /**
     * On error.
     *
     * @param readRequestCode the read request code
     * @param error
     */
    @Override
    public void onError(int readRequestCode, Object error) {
        /*List<Validation.UI.FieldError> inputErrors = (List<Validation.UI.FieldError>) error;
        for (Validation.UI.FieldError fieldError : inputErrors) {
            Toast.makeText(getActivity(), fieldError.getErrorMessage(), Toast.LENGTH_SHORT).show();
        }*/
        ((ProfileSetUpActivity) getActivity()).handleNext();
    }

    @Override
    public boolean needResponse() {
        return (isAdded() && isVisible());
    }
}
