package com.drive.share.ui.fragments.tripcard;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.drive.share.adapters.TalkAdapter;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.base.BaseFragment;

/**
 * Created by Pervacio-Dev on 3/4/2015.
 */
public class TripCardTalkFragment extends BaseFragment implements View.OnClickListener {

    ListView lstTalk;
    TalkAdapter adapter;
    EditText et_chatText;
    Button buttonSend;

    @Override
    protected void getAllArguments() {

    }

    @Override
    protected void initializeUI(View rootView) {
        lstTalk = (ListView)rootView.findViewById(R.id.listTalkView);
        adapter = new TalkAdapter(getActivity(),R.layout.talk_list_item);
        lstTalk.setAdapter(adapter);

        et_chatText = (EditText)rootView.findViewById(R.id.et_chatText);
        buttonSend = (Button)rootView.findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(this);
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_trip_card_talk;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    @Override
    public void onClick(View v) {
        adapter.add(new ChatElement(false, et_chatText.getText().toString()));
        et_chatText.setText("");
    }
}
