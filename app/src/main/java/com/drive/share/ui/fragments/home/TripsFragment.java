package com.drive.share.ui.fragments.home;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.utilities.datareader.DataReader;
import com.drive.share.adapters.HomeTripsAdapter;
import com.drive.share.businesslayer.TripEdtiorLogic;
import com.drive.share.data.webapi.CreatedTripResponse;
import com.drive.share.data.webapi.TripModel;
import com.drive.share.data.webapi.UserTripResponse;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.tripcard.TripCardActivity;
import com.drive.share.ui.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashsh on 29/1/15.
 */
public class TripsFragment extends BaseFragment implements DataReader.DataClient {


    private ListView lstTrip;
    private HomeTripsAdapter adapter;
    private List<TripModel> mTripsList;
    private View mEmptyTripView;
    private ProgressBar mProgressBar;
    private TextView mEmpTextView;

    public static final String TRIP_CARD_ID = "trip_card_id";

    @Override
    protected void getAllArguments() {

    }

    @Override
    protected void initializeUI(View rootView) {

        lstTrip = (ListView) rootView.findViewById(R.id.lstFriendList);
        mEmptyTripView = rootView.findViewById(android.R.id.empty);
        lstTrip.setEmptyView(mEmptyTripView);

        lstTrip.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), TripCardActivity.class);
                intent.putExtra(TRIP_CARD_ID, mTripsList.get(position).getTrip_card_id());
                startActivity(intent);
            }
        });

        mProgressBar = (ProgressBar) mEmptyTripView.findViewById(R.id.progress);
        mEmpTextView = (TextView) mEmptyTripView.findViewById(R.id.txtEmptyText);

        mTripsList = new ArrayList<TripModel>();
        adapter = new HomeTripsAdapter(getActivity(),
                mTripsList);
        lstTrip.setAdapter(adapter);

        TripEdtiorLogic.getUserTrip(this);


    }

    private void setEmptyLayout(boolean isEmpty) {

        if (isEmpty) {
            mProgressBar.setVisibility(View.GONE);
            mEmpTextView.setText("No trips found");
        } else {
            mProgressBar.setVisibility(View.VISIBLE);
            mEmpTextView.setText("Loading Trips");
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        TripEdtiorLogic.getUserTrip(this);
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_trip_list;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        if (null != data) {

            CreatedTripResponse tripModel = ((UserTripResponse) data).getTrip_list();


            if (tripModel != null) {
                setEmptyLayout(false);
                mTripsList.clear();

                if (tripModel.getTripsCreated() != null && tripModel.getTripsCreated().size() > 0) {
                    mTripsList.addAll(tripModel.getTripsCreated());
                }

                if (tripModel.getTripsInvitation() != null && tripModel.getTripsInvitation().size() > 0) {
                    mTripsList.addAll(tripModel.getTripsInvitation());
                }

                adapter.notifyDataSetChanged();
            } else {
                setEmptyLayout(true);
            }

        } else {
            setEmptyLayout(true);
        }
    }

    @Override
    public void onDataEmpty(int readRequestCode) {
        Log.d("Eroor", "Error fetching trips");
    }

    /**
     * On error.
     *
     * @param readRequestCode the read request code
     * @param error
     */
    @Override
    public void onError(int readRequestCode, Object error) {

    }


    @Override
    public boolean needResponse() {
        return isAdded();
    }
}
