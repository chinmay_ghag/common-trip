package com.drive.share.ui.activities.profile;

import android.os.Bundle;

import com.drive.share.sharedrive.R;
import com.drive.share.ui.base.BaseActivity;
import com.drive.share.ui.fragments.auth.UserVehicleDetailFragment;

/**
 * Created by webwerks on 16/2/15.
 */
public class VehiclRegistrationActivity extends BaseActivity {
    /**
     * Relese ui.
     */
    @Override
    protected void releaseUi() {

    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_auth);
    }

    @Override
    protected void initializeUi() {
        UserVehicleDetailFragment fragment = UserVehicleDetailFragment.newInstance(null);
        Bundle bundle = new Bundle();
        bundle.putInt(UserVehicleDetailFragment.COMING_FROM, UserVehicleDetailFragment.COMING_FROM_HOME);
        fragment.setArguments(bundle);
        fragmentTransaction(ADD_FRAGMENT, fragment, R.id.frameContainer, false);
    }
}
