package com.drive.share.ui.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.drive.share.data.webapi.TripModel;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.base.BaseActivity;
import com.drive.share.ui.fragments.editor.AddTripFragment;
import com.drive.share.ui.fragments.editor.TripInviteFragment;

/**
 * Created by atulOholic on 4/3/15.
 */
public class CreateTripActivity extends BaseActivity {

    private TripModel mTripModel = null;
    public static final String TAG_ADD_TRIP = "addTrip";
    public static final String TRIP_MODEL="TRIP_MODEL";

    @Override
    protected void releaseUi() {

    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_trip_editor);
        setTripModel((TripModel) getIntent().getSerializableExtra(TRIP_MODEL));
        fragmentTransaction(ADD_FRAGMENT, new AddTripFragment(), R.id.frameContainer, false, TAG_ADD_TRIP);
    }

    @Override
    protected void initializeUi() {
        setToolBar((android.support.v7.widget.Toolbar) findViewById(R.id.tool_bar), SET_HOME_AS_UP);
        setTitle(getString(R.string.title_activity_trip_editor));
    }

    public TripModel getTripModel() {
        if (mTripModel == null) {
            mTripModel = new TripModel();
        }
        return mTripModel;
    }

    public void setTripModel(TripModel mTripModel) {
        this.mTripModel = mTripModel;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_cerate_trip, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

        }

        return super.onOptionsItemSelected(item);
    }

    public void inviteFriends(String tripID) {
        TripInviteFragment inviteFragment = new TripInviteFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TripInviteFragment.EXTRA_TRIP_ID, tripID);
        Log.d("TripId",""+tripID);
        inviteFragment.setArguments(bundle);
        fragmentTransaction(BaseActivity.REPLACE_FRAGMENT, inviteFragment, R.id.frameContainer, false);
    }


}
