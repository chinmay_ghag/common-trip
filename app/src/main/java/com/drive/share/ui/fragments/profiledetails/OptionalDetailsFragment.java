package com.drive.share.ui.fragments.profiledetails;

import android.app.DatePickerDialog;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.android.utilities.Validation;
import com.android.utilities.datareader.DataReader;
import com.drive.share.businesslayer.UpdateProfileLogic;
import com.drive.share.dao.UserDao;
import com.drive.share.data.webapi.UserModel;
import com.drive.share.globals.App;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.profile.ProfileSetUpActivity;
import com.drive.share.ui.base.BaseFragment;
import com.opendroid.db.DbHelper;

/**
 * Created by yashesh on 12/2/15.
 */
public class OptionalDetailsFragment extends BaseFragment implements View.OnClickListener,
        DatePickerDialog.OnDateSetListener,DataReader.DataClient {

    private TextView txtDob = null;

    private View rootView = null;

    @Override
    protected void getAllArguments() {

    }

    @Override
    protected void initializeUI(View rootView) {

        this.rootView = rootView;
        txtDob = (TextView) rootView.findViewById(R.id.txtDob);
        UserModel user = App.getInstance().getLoggedInUser();

        if(user != null) {
            //((EditText) rootView.findViewById(R.id.txtUserName)).setText(user.getUserName());
            ((EditText) rootView.findViewById(R.id.txtEmailId)).setText(user.getEmail());
            //((RadioButton) rootView.findViewById(R.id.radioMale)).setChecked(user.getGender().equalsIgnoreCase("m"));
            //((RadioButton) rootView.findViewById(R.id.radioFemale)).setChecked(user.getGender().equalsIgnoreCase("f"));
            //txtDob.setText(user.getDob());
        }
        //txtDob.setOnClickListener(this);
        rootView.findViewById(R.id.btnSubmit).setOnClickListener(this);

    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_optional_detail;
    }

    /**
     * On iternet avaialble.
     */
    @Override
    public void onInternetAvailable() {

    }

    /**
     * On internet unavailable.
     */
    @Override
    public void onInternetUnavailable() {

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                if(Validation.Network.isConnected(getActivity()))
                UpdateProfileLogic.updateProfile(rootView,this);
                break;
            /*case R.id.txtDob:

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    new DatePickerDialog(getActivity(), android.R.style.Theme_Material_Light_Dialog, this, 1970, 0, 1).show();
                } else {
                    new DatePickerDialog(getActivity(),this, 1970, 0, 1).show();
                }

                break;*/
        }
    }

    /**
     * @param view        The view associated with this listener.
     * @param year        The year that was set.
     * @param monthOfYear The month that was set (0-11) for compatibility
     *                    with {@link java.util.Calendar}.
     * @param dayOfMonth  The day of the month that was set.
     */
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        txtDob.setText(getPaddedValue(dayOfMonth) + "-" + getPaddedValue(monthOfYear + 1) + "-" + year);
    }

    private String getPaddedValue(int number) {
        if (number > 9) {
            return number + "";
        } else {
            return "0" + number;
        }
    }

    /**
     * On data received.
     *
     * @param readRequestCode the read request code
     * @param data            the data
     */
    @Override
    public void onDataReceived(int readRequestCode, Object data) {

        //((BaseActivity) getActivity()).fragmentTransaction(BaseActivity.REPLACE_FRAGMENT, new UserProfilePicFragment(), R.id.frameContainer, false);
        UserDao dao = new UserDao(App.getInstance(), DbHelper.getInstance(App.getInstance()).getSQLiteDatabase());
     //   dao.createOrUpdate(model);
        ((ProfileSetUpActivity) getActivity()).handleNext();
    }

    /**
     * On data empty.
     *
     * @param readRequestCode the read request code
     */
    @Override
    public void onDataEmpty(int readRequestCode) {

    }

    /**
     * On error.
     *
     * @param readRequestCode the read request code
     * @param error
     */
    @Override
    public void onError(int readRequestCode, Object error) {

    }

    @Override
    public boolean needResponse() {
        return isAdded() && isVisible();
    }
}
