package com.drive.share.ui.activities.tripcard;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.utilities.datareader.DataReader;
import com.drive.share.adapters.InviteFriendsAdapter;
import com.drive.share.businesslayer.TripEdtiorLogic;
import com.drive.share.data.webapi.BaseResponseModel;
import com.drive.share.data.webapi.UserModel;
import com.drive.share.globals.Constants;
import com.drive.share.globals.utils.Commons;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pervacio-Dev on 3/13/2015.
 */
public class TripInviteActivity extends BaseActivity implements DataReader.DataClient {
    //UI
    private ListView lsvFriends;
    private View emptyView;
    private ProgressBar mProgressBar;
    private TextView mEmptyTextView;

    private InviteFriendsAdapter mInviteFriendsAdapter;
    private List<UserModel> mFriendList;
    private String mTripId;
    private Button btnInvite;

    public final static String EXTRA_TRIP_ID = "tripID";

    @Override
    protected void releaseUi() {

    }

    @Override
    protected void setContent() {
        setContentView(R.layout.fragment_invite_trip);
        mTripId = getIntent().getStringExtra("EXTRA_TRIP_ID");
    }

    @Override
    protected void initializeUi() {
        lsvFriends = (ListView) findViewById(R.id.lstFriendList);
        btnInvite = (Button) findViewById(R.id.btnInvite);
        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(mTripId)) {
                    TripEdtiorLogic.inviteUsers(TripInviteActivity.this, mTripId, getInvitedUsersList());
                }
            }
        });
    }

    private List<String> getInvitedUsersList() {

        List<String> userIDs = new ArrayList<String>();

        for (UserModel model : mFriendList) {
            if (model.isInvited()) {
                userIDs.add(model.getUserId());
            }
        }

        return userIDs;

    }

    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        if (readRequestCode == Constants.REQ_INVITE_USERS) {

            if (null != data) {
                Commons.toastShort(this, ((BaseResponseModel) data).getMessage());
            }
        } else if (null != data) {
            Log.e("", "ListView::" + data);

            setEmptyLayout(false);
            mFriendList.clear();
            mFriendList.addAll((ArrayList<UserModel>) data);

            mInviteFriendsAdapter.notifyDataSetChanged();

        } else {
            Log.e("", "ListView byll::");
            setEmptyLayout(true);
        }
    }

    private void setEmptyLayout(boolean isEmpty) {

        /*if (isEmpty) {
            mProgressBar.setVisibility(View.GONE);
            mEmptyTextView.setText("No friends found");
        } else {
            mProgressBar.setVisibility(View.VISIBLE);
            mEmptyTextView.setText("Loading");
        }*/

    }


    @Override
    public void onDataEmpty(int readRequestCode) {
        Log.e("", "ListView::" + readRequestCode);
        setEmptyLayout(true);
    }

    @Override
    public void onError(int readRequestCode, Object error) {
        Log.e("", "ListView::" + readRequestCode + " " + error.toString());
        setEmptyLayout(true);
    }

    @Override
    public boolean needResponse() {
        return false;
    }
}
