package com.drive.share.ui.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.drive.share.businesslayer.TripTimeSettings;
import com.drive.share.sharedrive.R;

/**
 * Created by yashesh on 21/2/15.
 */
public class TripDateTimeDialog extends Dialog implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, View.OnClickListener {


    TripTimeSettings.RepeatType repeatType;
    private int year;
    private int month;
    private int day;

    boolean startDateTime = true;

    private String strDate, strTime, endDate, endTime;
    private int hourOfDay, minute;
    private OnCreateTripTimeSettingsListener listener;

    private DatePickerDialog startDatePicker, endDatePicker;
    private TimePickerDialog startTimePicker, endTimePicker;
    private Context mContext;

    /**
     * Create a Dialog window that uses the default dialog frame style.
     *
     * @param context The Context the Dialog is to run it.  In particular, it
     *                uses the window manager and theme in this context to
     *                present its UI.
     */
    public TripDateTimeDialog(Context context, TripTimeSettings.RepeatType repeatType, OnCreateTripTimeSettingsListener listener) {
        super(context);
        this.repeatType = repeatType;
        mContext = context;
        this.listener = listener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Trip Time Serrings");
        setContentView(R.layout.dialog_trip_time);
        findViewById(R.id.lblTripEndDate).setOnClickListener(this);
        findViewById(R.id.lblTripStartDate).setOnClickListener(this);
        findViewById(R.id.lblTripStartTime).setOnClickListener(this);
        findViewById(R.id.lblTripEndTime).setOnClickListener(this);
        findViewById(R.id.btnOk).setOnClickListener(this);
        switch (repeatType) {
            case WEEKLY:
                findViewById(R.id.lblTripEndDate).setEnabled(false);
                findViewById(R.id.lblTripEndDate).setClickable(false);
                ((TextView) findViewById(R.id.lblTripEndDate)).setText("REPEAT WEEKLY");
                break;
            case NEVER:
                findViewById(R.id.lblTripEndDate).setEnabled(false);
                findViewById(R.id.lblTripEndDate).setClickable(false);
                ((TextView) findViewById(R.id.lblTripEndDate)).setText("ONE TIME TRIP");
                findViewById(R.id.llDays).setVisibility(View.GONE);
                break;
        }


    }

    /**
     * @param view        The view associated with this listener.
     * @param year        The year that was set.
     * @param monthOfYear The month that was set (0-11) for compatibility
     *                    with {@link java.util.Calendar}.
     * @param dayOfMonth  The day of the month that was set.
     */
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        this.year = year;
        this.month = monthOfYear + 1;
        this.day = dayOfMonth;

        String yearS = this.year + "";
        String monthS = month + "";
        String dayofMonthS = this.day + "";
        if (month < 10) {
            monthS = "0" + monthS;
        }
        if (dayOfMonth < 10) {
            dayofMonthS = "0" + dayofMonthS;
        }

        if (startDateTime) {
            strDate = yearS + monthS + dayofMonthS;
            ((TextView) findViewById(R.id.lblTripStartDate)).setText(yearS + "/" + monthS + "/" + dayofMonthS);
        } else {
            endDate = yearS + monthS + dayofMonthS;
            ((TextView) findViewById(R.id.lblTripEndDate)).setText(yearS + "/" + monthS + "/" + dayofMonthS);
        }
    }

    /**
     * @param view      The view associated with this listener.
     * @param hourOfDay The hour that was set.
     * @param minute    The minute that was set.
     */
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        this.hourOfDay = hourOfDay;
        this.minute = minute;
        String hd = "", min = "";
        if (hourOfDay < 10) {
            hd = "0" + hourOfDay;
        } else {
            hd = hourOfDay + "";
        }
        if (minute < 10) {
            min = "0" + minute;
        } else {
            min = minute + "";
        }

        if (startDateTime) {
            strTime = hd + "" + min;
            ((TextView) findViewById(R.id.lblTripStartTime)).setText(hd + ":" + min);

        } else {
            endTime = hd + "" + min;
            ((TextView) findViewById(R.id.lblTripEndTime)).setText(hd + ":" + min);
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnOk:
                TripTimeSettings settings = new TripTimeSettings();
                settings.setRepeatType(repeatType);
                settings.setStartDate(strDate);
                settings.setEndDate(endDate);
                settings.setStartTime(strTime);
                settings.setEndTime(endTime);
                if (repeatType.equals(TripTimeSettings.RepeatType.WEEKLY)) {
                    settings.setRepeatDays(getRepeatDays());
                }
                listener.onTripTimeSettingsCreated(settings);
                dismiss();
                break;
            //manipulate startDateTime to get correct value in onDateSet() and onTimeSet()
            case R.id.lblTripEndTime:
                startDateTime = false;
                endTimePicker = new TimePickerDialog(mContext,
                        this, 0, 0, true);
                endTimePicker.show();
                break;
            case R.id.lblTripStartTime:
                startDateTime = true;
                startTimePicker = new TimePickerDialog(mContext,
                        this, 0, 0, true);
                startTimePicker.show();
                break;
            case R.id.lblTripEndDate:
                startDateTime = false;
                endDatePicker = new DatePickerDialog(mContext,
                        this, 0, 0, 0);
                //   endDatePicker.getDatePicker().setMinDate(minDateMilliSecs);
                endDatePicker.show();
                break;
            case R.id.lblTripStartDate:
                startDateTime = true;
                startDatePicker = new DatePickerDialog(mContext,
                        this, 0, 0, 0);
                //   endDatePicker.getDatePicker().setMinDate(minDateMilliSecs);
                startDatePicker.show();
                break;
        }


    }


    public static interface OnCreateTripTimeSettingsListener {

        public void onTripTimeSettingsCreated(TripTimeSettings settings);

    }


    private String getRepeatDays() {
        String days = "";

        if (((CheckBox) findViewById(R.id.chkSun)).isChecked()) {
            days += "sunday,";
        } else if (((CheckBox) findViewById(R.id.chkMon)).isChecked()) {
            days += "monday,";
        } else if (((CheckBox) findViewById(R.id.chkTue)).isChecked()) {
            days += "tuesday,";
        } else if (((CheckBox) findViewById(R.id.chkWed)).isChecked()) {
            days += "wednesday,";
        } else if (((CheckBox) findViewById(R.id.chkThu)).isChecked()) {
            days += "thursday,";
        } else if (((CheckBox) findViewById(R.id.chkFri)).isChecked()) {
            days += "friday,";
        } else if (((CheckBox) findViewById(R.id.chkSat)).isChecked()) {
            days += "saturday,";
        }

        if (days.length() > 0)
            days = days.substring(0, days.length() - 1);

        return days;
    }


}
