package com.drive.share.ui.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import com.android.utilities.broadcastreveivers.InternetAvailablityReceiver;

import java.util.List;

/**
 * Created by webwerks on 29/1/15.
 */
public abstract class BaseActivity extends ActionBarActivity implements
        InternetAvailablityReceiver.InternetAlertReceiver {


    /**
     * The Constant ADD_FRAGMENT.
     */
    public static final int ADD_FRAGMENT = 0;

    /**
     * The Constant REPLACE_FRAGMENT.
     */
    public static final int REPLACE_FRAGMENT = 1;

    public static final boolean SET_HOME_AS_UP = true;
    public static final boolean DONT_SET_HOME_AS_UP = false;

    /**
     * The internet receiver.
     */
    private InternetAvailablityReceiver.AppInternetReceiver internetReceiver;


    @Override
    protected void onCreate(Bundle arg0) {
        //getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        super.onCreate(arg0);

        setContent();
        initializeUi();
    }


    public void setToolBar(Toolbar mToolBar, boolean ifSetHomeAsUp) {

        setSupportActionBar(mToolBar);
        if (ifSetHomeAsUp) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }


    /*
     * (non-Javadoc)
     *
     * @see android.support.v4.app.FragmentActivity#onPause()
     */
    @Override
    protected void onPause() {
        unregisterReceiver(internetReceiver);
        internetReceiver = null;
        super.onPause();
    }

    /*
     * (non-Javadoc)
     *
     * @see android.support.v4.app.FragmentActivity#onResume()
     */
    @Override
    protected void onResume() {
        super.onResume();
        internetReceiver = new InternetAvailablityReceiver.AppInternetReceiver(this);
        InternetAvailablityReceiver.InternetAvailablilityFilter filter = new InternetAvailablityReceiver.InternetAvailablilityFilter();
        registerReceiver(internetReceiver, filter);
    }

    /* (non-Javadoc)
     * @see com.android.utilities.BroadcastReveivers.InternetAvailablityReceiver.InternetAlertReceiver#onInternetUnavailable()
     */
    @Override
    public void onInternetUnavailable() {
        notifyFragmentsForInternet(false);
    }

    /* (non-Javadoc)
     * @see com.android.utilities.BroadcastReveivers.InternetAvailablityReceiver.InternetAlertReceiver#onInternetAvaialble()
     */
    @Override
    public void onInternetAvailable() {
        notifyFragmentsForInternet(true);
    }

    /**
     * Notify fragments for internet.
     *
     * @param available the available
     */
    private void notifyFragmentsForInternet(boolean available) {
        List<Fragment> frags = getSupportFragmentManager().getFragments();
        if (frags != null) {
            for (Fragment frag : frags) {
             /*   if (available && frag != null) {
                    ((InternetAlertReceiver) frag).onInternetAvailable();
                } else if (frag != null) {
                    ((InternetAlertReceiver) frag).onInternetUnavailable();
               }*/
            }
        }
    }


    /**
     * Fragment transaction.
     *
     * @param transactionType  the transaction type
     * @param fragment         -the new fragment
     * @param container        the container
     * @param isAddToBackStack the is add to back stack
     * @author yashesh does Fragment transaction.
     */
    public void fragmentTransaction(int transactionType, Fragment fragment,
                                    int container, boolean isAddToBackStack) {

        FragmentTransaction trans = getSupportFragmentManager()
                .beginTransaction();

        /***
         * set transaction animation vertical or horizontal
         * **/
        trans.setCustomAnimations(com.android.utilities.R.anim.pull_in_from_left,
                com.android.utilities.R.anim.swipe_out_to_left, com.android.utilities.R.anim.swipe_in_from_left,
                com.android.utilities.R.anim.push_out_to_left);

        /*****
         *
         * set transaction type add or replace
         *
         * **/
        switch (transactionType) {
            case (ADD_FRAGMENT):
                trans.add(container, fragment);
                break;
            case (REPLACE_FRAGMENT):
                trans.replace(container, fragment);
                if (isAddToBackStack)
                    trans.addToBackStack(null);

                break;
            default:
                break;

        }
        trans.commit();
    }

    public void fragmentTransaction(int transactionType, Fragment fragment,
                                    int container, boolean isAddToBackStack, String tag) {

        FragmentTransaction trans = getSupportFragmentManager()
                .beginTransaction();

        /***
         * set transaction animation vertical or horizontal
         * **/
        trans.setCustomAnimations(com.android.utilities.R.anim.pull_in_from_left,
                com.android.utilities.R.anim.swipe_out_to_left, com.android.utilities.R.anim.swipe_in_from_left,
                com.android.utilities.R.anim.push_out_to_left);

        /*****
         *
         * set transaction type add or replace
         *
         * **/
        switch (transactionType) {
            case (ADD_FRAGMENT):
                trans.add(container, fragment, tag);
                break;
            case (REPLACE_FRAGMENT):
                trans.replace(container, fragment, tag);
                if (isAddToBackStack)
                    trans.addToBackStack(null);

                break;
            default:
                break;

        }
        trans.commit();
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onTrimMemory(int)
     */
    @Override
    public void onTrimMemory(int level) {

        switch (level) {
            case Activity.TRIM_MEMORY_UI_HIDDEN:
                // app in background,relese ui resources.
                releaseUi();
                break;
            case Activity.TRIM_MEMORY_BACKGROUND:

                break;
            default:
                break;
        }
    }

    /**
     * Relese ui.
     */
    protected abstract void releaseUi();

    protected abstract void setContent();

    protected abstract void initializeUi();


}
