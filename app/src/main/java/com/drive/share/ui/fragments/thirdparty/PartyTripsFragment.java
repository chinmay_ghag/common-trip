package com.drive.share.ui.fragments.thirdparty;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.utilities.datareader.DataReader;
import com.drive.share.adapters.ThirdPartyTripsAdapter;
import com.drive.share.businesslayer.TripEdtiorLogic;
import com.drive.share.data.webapi.ThirdPartyTripsResponseModel;
import com.drive.share.data.webapi.TripModel;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.thirdparty.ThirdPartyActivity;
import com.drive.share.ui.activities.tripcard.TripInviteActivity;
import com.drive.share.ui.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;


public class PartyTripsFragment extends BaseFragment implements DataReader.DataClient {


    private ListView lstTrip;
    private ThirdPartyTripsAdapter adapter;
    private List<TripModel> mTripsList;
    private View mEmptyTripView;
    private ProgressBar mProgressBar;
    private TextView mEmpTextView;

    @Override
    protected void getAllArguments() {

    }

    @Override
    protected void initializeUI(View rootView) {

        lstTrip = (ListView) rootView.findViewById(R.id.lstFriendList);
        mEmptyTripView = rootView.findViewById(android.R.id.empty);
        lstTrip.setEmptyView(mEmptyTripView);

        mProgressBar = (ProgressBar) mEmptyTripView.findViewById(R.id.progress);
        mEmpTextView = (TextView) mEmptyTripView.findViewById(R.id.txtEmptyText);

        mTripsList = new ArrayList<TripModel>();
        adapter = new ThirdPartyTripsAdapter(getActivity(),
                mTripsList);
        lstTrip.setAdapter(adapter);

        lstTrip.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });


    }


    private void setEmptyLayout(boolean isEmpty) {

        if (isEmpty) {
            mProgressBar.setVisibility(View.GONE);
            mEmpTextView.setText("No trips found");
        } else {
            mProgressBar.setVisibility(View.VISIBLE);
            mEmpTextView.setText("Loading Trips");
        }

    }

    @Override
    public void onResume() {
        super.onResume();
//        TripEdtiorLogic.getUserTrip(this);
        TripEdtiorLogic.getThirdPartyUserTrip(this, ((ThirdPartyActivity) getActivity()).getUser_id());
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_trip_list;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        if (null != data) {

            List<TripModel> userTrips = ((ThirdPartyTripsResponseModel) data).getUserTripList();


            if (userTrips != null && userTrips.size() > 0) {
                setEmptyLayout(false);
                mTripsList.clear();

                mTripsList.addAll(userTrips);

                adapter.notifyDataSetChanged();
            } else {
                setEmptyLayout(true);
            }


        } else {
            setEmptyLayout(true);
        }
    }

    @Override
    public void onDataEmpty(int readRequestCode) {
        Log.d("Eroor", "Error fetching trips");
    }

    /**
     * On error.
     *
     * @param readRequestCode the read request code
     * @param error
     */
    @Override
    public void onError(int readRequestCode, Object error) {

    }


    @Override
    public boolean needResponse() {
        return isAdded();
    }
}
