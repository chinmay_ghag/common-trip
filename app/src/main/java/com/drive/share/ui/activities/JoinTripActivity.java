package com.drive.share.ui.activities;

import android.os.Bundle;
import android.text.TextUtils;

import com.android.utilities.datareader.DataReader;
import com.drive.share.businesslayer.TripEdtiorLogic;
import com.drive.share.data.webapi.BaseResponseModel;
import com.drive.share.globals.Constants;
import com.drive.share.globals.utils.Commons;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.base.BaseActivity;
import com.drive.share.ui.fragments.editor.TripMapFragment;

public class JoinTripActivity extends BaseActivity implements TripMapFragment.OnFetchLocationListener, DataReader.DataClient {

    private String mTripId = null, mDriverId = null;
    public final static String EXTRA_TRIP_ID = "tripId";
    public final static String EXTRA_DRIVER_ID = "driverId";

    @Override
    protected void releaseUi() {

    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_join_trip);
    }

    @Override
    protected void initializeUi() {

        mTripId = getIntent().getStringExtra(EXTRA_TRIP_ID);
        mDriverId = getIntent().getStringExtra(EXTRA_DRIVER_ID);

        TripMapFragment tripMapFragment = new TripMapFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(TripMapFragment.COMING_FROM, TripMapFragment.COMING_FROM_JOIN);
        tripMapFragment.setArguments(bundle);
        fragmentTransaction(ADD_FRAGMENT, tripMapFragment, R.id.mapContainer, false);
    }


    @Override
    public void setFetchedLocation(String placeName, String latitude, String longitude) {
        if (!(TextUtils.isEmpty(mTripId)) && !(TextUtils.isEmpty(mDriverId)) &&
                !(TextUtils.isEmpty(latitude)) && !(TextUtils.isEmpty(longitude)) && !(TextUtils.isEmpty(placeName))) {
            TripEdtiorLogic.joinTripRequest(this, mDriverId, mTripId, latitude, longitude, placeName);

        } else {
            Commons.toastShort(this, "Incomplete data to join trip");
        }
    }

    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        if (readRequestCode == Constants.REQ_JOIN_TRIP) {

            BaseResponseModel response = (BaseResponseModel) data;
            if (response != null) {
                if (response.getStatus()) {
                    Commons.toastShort(this, "Congratulations, you have joined the trip!");
                } else {
                    Commons.toastShort(this, "Sorry, there was an error while joining the trip. Please try again!");
                }
            } else {
                Commons.toastShort(this, "Sorry, there was an error while joining the trip. Please try again!");
            }
        }
    }

    @Override
    public void onDataEmpty(int readRequestCode) {

    }

    @Override
    public void onError(int readRequestCode, Object error) {
        if (readRequestCode == Constants.REQ_JOIN_TRIP) {
            Commons.toastShort(this, "Sorry, there was an error while joining the trip. Please try again!");
        }
    }

    @Override
    public boolean needResponse() {
        return true;
    }
}
