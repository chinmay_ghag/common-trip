package com.drive.share.ui.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by Pervacio-Dev on 3/6/2015.
 */
public class HurdleView extends View {
    private Paint mDefaultPaint;
    private Paint mHurdlePaint;
    Paint mPaint;
    Paint mPaintSub;

    private int mNumHurdles = 1;

    String[] mMainData = null;
    String[] mSubData = null;


    public HurdleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setHurdleData(int mHurdleCount, String[] mMainData, String[] mSubData) {
        mNumHurdles = mHurdleCount;
        this.mMainData = mMainData;
        this.mSubData = mSubData;
    }

    private void init() {
        mDefaultPaint = new Paint();
        mDefaultPaint.setColor(Color.RED);
        mDefaultPaint.setStrokeWidth(3.0f);
        mDefaultPaint.setStyle(Paint.Style.FILL);
        mHurdlePaint = new Paint();
        mHurdlePaint.setColor(Color.YELLOW);
        mHurdlePaint.setStyle(Paint.Style.FILL);

         mPaint = new Paint();
        mPaint.setTextSize(40);
        mPaint.setColor(Color.RED);

         mPaintSub = new Paint();
        mPaintSub.setTextSize(40);
        mPaintSub.setColor(Color.GRAY);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawLine(getLeft() + 50, getTop() + 30, getLeft() + 50, getBottom() - 10, mDefaultPaint);
        canvas.drawCircle(getLeft() + 50, getTop() + 30, 20.0f, mHurdlePaint);
        final float hurdleDistnace = calculateHurdleDistance((float) mNumHurdles);

        canvas.drawText(mMainData[0], getLeft() + 80, getTop() + 30, mPaint);
        canvas.drawText(mSubData[0], getLeft() + 80, getTop() + 30 +mPaint.getTextSize(), mPaintSub);

        float currentY = 0;
        for (int hurdle = 1; hurdle <= mNumHurdles; hurdle++) {
            canvas.drawCircle(getLeft() + 50, currentY + ((getTop()) + hurdleDistnace), 20.0f, mHurdlePaint);
            if (mMainData != null && mMainData.length + 1 > hurdle && mMainData[hurdle - 1] != null) {
                canvas.drawText(mMainData[hurdle], getLeft() + 80, currentY + ((getTop()) + hurdleDistnace), mPaint);
                canvas.drawText(mSubData[hurdle], getLeft() + 80, currentY + ((getTop()) + hurdleDistnace)+mPaint.getTextSize(), mPaintSub);
            }
            currentY = currentY + ((getTop()) + hurdleDistnace);
            Log.d("Drawing circle", "cx : " + getLeft() + 50 + "  cy : " + currentY);


        }
        canvas.drawCircle(getLeft() + 50, getBottom() - 30, 20.0f, mHurdlePaint);
        canvas.drawText(mMainData[mNumHurdles + 1], getLeft() + 80, getBottom() - 30, mPaint);
        canvas.drawText(mSubData[mNumHurdles + 1], getLeft() + 80, getBottom() - 30+mPaint.getTextSize(), mPaintSub);
    }

    private float calculateHurdleDistance(float numHurdles) {
        return getBottom() / (numHurdles + 1);
    }
}
