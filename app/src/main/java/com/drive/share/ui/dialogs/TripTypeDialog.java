package com.drive.share.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.RadioButton;

import com.drive.share.globals.utils.Commons;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.fragments.editor.AddTripFragment;

/**
 * Created by atulOholic on 4/3/15.
 */
public class TripTypeDialog extends Dialog {

    public final static String FLAG_TRIP_TYPE = "tripType";
    public final static int FLAG_SINGLE = 101;
    public final static int FLAG_WEEKLY = 102;

    private Context mContext;
    private Fragment mFragment;

    OnCreateTripTypeListener listener;
    private int mTripType;


    public TripTypeDialog(Context context, Fragment fragment, int defaultType) {
        super(context);
        mContext = context;
        mFragment = fragment;
        listener = (AddTripFragment) mFragment;
        mTripType = defaultType;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_trip_type);
        setTitle(mContext.getString(R.string.lbl_trip_type));

        Commons.setDialogFullScreen(mContext, this);

        setDefault();

        findViewById(R.id.txtSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (((RadioButton) findViewById(R.id.rdSingle)).isChecked()) {
                    listener.onCreateTripType(FLAG_SINGLE);
                } else {
                    listener.onCreateTripType(FLAG_WEEKLY);
                }

                dismiss();


            }
        });

        findViewById(R.id.txtCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    private void setDefault() {
        if (mTripType == FLAG_SINGLE) {
            ((RadioButton) findViewById(R.id.rdSingle)).setChecked(true);
        } else {
            ((RadioButton) findViewById(R.id.rdWeekly)).setChecked(true);
        }
    }

    public static interface OnCreateTripTypeListener {
        public void onCreateTripType(int flag);
    }
}
