package com.drive.share.ui.activities.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.HomeActivity;
import com.drive.share.ui.base.BaseActivity;
import com.drive.share.ui.fragments.auth.UserProfilePicFragment;
import com.drive.share.ui.fragments.auth.UserVehicleDetailFragment;
import com.drive.share.ui.fragments.profiledetails.OptionalDetailsFragment;

/**
 * Created by yashesh on 12/2/15.
 */
public class ProfileSetUpActivity extends BaseActivity {
    /**
     * Relese ui.
     */
    private Button btnSkip;
    @Override
    protected void releaseUi() {

    }

    @Override
    protected void setContent() {
        setContentView(R.layout.activity_profile_comple);
    }

    @Override
    protected void initializeUi() {
        // show optional details form.
        fragmentTransaction(ADD_FRAGMENT, new OptionalDetailsFragment(), R.id.frameContainer, false);

        btnSkip = (Button) findViewById(R.id.btnSkip);
        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleNext();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cerate_trip, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            handleNext();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void handleNext() {
        if (getSupportFragmentManager().findFragmentById(R.id.frameContainer)
                instanceof OptionalDetailsFragment) {
            fragmentTransaction(REPLACE_FRAGMENT, new UserProfilePicFragment(),
                    R.id.frameContainer, false);
        } else if (getSupportFragmentManager().findFragmentById(R.id.frameContainer)
                instanceof UserProfilePicFragment) {

            UserVehicleDetailFragment fragment = UserVehicleDetailFragment.newInstance(null);
            Bundle bundle = new Bundle();
            bundle.putInt(UserVehicleDetailFragment.COMING_FROM, UserVehicleDetailFragment.COMING_FROM_LOGIN);
            fragment.setArguments(bundle);
            fragmentTransaction(REPLACE_FRAGMENT, fragment,
                    R.id.frameContainer, false);
            findViewById(R.id.step2).setVisibility(View.VISIBLE);
        } else {
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        }
    }
}
