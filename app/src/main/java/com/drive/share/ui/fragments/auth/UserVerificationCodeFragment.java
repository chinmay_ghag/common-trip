package com.drive.share.ui.fragments.auth;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.android.utilities.Validation;
import com.android.utilities.datareader.DataReader;
import com.drive.share.businesslayer.AuthLogic;
import com.drive.share.data.webapi.BaseResponseModel;
import com.drive.share.globals.App;
import com.drive.share.globals.Constants;
import com.drive.share.globals.utils.Commons;
import com.drive.share.services.ContactSyncService;
import com.drive.share.sharedrive.R;
import com.drive.share.storage.PreferenceUtils;
import com.drive.share.ui.activities.auth.AuthenticationActivity;
import com.drive.share.ui.activities.profile.ProfileSetUpActivity;
import com.drive.share.ui.base.BaseFragment;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * interface
 * to handle interaction events.
 * Use the {@link com.drive.share.ui.fragments.auth.UserVerificationCodeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserVerificationCodeFragment extends BaseFragment implements View.OnClickListener, DataReader.DataClient {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private  String verificationCode;

    EditText txtVerification, txtVerification1, txtVerification2, txtVerification3;

    //  private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TripInfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserVerificationCodeFragment newInstance(Bundle args) {
        UserVerificationCodeFragment fragment = new UserVerificationCodeFragment();


        fragment.setArguments(args);
        return fragment;
    }

    public UserVerificationCodeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    protected void getAllArguments() {

    }

    @Override
    protected void initializeUI(View rootView) {
        txtVerification = (EditText) rootView.findViewById(R.id.txtSmsVerificationCode);
        txtVerification1 = (EditText) rootView.findViewById(R.id.txtSmsVerificationCode1);
        txtVerification2 = (EditText) rootView.findViewById(R.id.txtSmsVerificationCode2);
        txtVerification3 = (EditText) rootView.findViewById(R.id.txtSmsVerificationCode3);

        //if (!PreferenceUtils.getBoolean(PreferenceUtils.KEY_USER_NEW)) {
        verificationCode = PreferenceUtils.getString(PreferenceUtils.KEY_VERIFICATION_CODE);
        txtVerification.setText(verificationCode != null ? verificationCode.charAt(0) + "" : "");
        txtVerification1.setText(verificationCode != null ? verificationCode.charAt(1) + "" : "");
        txtVerification2.setText(verificationCode != null ? verificationCode.charAt(2) + "" : "");
        txtVerification3.setText(verificationCode != null ? verificationCode.charAt(3) + "" : "");
        //}


        txtVerification.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s))
                    txtVerification1.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtVerification1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s))
                    txtVerification2.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        txtVerification2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s))
                    txtVerification3.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        rootView.findViewById(R.id.btnVerify).setOnClickListener(this);
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_verification_code;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
       /* if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }*/
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //  mListener = null;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnVerify:
                //verify();

                startProfileSetUpActivity();

                break;
        }
    }

    private void startProfileSetUpActivity() {

        // sync contacts
        if (Validation.Network.isConnected(getActivity())) {
            App.getInstance().startService(new Intent(App.getInstance(), ContactSyncService.class));
        }

        App.getInstance().setLoggedInUser(((AuthenticationActivity) getActivity()).getUser());
        AuthLogic.Verification.verifyCode(verificationCode);
        getActivity().startActivity(new Intent(getActivity(), ProfileSetUpActivity.class));
        getActivity().finish();
    }

    private void verify() {


        String verificationCode = txtVerification.getText().toString();

        if (TextUtils.isEmpty(verificationCode)) {
            txtVerification.setError(App.getInstance().getString(R.string.error_verification_code));
            return;
        }

        String verificationCode1 = txtVerification1.getText().toString();

        if (TextUtils.isEmpty(verificationCode1)) {
            txtVerification1.setError(App.getInstance().getString(R.string.error_verification_code));
            return;
        }

        String verificationCode2 = txtVerification2.getText().toString();

        if (TextUtils.isEmpty(verificationCode2)) {
            txtVerification2.setError(App.getInstance().getString(R.string.error_verification_code));
            return;
        }

        String verificationCode3 = txtVerification3.getText().toString();

        if (TextUtils.isEmpty(verificationCode3)) {
            txtVerification3.setError(App.getInstance().getString(R.string.error_verification_code));
            return;
        }

        String finalVerificationCode = verificationCode + verificationCode1 + verificationCode2 + verificationCode3;

        if (Validation.Network.isConnected(getActivity())) {
            AuthLogic.sendVerificationCode(finalVerificationCode, this);
        } else {
            Commons.errorNoInternet(getActivity());
        }








        /*if (!PreferenceUtils.getBoolean(PreferenceUtils.KEY_USER_NEW)) {


            String verificationCode = txtVerification.getText().toString() + "" + txtVerification1.getText().toString() + "" +
                    txtVerification2.getText().toString() + "" + txtVerification3.getText().toString();
            if (AuthLogic.Verification.verifyCode(verificationCode)) {

                // sync contacts
                if (Validation.Network.isConnected(getActivity())) {
                    App.getInstance().startService(new Intent(App.getInstance(), ContactSyncService.class));
                }


                // next screen
                getActivity().startActivity(new Intent(getActivity(), ProfileSetUpActivity.class));
                getActivity().finish();
            } else {
                txtVerification.setError(App.getInstance().getString(R.string.error_verification_code));
            }
        } else {
            Toast.makeText(getActivity(), "Old User", Toast.LENGTH_SHORT).show();

            String verificationCode = txtVerification.getText().toString() + "" + txtVerification1.getText().toString() + "" +
                    txtVerification2.getText().toString() + "" + txtVerification3.getText().toString();
            if (AuthLogic.Verification.verifyCode(verificationCode)) {

                // sync contacts
                if (Validation.Network.isConnected(getActivity())) {
                    App.getInstance().startService(new Intent(App.getInstance(), ContactSyncService.class));
                }


                // next screen
                getActivity().startActivity(new Intent(getActivity(), ProfileSetUpActivity.class));
                getActivity().finish();
            } else {
                txtVerification.setError(App.getInstance().getString(R.string.error_verification_code));
            }
        }*/
    }

    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        if (readRequestCode == Constants.REQ_VERIFICATION_CODE) {
            BaseResponseModel responseModel = (BaseResponseModel) data;
            if (responseModel.getStatus()) {
                Commons.toastShort(getActivity(), "You have been registered successfully");
                startProfileSetUpActivity();
            } else {
                Commons.toastShort(getActivity(), "Verification failed. Please try again!");
            }
        } else {
            Commons.toastShort(getActivity(), "Verification failed. Please try again!");
        }
    }

    @Override
    public void onDataEmpty(int readRequestCode) {

    }

    @Override
    public void onError(int readRequestCode, Object error) {

    }

    @Override
    public boolean needResponse() {
        return isAdded() && isVisible();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
   /* public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }*/

}
