package com.drive.share.ui.activities.tripcard;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.android.utilities.datareader.DataReader;
import com.drive.share.adapters.TripCardPagerAdapter;
import com.drive.share.businesslayer.TripCardLogic;
import com.drive.share.dao.TripDao;
import com.drive.share.data.webapi.Passenger;
import com.drive.share.data.webapi.TripCardDetails;
import com.drive.share.data.webapi.TripCardResponseModel;
import com.drive.share.data.webapi.TripCardStatus;
import com.drive.share.data.webapi.TripModel;
import com.drive.share.globals.App;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.CreateTripActivity;
import com.drive.share.ui.activities.TripEditorActivity;
import com.drive.share.ui.base.BaseActivity;
import com.drive.share.ui.customviews.SlidingTabLayout;
import com.drive.share.ui.fragments.editor.TripInviteFragment;
import com.drive.share.ui.fragments.home.TripsFragment;
import com.opendroid.db.DbHelper;

import java.util.List;

/**
 * Created by yashesh on 3/4/2015.
 */
public class TripCardActivity extends BaseActivity implements DataReader.DataClient,MenuItem.OnMenuItemClickListener {
    private ViewPager mViewPager;
    private SlidingTabLayout mSlidingTabLayout;
    private Toolbar mToolbar;
    private String mTripCardId;
    private TripCardStatus mTripCardStatus;
    public static  final int DELETE_TRIP=123;
    private TripCardResponseModel cardResponseModel;


    @Override
    protected void releaseUi() {

    }


    @Override
    protected void setContent() {
        setContentView(R.layout.activity_trip_card);
    }

    public TripCardStatus getmTripCardStatus() {
        return mTripCardStatus;
    }


    public void setmTripCardStatus(TripCardStatus mTripCardStatus) {
        this.mTripCardStatus = mTripCardStatus;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(App.getInstance().getAppUserId().equals(mTripCardId)) {
            // inflate trip owner menu
            getMenuInflater().inflate(R.menu.menu_tripacard_owner, menu);

            menu.findItem(R.id.menuSettings).setOnMenuItemClickListener(this);
            menu.findItem(R.id.menuDelete).setOnMenuItemClickListener(this);
            menu.findItem(R.id.menuShare).setOnMenuItemClickListener(this);
            menu.findItem(R.id.menuInvite).setOnMenuItemClickListener(this);
            menu.findItem(R.id.menuEdit).setOnMenuItemClickListener(this);

        }else if(isTripJoined()){
            // inflate trip member menu


        }else{
            // inflate trip non member menu

        }



        return true;
    }


    private boolean isTripJoined(){

        boolean joined=false;
        for(Passenger passenger : getTripCardPassengers()){

          if(App.getInstance().getAppUserId().equals(passenger.getPassengerId())){
                  joined=true;
                    break;
            }
        }

        return joined;
    }


    @Override
    protected void initializeUi() {
        setmTripCardId(getIntent().getStringExtra(TripsFragment.TRIP_CARD_ID));

        mToolbar = (Toolbar) findViewById(R.id.tool_bar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPager.setOffscreenPageLimit(3);


        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        mTripCardId = getIntent().getStringExtra(TripsFragment.TRIP_CARD_ID);
        if (mTripCardId != null) {
            TripCardLogic.tripDetails(mTripCardId, this);
        }

    }


    public TripCardResponseModel
    getCardResponseModel() {
        return cardResponseModel;
    }

    public void setCardResponseModel(TripCardResponseModel cardResponseModel) {
        this.cardResponseModel = cardResponseModel;
    }

    public TripCardDetails getTripCardDetails() {
        return getCardResponseModel().getTripDetails().getTripCardDetails();
    }

    public List<Passenger> getTripCardPassengers() {
        return getCardResponseModel().getTripDetails().getPassengers();
    }

    public String getmTripCardId() {
        //        return "100045";
        return (mTripCardId == null) ? "100045" : mTripCardId;
    }

    public void setmTripCardId(String mTripCardId) {
        this.mTripCardId = mTripCardId;
    }

    @Override
    public void onDataReceived(int readRequestCode, Object data) {



        switch (readRequestCode){
            case DELETE_TRIP:

                new TripDao(this, DbHelper.getInstance(this).getSQLiteDatabase()).deleteByField(TripModel.COLUMN_TRIP_CARD_ID,mTripCardId);
                finish();
                break;
            default:
                Log.i("patrol", "Data RE: " + data);
                setCardResponseModel((TripCardResponseModel) data);
                setmTripCardStatus(((TripCardResponseModel) data).getTripDetails().getTripCardStatus());
                mViewPager.setAdapter(new TripCardPagerAdapter(this, getSupportFragmentManager()));
                //mSlidingTabLayout.setDistributeEvenly(true);
                mSlidingTabLayout.setViewPager(mViewPager);
                mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.color_primary_dark));

                break;
        }


    }

    @Override
    public void onDataEmpty(int readRequestCode) {

    }

    @Override
    public void onError(int readRequestCode, Object error) {

    }

    @Override
    public boolean needResponse() {
        return true;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menuDelete:
                //call delete web service .

                break;
            case R.id.menuEdit:
                     List<TripModel> tripModels=  new TripDao(this,DbHelper.getInstance(this).getSQLiteDatabase()).findAllByField(TripModel.COLUMN_TRIP_CARD_ID,mTripCardId,null);
                     Intent intent=new Intent(this,TripModel.class);
                      intent.putExtra(CreateTripActivity.TRIP_MODEL,tripModels.get(0));
                      startActivity(intent);
                break;
            case R.id.menuInvite:
                    Fragment inviteFragment=new TripInviteFragment();
                    Bundle inviteArgs=new Bundle();
                    inviteArgs.putString(TripInviteFragment.EXTRA_TRIP_ID,mTripCardId);
                    fragmentTransaction(REPLACE_FRAGMENT,inviteFragment,R.id.frameContainer,true);


                break;
            case R.id.menuSettings:


                break;
            case R.id.menuShare:


                break;
        }

        return true;
    }



}
