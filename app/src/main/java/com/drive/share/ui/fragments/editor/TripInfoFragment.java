package com.drive.share.ui.fragments.editor;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.format.Time;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.drive.share.adapters.VehiclePagerAdapter;
import com.drive.share.businesslayer.TripTimeSettings;
import com.drive.share.businesslayer.UpdateProfileLogic;
import com.drive.share.data.webapi.TripModel;
import com.drive.share.data.webapi.VehicleModel;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.base.BaseFragment;
import com.drive.share.ui.dialogs.TripDateTimeDialog;

import java.util.Date;
import java.util.List;

import be.billington.calendar.recurrencepicker.EventRecurrence;
import be.billington.calendar.recurrencepicker.EventRecurrenceFormatter;
import be.billington.calendar.recurrencepicker.RecurrencePickerDialog;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * interface
 * to handle interaction events.
 * Use the {@link TripInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TripInfoFragment extends BaseFragment implements View.OnClickListener, RecurrencePickerDialog.OnRecurrenceSetListener, ViewPager.OnPageChangeListener, RadioGroup.OnCheckedChangeListener, TripDateTimeDialog.OnCreateTripTimeSettingsListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private TripTimeSettings tripTimeSettings;
    private View rootView;
    private TextView lblRecurrence;
    private List<VehicleModel> vehicles;
    private CircleImageView imgTripImage;
    private RadioButton rdPickUp, rdDropOut;
    //  private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TripInfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TripInfoFragment newInstance(Bundle args) {
        TripInfoFragment fragment = new TripInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public TripInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    protected void getAllArguments() {

    }

    String recurrenceRule;
    ViewPager pagerVehicleType;
    Button btnPrevType, btnNextType;
    VehiclePagerAdapter adapter;

    @Override
    protected void initializeUI(View rootView) {
        this.rootView = rootView;
        /*rootView.findViewById(R.id.btnNext).setOnClickListener(this);
        lblRecurrence = (TextView) rootView.findViewById(R.id.lblRecurrence);

        btnPrevType = (Button) rootView.findViewById(R.id.btnPrevType);
        btnPrevType.setOnClickListener(this);

        btnNextType = (Button) rootView.findViewById(R.id.btnNextType);
        btnNextType.setOnClickListener(this);

        imgTripImage = (CircleImageView) rootView.findViewById(R.id.imgTripImage);
        imgTripImage.setOnClickListener(this);

        rdPickUp = (RadioButton) rootView.findViewById(R.id.rdPickUp);
        rdDropOut = (RadioButton) rootView.findViewById(R.id.rdDrop);


        lblRecurrence.setOnClickListener(this);
        pagerVehicleType = (ViewPager) rootView.findViewById(R.id.pagerVehicleType);
        vehicles = TripEdtiorLogic.getLocalVhicles();
        adapter = new VehiclePagerAdapter(getActivity(), vehicles);
        pagerVehicleType.setAdapter(adapter);
        pagerVehicleType.setOnPageChangeListener(this);


        ((RadioGroup) rootView.findViewById(R.id.rgTripRepeats)).setOnCheckedChangeListener(this);*/

    }


    @Override
    public void onResume() {
        super.onResume();
        /*if (TripEdtiorLogic.getLocalVhicles().size() < 1) {
            new AlertDialog.Builder(getActivity()).setTitle(R.string.title_vehicle_alert).setPositiveButton(R.string.prompt_select, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    getActivity().startActivity(new Intent(getActivity(), VehiclRegistrationActivity.class));
                }
            }).setNegativeButton(R.string.prompt_no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    getActivity().finish();
                }
            }).create().show();
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {

            case Activity.RESULT_OK:
                Bitmap picture = UpdateProfileLogic.getCapturedImage(this, data, requestCode);
                if (picture != null) {
                    imgTripImage.setImageBitmap(picture);
                }
                break;
            case Activity.RESULT_CANCELED:

                break;
        }
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_create_trip;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
       /* if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }*/
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //  mListener = null;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /*case R.id.btnNext:
                // go to next module.
                TripEditorActivity editorActivity = ((TripEditorActivity) getActivity());
                TripModel trip = TripEdtiorLogic.getTrip(((EditText) rootView.findViewById(R.id.txtTripName)).getText().toString().trim());
                trip.setVehicle_id(vehicles.get(pagerVehicleType.getCurrentItem()).getVehicleId());
                trip.setIs_pick_up(isTripPickUp());
                // set the trip timings
                tripTimeSettings.setTripTime(trip);
                editorActivity.setTripModel(trip);

                editorActivity.getPagerEditors().setCurrentItem(editorActivity.getPagerEditors().getCurrentItem() + 1);
                break;

            case R.id.lblRecurrence:

                RecurrencePickerDialog recurrenceDialog = new RecurrencePickerDialog();
                recurrenceDialog.setOnRecurrenceSetListener(this);
                if (recurrenceRule != null && recurrenceRule.length() > 0) {
                    Bundle bundle = new Bundle();
                    bundle.putString(RecurrencePickerDialog.BUNDLE_RRULE, recurrenceRule);
                    recurrenceDialog.setArguments(bundle);
                }
                recurrenceDialog.show(getActivity().getSupportFragmentManager(), "RecurrentFrag");

                break;

            case R.id.btnPrevType:
                if (pagerVehicleType.getCurrentItem() > 0) {
                    pagerVehicleType.setCurrentItem(pagerVehicleType.getCurrentItem() - 1);
                }
                break;

            case R.id.btnNextType:
                if (pagerVehicleType.getCurrentItem() < adapter.getCount() - 1) {
                    pagerVehicleType.setCurrentItem(pagerVehicleType.getCurrentItem() + 1);
                }
                break;

            case R.id.imgTripImage:
                UpdateProfileLogic.showPictureSelector(getActivity(), this);
                break;*/

        }
    }

    @Override
    public void onRecurrenceSet(String s) {
        recurrenceRule = s;
        if (recurrenceRule != null && recurrenceRule.length() > 0) {
            EventRecurrence recurrenceEvent = new EventRecurrence();
            recurrenceEvent.setStartDate(new Time("" + new Date().getTime()));
            recurrenceEvent.parse(s);

            String str = EventRecurrenceFormatter.getRepeatString(getActivity(), getResources(), recurrenceEvent, true);
            lblRecurrence.setText(str);
        } else {
            lblRecurrence.setText("No recurrence");
        }
    }

    /**
     * This method will be invoked when the current page is scrolled, either as part
     * of a programmatically initiated smooth scroll or a user initiated touch scroll.
     *
     * @param position             Position index of the first page currently being displayed.
     *                             Page position+1 will be visible if positionOffset is nonzero.
     * @param positionOffset       Value from [0, 1) indicating the offset from the page at position.
     * @param positionOffsetPixels Value in pixels indicating the offset from position.
     */
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }


    @Override
    public void onPageSelected(int position) {
        if (position == 0) {
            btnPrevType.setVisibility(View.INVISIBLE);
        } else {
            btnPrevType.setVisibility(View.VISIBLE);
        }

        if (position == (adapter.getCount() - 1)) {
            btnNextType.setVisibility(View.INVISIBLE);
        } else {
            btnNextType.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void onPageScrollStateChanged(int state) {

    }


    /**
     * <p>Called when the checked radio button has changed. When the
     * selection is cleared, checkedId is -1.</p>
     *
     * @param group     the group in which the checked radio button has changed
     * @param checkedId the unique identifier of the newly checked radio button
     */
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {

            case R.id.rbtRandom:
                break;
            case R.id.rbtWeekly:
                new TripDateTimeDialog(getActivity(), TripTimeSettings.RepeatType.WEEKLY, this).show();
                break;
            case R.id.rbtWeeNever:
                new TripDateTimeDialog(getActivity(), TripTimeSettings.RepeatType.NEVER, this).show();
                break;
        }
    }

    @Override
    public void onTripTimeSettingsCreated(TripTimeSettings settings) {
        tripTimeSettings = settings;
    }

    private String isTripPickUp() {
        if (rdPickUp.isChecked()) {
            return TripModel.IS_DROP_OUT;
        } else if (rdDropOut.isChecked()) {
            return TripModel.IS_PICK_UP;
        }

        return null;
    }
}
