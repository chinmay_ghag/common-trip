package com.drive.share.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.drive.share.globals.utils.Commons;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.fragments.editor.AddTripFragment;

/**
 * Created by atulOholic on 4/3/15.
 */
public class TripNameDialog extends Dialog {

    private Context mContext;
    private EditText edtTripName;
    private Fragment mFragment;
    private String mDefault;


    public TripNameDialog(Context context, Fragment fragment, String defaultName) {
        super(context);
        mContext = context;
        mFragment = fragment;
        mDefault = defaultName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_trip_name);
        setTitle(mContext.getString(R.string.lbl_trip_name));

        Commons.setDialogFullScreen(mContext, this);


        edtTripName = (EditText) findViewById(R.id.edtTripName);
        edtTripName.setText(mDefault);

        findViewById(R.id.txtSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtTripName.getText().toString())) {
                    OnCreateTripNameListener listener = (AddTripFragment) mFragment;
                    listener.onCreateTripName(edtTripName.getText().toString());
                    dismiss();
                } else {
                    Commons.toastShort(mContext, mContext.getString(R.string.error_trip_name));
                }

            }
        });

        findViewById(R.id.txtCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }


    public static interface OnCreateTripNameListener {
        public void onCreateTripName(String tripName);
    }
}
