package com.drive.share.ui.fragments.tripcard.info;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.utilities.datareader.DataReader;
import com.drive.share.adapters.PeopleAdapter;
import com.drive.share.businesslayer.UserRequestLogic;
import com.drive.share.data.webapi.Passenger;
import com.drive.share.data.webapi.TripCardStatus;
import com.drive.share.globals.App;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.activities.tripcard.TripCardActivity;
import com.drive.share.ui.base.BaseFragment;
import com.mobeta.android.dslv.DragSortListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pervacio-Dev on 3/4/2015.
 */
public class TripCardPeopleFragment extends BaseFragment implements DataReader.DataClient{

    private DragSortListView lstPeople;
    private List<Passenger> mPeopleList;
    private PeopleAdapter adapter;
    private TripCardStatus tripCardStatus;

    @Override
    protected void getAllArguments() {

    }

    @Override
    protected void initializeUI(View rootView) {
        lstPeople = (DragSortListView) rootView.findViewById(R.id.lstPeopleList);
        tripCardStatus = ((TripCardActivity) getActivity()).getmTripCardStatus();
        if(tripCardStatus != null && "true".equalsIgnoreCase("" + tripCardStatus.getIsTripStarted())){
//            UserRequestLogic.sendTripAction(App.getInstance().getLoggedInUser().getUserId(), ((TripCardActivity) getActivity()).getmTripCardId(), "2",mTripCardDetails.getFirstname(),mCurrentLocation!=null?Double.toString(mCurrentLocation.getLatitude()):"72.6",mCurrentLocation!=null?Double.toString(mCurrentLocation.getLongitude()):"18.72",1, this);
        }
        lstPeople.setDropListener(onDrop);
        lstPeople.setRemoveListener(onRemove);
        mPeopleList = new ArrayList<Passenger>();
        mPeopleList.addAll(((TripCardActivity) getActivity()).getTripCardPassengers());
//        adapter.notifyDataSetChanged();


//        lstPeople.setCheeseList((ArrayList<Passenger>) mPeopleList);
        adapter = new PeopleAdapter(getActivity(),
                mPeopleList);

        adapter.setTripUserRelation(((TripCardActivity) getActivity()).getCardResponseModel().getTrip_user_relation());
        adapter.setTripCardId(((TripCardActivity) getActivity()).getmTripCardId());

        lstPeople.setAdapter(adapter);
        //TripEdtiorLogic.getUserTrip(this);
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_card_info_people;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    @Override
    public void onDataReceived(int readRequestCode, Object data) {

    }

    @Override
    public void onDataEmpty(int readRequestCode) {
        Log.d("Eroor", "Error fetching trips");
    }

    /**
     * On error.
     *
     * @param readRequestCode the read request code
     * @param error
     */
    @Override
    public void onError(int readRequestCode, Object error) {

    }
    private DragSortListView.DropListener onDrop =
            new DragSortListView.DropListener() {
                @Override
                public void drop(int from, int to) {
                    Passenger item = adapter.getItem(from);

                    adapter.remove(item);
                    adapter.insert(item, to);
                }
            };

    private DragSortListView.RemoveListener onRemove =
            new DragSortListView.RemoveListener() {
                @Override
                public void remove(int which) {
                    adapter.remove(adapter.getItem(which));
                    Passenger peopleResponse = adapter.getItem(which);
                    //remove user
                    UserRequestLogic.respondToRequest(peopleResponse.getPassengerId(), ((TripCardActivity) getActivity()).getmTripCardId(), "0", App.getInstance().getLoggedInUser().getUserId(), new DataReader.DataClient() {
                        @Override
                        public void onDataReceived(int readRequestCode, Object data) {
                            Toast.makeText(getActivity(), "Passenger removed.", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onDataEmpty(int readRequestCode) {

                        }

                        @Override
                        public void onError(int readRequestCode, Object error) {
                            Toast.makeText(getActivity(), "Something went wrong.", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public boolean needResponse() {
                            return true;
                        }
                    });
                }
            };



    @Override
    public boolean needResponse() {
        return isAdded();
    }
}