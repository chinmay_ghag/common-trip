package com.drive.share.ui.fragments.auth;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.utilities.Validation;
import com.android.utilities.datareader.DataReader;
import com.drive.share.businesslayer.AuthLogic;
import com.drive.share.data.webapi.UserModel;
import com.drive.share.globals.utils.Commons;
import com.drive.share.sharedrive.R;
import com.drive.share.storage.PreferenceUtils;
import com.drive.share.ui.activities.auth.AuthenticationActivity;
import com.drive.share.ui.base.BaseActivity;
import com.drive.share.ui.base.BaseFragment;

import java.util.List;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * interface
 * to handle interaction events.
 * Use the {@link UserAuthDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserAuthDetailFragment extends BaseFragment implements View.OnClickListener, DataReader.DataClient {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    // UI fields

    View rootLayout;
    UserModel user = null;
    private Spinner spnCountryCode;
    //  private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TripInfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserAuthDetailFragment newInstance(Bundle args) {
        UserAuthDetailFragment fragment = new UserAuthDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public UserAuthDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    protected void getAllArguments() {

    }

    @Override
    protected void initializeUI(View rootView) {
        rootLayout = rootView;
        rootView.findViewById(R.id.btnSignUp).setOnClickListener(this);
     //   rootView.findViewById(R.id.lblNumberQs).setOnClickListener(this);
        spnCountryCode = (Spinner) rootView.findViewById(R.id.spnCountryCode);
        spnCountryCode.setSelection(1);

        ((EditText) rootView.findViewById(R.id.txtMobileNumber)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    signUp();
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_user_auth_detail;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
       /* if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }*/
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //  mListener = null;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignUp:
                signUp();
                break;

         /*   case R.id.lblNumberQs:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("CommonTrip verifies users with the mobile number. We help connect with friends & also send you updates about rewards & Green Miles earned to this number.");
                alertDialog.setPositiveButton("OK", null);
                AlertDialog dialog = alertDialog.create();
                dialog.show();
                break;*/

        }
    }

    private void signUp() {
        if (Validation.Network.isConnected(getActivity())) {
            if ((user = AuthLogic.SignUp.getValidUser(rootLayout, this)) != null) {
                PreferenceUtils.insertBoolean(PreferenceUtils.KEY_IS_USER_LOGGED_IN,true);
                        ((AuthenticationActivity) getActivity()).setUser(user);
                AuthLogic.SignUp.signUp(user, this);
            }
        } else {
            Commons.errorNoInternet(getActivity());
        }
    }

    /**
     * On data received.
     *
     * @param readRequestCode the read request code
     * @param data            the data
     */
    @Override
    public void onDataReceived(int readRequestCode, Object data) {
        switch (readRequestCode) {
            case AuthLogic.SignUp.SIGNUP_WEB:

                if (AuthLogic.SignUp.processResponse(data, this, user)) {

                    ((BaseActivity) getActivity()).fragmentTransaction(BaseActivity.REPLACE_FRAGMENT, UserVerificationCodeFragment.newInstance(null),
                            R.id.frameContainer, false);
                } else {
                    Log.d("patrol", "NEW USER");
                }
                break;
        }
    }

    /**
     * On data empty.
     *
     * @param readRequestCode the read request code
     */
    @Override
    public void onDataEmpty(int readRequestCode) {

    }

    /**
     * On error.
     *
     * @param readRequestCode the read request code
     * @param error
     */
    @Override
    public void onError(int readRequestCode, Object error) {
        switch (readRequestCode) {

            case AuthLogic.SignUp.VALIDATE_INPUT:
                List<Validation.UI.FieldError> inputErrors = (List<Validation.UI.FieldError>) error;
                for (Validation.UI.FieldError fieldError : inputErrors) {
                    ((EditText) rootLayout.findViewById(fieldError.getFieldResourceId())).setError(fieldError.getErrorMessage());
                }

                break;

            case AuthLogic.SignUp.SIGNUP_WEB:
                Log.e("Sign Up Error", "" + error.toString());
                break;
        }
    }

    @Override
    public boolean needResponse() {
        return (isAdded() && isVisible());
    }


}
