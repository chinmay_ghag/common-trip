package com.drive.share.ui.fragments.editor;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.utilities.datareader.DataReader;
import com.drive.share.adapters.InviteFriendsAdapter;
import com.drive.share.businesslayer.TripEdtiorLogic;
import com.drive.share.data.webapi.BaseResponseModel;
import com.drive.share.data.webapi.UserModel;
import com.drive.share.globals.Constants;
import com.drive.share.globals.utils.Commons;
import com.drive.share.loaders.ContactsLoader;
import com.drive.share.sharedrive.R;
import com.drive.share.ui.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashesh on 14/2/15.
 */
public class TripInviteFragment extends BaseFragment implements DataReader.DataClient {

    //UI
    private ListView lsvFriends;
    private View emptyView;
    private ProgressBar mProgressBar;
    private TextView mEmptyTextView;

    private InviteFriendsAdapter mInviteFriendsAdapter;
    private List<UserModel> mFriendList;
    private String mTripId;
    private Button btnInvite;

    public final static String EXTRA_TRIP_ID = "tripID";

    public static TripInviteFragment newInstance() {
        return new TripInviteFragment();
    }


    @Override
    protected void getAllArguments() {
        mTripId = getArguments().getString(EXTRA_TRIP_ID);
    }

    @Override
    protected void initializeUI(View rootView) {
        lsvFriends = (ListView) rootView.findViewById(R.id.lstFriendList);
        btnInvite = (Button) rootView.findViewById(R.id.btnInvite);
        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TripEdtiorLogic.inviteUsers(TripInviteFragment.this, mTripId, getInvitedUsersList());
            }
        });

/*        emptyView = rootView.findViewById(android.R.id.empty);
        lsvFriends.setEmptyView(emptyView);*/

        mFriendList = new ArrayList<UserModel>();
        mInviteFriendsAdapter = new InviteFriendsAdapter(getActivity(), mFriendList);
        lsvFriends.setAdapter(mInviteFriendsAdapter);

        new ContactsLoader(getActivity(), this).loadRegisteredFriends();
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_invite_trip;
    }

    @Override
    public void onInternetAvailable() {

    }

    @Override
    public void onInternetUnavailable() {

    }

    @Override
    public void onDataReceived(int readRequestCode, Object data) {

        if (readRequestCode == Constants.REQ_INVITE_USERS) {

            if (null != data) {
                Commons.toastShort(getActivity(), ((BaseResponseModel) data).getMessage());
            }
        } else if (null != data) {
            Log.e("", "ListView::" + data);

            setEmptyLayout(false);
            mFriendList.clear();
            mFriendList.addAll((ArrayList<UserModel>) data);

            mInviteFriendsAdapter.notifyDataSetChanged();

        } else {
            Log.e("", "ListView byll::");
            setEmptyLayout(true);
        }
    }

    private void setEmptyLayout(boolean isEmpty) {

        /*if (isEmpty) {
            mProgressBar.setVisibility(View.GONE);
            mEmptyTextView.setText("No friends found");
        } else {
            mProgressBar.setVisibility(View.VISIBLE);
            mEmptyTextView.setText("Loading");
        }*/

    }

    @Override
    public void onDataEmpty(int readRequestCode) {
        Log.e("", "ListView::" + readRequestCode);
        setEmptyLayout(true);
    }

    @Override
    public void onError(int readRequestCode, Object error) {
        Log.e("", "ListView::" + readRequestCode + " " + error.toString());
        setEmptyLayout(true);
    }

    @Override
    public boolean needResponse() {
        return isAdded() && isVisible();
    }

    private List<String> getInvitedUsersList() {

        List<String> userIDs = new ArrayList<String>();

        for (UserModel model : mFriendList) {
            if (model.isInvited()) {
                userIDs.add(model.getUserId());
            }
        }

        return userIDs;

    }
}
