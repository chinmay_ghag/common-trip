package com.drive.share.ui.activities.auth;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.drive.share.data.webapi.UserModel;
import com.drive.share.sharedrive.R;
import com.drive.share.storage.PreferenceUtils;
import com.drive.share.ui.activities.HomeActivity;
import com.drive.share.ui.activities.profile.ProfileSetUpActivity;
import com.drive.share.ui.base.BaseActivity;
import com.drive.share.ui.fragments.auth.UserAuthDetailFragment;
import com.drive.share.ui.fragments.auth.UserVerificationCodeFragment;

public class AuthenticationActivity extends BaseActivity {

    private static final String TAG = AuthenticationActivity.class.getSimpleName();

    private UserModel mUser;

    public UserModel getUser() {
        return mUser;
    }

    public void setUser(UserModel mUser) {
        this.mUser = mUser;
    }


    @Override
    protected void onCreate(Bundle arg0) {
        if (Build.VERSION.SDK_INT < 16) {
            WindowManager.LayoutParams params = getWindow().getAttributes();
            params.flags |= WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
        }else{
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        super.onCreate(arg0);
    }

    @Override
    protected void releaseUi() {
    }

    @Override
    protected void setContent() {

        setContentView(R.layout.activity_auth);
    }

    @Override
    protected void initializeUi() {
        Log.d("patrol", TAG + " INIT --> ");
       /* if (App.getInstance().getLoggedInUser() != null) {
            Log.d("patrol", TAG + " LOGGED IN USER --> ");
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        } else {
            Log.d("Alpha","pref val "+PreferenceUtils.getString(PreferenceUtils.KEY_VERIFICATION_CODE));
            if (PreferenceUtils.getString(PreferenceUtils.KEY_VERIFICATION_CODE) == null) {
                Log.d("patrol", TAG + " NAV USER AUTH --> ");
                fragmentTransaction(ADD_FRAGMENT, UserAuthDetailFragment.newInstance(null),
                        R.id.frameContainer, false);
            } else {
                fragmentTransaction(ADD_FRAGMENT, UserVerificationCodeFragment.newInstance(null),
                        R.id.frameContainer, false);
            }

        }*/
        // Ashwini
        if (PreferenceUtils.getBoolean(PreferenceUtils.KEY_IS_USER_LOGGED_IN)== false){
            Log.d("patrol", TAG + " NAV USER AUTH --> ");
            fragmentTransaction(ADD_FRAGMENT, UserAuthDetailFragment.newInstance(null),
                    R.id.frameContainer, false);
        }else{
            Log.d("Alpha","pref val "+PreferenceUtils.getBoolean(PreferenceUtils.KEY_IS_UESR_VERIFIED));
            if (PreferenceUtils.getBoolean(PreferenceUtils.KEY_IS_UESR_VERIFIED) == false) {
                fragmentTransaction(ADD_FRAGMENT, UserVerificationCodeFragment.newInstance(null),
                        R.id.frameContainer, false);
            }else{
                Log.d("patrol", TAG + " LOGGED IN USER --> ");
                startActivity(new Intent(this, HomeActivity.class));
                finish();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cerate_trip, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            if (getSupportFragmentManager().findFragmentById(R.id.frameContainer)
                    instanceof UserAuthDetailFragment) {

                fragmentTransaction(REPLACE_FRAGMENT, UserVerificationCodeFragment.newInstance(null),
                        R.id.frameContainer, false);
            } else if (getSupportFragmentManager().findFragmentById(R.id.frameContainer)
                    instanceof UserVerificationCodeFragment) {
                startActivity(new Intent(this, ProfileSetUpActivity.class));
                finish();
            } else {
                startActivity(new Intent(this, HomeActivity.class));
                finish();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
